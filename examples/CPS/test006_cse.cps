fix mainfunc(k, a, b, x) =
  let ab1 = primop mul(a,b)
  let abx1 = primop mul(ab1, x)
  let ab2 = primop mul(a, b)
  let abx2 = primop mul(ab2, x)
  let s = primop add(abx1, abx2)
  call k(s)
  
call mainfunc(top_cont,2,3,4)
