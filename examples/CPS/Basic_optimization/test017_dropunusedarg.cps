fix mainfunc(k, x) =
  let r = record
    7
    42
  fix otherfunc(kk, t, g) =
    let a = #0 t
    let b = #1 t
    let c = primop mul(a, b)
    call kk(c)
  call otherfunc(k, r, 5)
call mainfunc(top_cont, 3)