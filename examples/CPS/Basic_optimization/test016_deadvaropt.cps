fix mainfunc(k, x) =
  let r = record
    7
    42
  fix otherfunc(kk, t) =
    let a = #0 t
    let b = #1 t
    let c = primop mul(a, b)
    call kk(c)
  call otherfunc(k, r)
  fix deadfun(x, t) =
  	let a = #0 t
  	call x(a)
call mainfunc(top_cont, 3)