fix f(c,x) =
  fix g(b,k) =
  	let p =  primop mul(b, x)
  	call k(p)
  call c(g)
call f(top_cont, 5, 5)