bin/semiML -check-contracts -internal-log-file log \
  -require transform:to_lambda -sep -echo Lambda: -sep -pretty \
  -require transform:to_cps -sep -echo CPS: -sep -pretty \
  -require transform:closure_conversion -sep -echo CLO: -sep -pretty \
  -require transform:to_abstract_machine -sep -echo AM: -sep -pretty $1
