
open Compiler

let register_analyses () =
  Analysis.NuL.FreeVars.register ();
  Analysis.CPS.FreeVars.register ();
  Analysis.CPS.TrivialVars.register ();
  Analysis.CPS.FuncCallCount.register ();
  Analysis.CPS.FunName2Def.register ();
  Analysis.CPS.Var2Def.register ();
  Analysis.CPS.FlattenInfo.register ();
  Analysis.CPS.VarUseCount.register ();
  Analysis.CPS.FunctionEscape.register ();
  Analysis.CPS.RecordInfo.register ();
  ()

let register_transformations () =
  Transform.CPS.ToAbstractMachine.register ();
  Transform.CPS.RegisterSpilling.register ();
  Transform.CPS.ScopeCheck.register ();
  Transform.CPS.UniqueTags.register ();
  Transform.CPS.UniqueVars.register ();
  Transform.CPS.ClosureConversion.register ();
  Transform.CPS.LiftDefinitions.register ();
  Transform.Lambda.ToCPS.register ();
  Transform.CPS.BetaExpansion.register ();
  Transform.CPS.Hoisting.register ();
  Transform.CPS.CSE.register ();
  Transform.CPS.Meta.register ();
  Transform.CPS.EtaSplit.register ();
  Transform.CPS.EtaReduction.register ();
  Transform.CPS.BetaContraction.register ();
  Transform.CPS.Uncurring.register();
  Transform.CPS.Flatten.register();
  Transform.CPS.Folding.register();
  Transform.CPS.Bis.register();
  Transform.CPS.Sfkr.register();
  Transform.CPS.RecordOpt.register();
  Transform.CPS.DeadVarElim.register();
  Transform.CPS.DropUnusedArg.register();
  Transform.CPS.BasicMeta.register();
  Transform.Lambda.ToCPS.register ();  
  Transform.Lambda.UniqueVars.register ();
  Transform.MiniML.ToLambda.register ();
  Transform.MiniML.TypeCheck.Main.register ();
  Transform.NuL.PartialEval.register ();
  Transform.NuL.ScopeCheck.register ();
  Transform.NuL.UniqueTags.register ();
  Transform.NuL.UniqueVars.register ();
  Transform.NuL.UnusedLet.register ();
  Transform.NuL.ConstantFold.register ();
  ()

let register_contract_checkers () =
  ContractCheck.CPS.PrimopArity.register ();
  ContractCheck.CPS.RegistersSpilled.register ();
  ContractCheck.CPS.RightScopes.register ();
  ContractCheck.CPS.UniqueTags.register ();
  ContractCheck.CPS.UniqueVars.register ();
  ContractCheck.Lambda.RightScopes.register ();
  ContractCheck.Lambda.UniqueTags.register ();
  ContractCheck.Lambda.UniqueVars.register ();
  ContractCheck.Lambda.SwitchWellFormed.register ();
  ContractCheck.NuL.RightScopes.register ();
  ContractCheck.NuL.UniqueTags.register ();
  ContractCheck.NuL.UniqueVars.register ();
  ()

let register_evaluators () =
  register_evaluator
    ~lang: Lang_CPS
    ~name: "CPS:eval"
    Lang.CPS.Eval.eval_program;
  register_evaluator
    ~lang: Lang_Lambda
    ~name: "Lambda:eval"
    Lang.Lambda.Eval.eval_program;
  register_evaluator
    ~lang: Lang_NuL
    ~name: "NuL:eval"
    ~require: [ Lang.NuL.Contracts.right_scopes ]
    Lang.NuL.Eval.eval_program;
  ()

let register_parsers () =
  register_parser
    ~lang: Lang_RawCPS
    ~name: "RawCPS:parser"
    ~extension: "cps"
    Lang.RawCPS.Parser.parser_func;
  register_parser
    ~lang: Lang_RawMiniML
    ~name: "RawMiniML:parser"
    ~extension: "mini"
    Lang.RawMiniML.Parser.parser_func;
  register_parser
    ~lang: Lang_RawNuL
    ~name: "RawNuL:parser"
    ~extension: "nul"
    Lang.RawNuL.Parser.parser_func;
  ()

let register_printers () =
  register_pretty_printer
    ~lang: Lang_AbstractM
    ~name: "AbstractM:pretty"
    Lang.AbstractM.Pretty.pretty_program;
  register_pretty_printer
    ~lang: Lang_CPS
    ~name: "CPS:pretty"
    Lang.CPS.Pretty.pretty_program;
  register_pretty_printer
    ~lang: Lang_Lambda
    ~name: "Lambda:pretty"
    Lang.Lambda.Pretty.pretty_program;
  register_pretty_printer
    ~lang: Lang_NuL
    ~name: "NuL:pretty"
    Lang.NuL.Pretty.pretty_program;
  ()

let init () =
  register_analyses ();
  register_contract_checkers ();
  register_evaluators ();
  register_parsers ();
  register_printers ();
  register_transformations ();
  ()
