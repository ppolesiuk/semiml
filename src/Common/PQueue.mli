
module type OrderedType = sig
  type t
  val compare : t -> t -> int
end

module type S = sig
  type key
  type 'a t

  val empty : 'a t

  val is_empty : 'a t -> bool

  val peek : 'a t -> key * 'a
  val pop  : 'a t -> 'a t
  val push : 'a t -> key -> 'a -> 'a t
end

module Make(Key : OrderedType) : S with type key = Key.t
