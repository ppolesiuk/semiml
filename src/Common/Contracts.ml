open Language

let right_scopes = Contract.create
  ~description:
    "Every variable is bound in the program."
  ~languages: [CPS; Lambda; NuL]
  "contract:right_scopes"

let unique_vars = Contract.create
  ~description:
    "There are no two different local variables represeted by the same value."
  ~languages: [CPS; Lambda; MiniML; NuL]
  "contract:unique_vars"

let unique_tags = Contract.create
  ~description:
    "Every tag in the program is unique."
  ~languages: [AbstractM; CPS; Lambda; MiniML; NuL]
  "contract:unique_tags"
