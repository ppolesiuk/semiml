
module type OrderedType = sig
  type t
  val compare : t -> t -> int
end

module type S = sig
  type key
  type 'a t

  val create : unit -> 'a t

  val is_empty : 'a t -> bool

  val pop  : 'a t -> key * 'a
  val push : 'a t -> key -> 'a -> unit
end

module Make(Key : OrderedType) : S with type key = Key.t = struct
  module Q = PQueue.Make(Key)

  type key = Key.t
  type 'a t = 'a Q.t ref

  let create () = ref Q.empty

  let is_empty q = Q.is_empty !q

  let pop q =
    let r = Q.peek !q in
    q := Q.pop !q;
    r

  let push q k v =
    q := Q.push !q k v
end
