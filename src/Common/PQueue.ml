
module type OrderedType = sig
  type t
  val compare : t -> t -> int
end

module type S = sig
  type key
  type 'a t

  val empty : 'a t

  val is_empty : 'a t -> bool

  val peek : 'a t -> key * 'a
  val pop  : 'a t -> 'a t
  val push : 'a t -> key -> 'a -> 'a t
end

module Make(Key : OrderedType) : S with type key = Key.t = struct
  type key = Key.t
  type 'a t =
  | Leaf
  | Node of key * 'a * int * 'a t * 'a t

  let empty = Leaf

  let is_empty q =
    match q with
    | Leaf   -> true
    | Node _ -> false

  let peek q =
    match q with
    | Leaf -> raise Not_found
    | Node(k, v, _, _, _) -> (k, v)

  let depth q =
    match q with
    | Leaf -> 0
    | Node(_, _, d, _, _) -> d

  let rec join q1 q2 =
    match q1, q2 with
    | Leaf, q2 -> q2
    | q1, Leaf -> q1
    | Node(k1, v1, _, l1, r1), Node(k2, v2, _, l2, r2) ->
      if Key.compare k1 k2 < 0 then
        join3 k1 v1 l1 r1 q2
      else
        join3 k2 v2 l2 r2 q1
  and join3 k v l r q =
    let (q1, q2) =
      if depth q < depth l then (l, join r q)
      else (q, join l r)
    in
    let d1 = depth q1 in
    let d2 = depth q2 in
    if d1 < d2 then Node(k, v, d1 + 1, q2, q1)
    else Node(k, v, d2 + 1, q1, q2)

  let pop q =
    match q with
    | Leaf -> raise Not_found
    | Node(_, _, _, l, r) -> join l r

  let push q k v =
    join q (Node(k, v, 1, Leaf, Leaf))
end
