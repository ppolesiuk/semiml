
type primop_type =
| TUnit
| TBool
| TInt
| TReal
| TString
| TExn
| TVar    of int
| TRef    of primop_type
| TCont   of primop_type
| TArray  of primop_type
| TArrow  of primop_type * primop_type

module type Group = sig
  type t

  (** [is_operator op] determines if op is operator or predefined constant
  (useful for pretty-printing *)
  val is_operator : t -> bool

  val name        : t -> string
  val alpha_name  : t -> string
  val arity       : t -> int
  val cps_value_n : t -> int
  val cps_cont_n  : t -> int

  val type_of : t -> int * primop_type list * primop_type

  val all_values : t list
end

module Arith = struct
  type t =
  | Add | Sub
  | Neg
  | Mul | Div | Mod
  | RShift | LShift
  | Orb | Andb | Xorb
  | Notb
  | Lt  | Le  | Gt  | Ge
  | RangeCheck

  let all_values =
    [ Add; Sub; Neg; Mul; Div; Mod; RShift; LShift; Orb; Andb; Xorb; Notb
    ; Lt; Le; Gt; Ge; RangeCheck
    ]

  let is_operator op =
    match op with
    | Add | Sub | Neg | Mul | Div | Mod | RShift | LShift | Orb | Andb | Xorb 
    | Notb | Lt | Le | Gt | Ge -> true
    | RangeCheck -> false

  let name op =
    match op with
    | Add        -> "+"
    | Sub        -> "-"
    | Neg        -> "~"
    | Mul        -> "*"
    | Div        -> "/"
    | Mod        -> "mod"
    | RShift     -> ">>>"
    | LShift     -> "<<<"
    | Orb        -> "orb"
    | Andb       -> "andb"
    | Xorb       -> "xorb"
    | Notb       -> "notb"
    | Lt         -> "<"
    | Le         -> "<="
    | Gt         -> ">"
    | Ge         -> ">="
    | RangeCheck -> "rangecheck"

  let alpha_name op =
    match op with
    | Add        -> "add"
    | Sub        -> "sub"
    | Neg        -> "neg"
    | Mul        -> "mul"
    | Div        -> "div"
    | Mod        -> "mod"
    | RShift     -> "shr"
    | LShift     -> "shl"
    | Orb        -> "orb"
    | Andb       -> "andb"
    | Xorb       -> "xorb"
    | Notb       -> "notb"
    | Lt         -> "ilt"
    | Le         -> "ile"
    | Gt         -> "igt"
    | Ge         -> "ige"
    | RangeCheck -> "rangecheck"

  let arity op =
    match op with
    | Neg | Notb -> 1
    | Add | Sub | Mul | Div | Mod | RShift | LShift | Orb | Andb | Xorb 
    | Lt | Le | Gt | Ge | RangeCheck -> 2

  let cps_value_n op =
    match op with
    | Lt | Le | Gt | Ge | RangeCheck -> 0
    | Add | Sub | Neg | Mul | Div | Mod | RShift | LShift 
    | Orb | Andb | Xorb | Notb -> 1

  let cps_cont_n op =
    match op with
    | Add | Sub | Neg | Mul | Div | Mod | RShift | LShift 
    | Orb | Andb | Xorb | Notb -> 1
    | Lt | Le | Gt | Ge | RangeCheck -> 2

  let type_of op =
    match op with
    | Neg | Notb -> (0, [TInt], TInt)
    | Add | Sub | Mul | Div | Mod | RShift | LShift | Orb | Andb | Xorb ->
      (0, [TInt; TInt], TInt)
    | Lt | Le | Gt | Ge | RangeCheck -> (0, [TInt; TInt], TBool)
end

module FArith = struct
  type t =
  | FAdd | FSub
  | FMul | FDiv
  | FEq  | FNeq 
  | FLt  | FLe  | FGt  | FGe

  let all_values =
    [ FAdd; FSub; FMul; FDiv; FEq; FNeq; FLt; FLe; FGt; FGe ]

  let is_operator op = false

  let name op =
    match op with
    | FAdd -> "fadd"
    | FSub -> "fsub"
    | FMul -> "fmul"
    | FDiv -> "fdiv"
    | FEq  -> "feq"
    | FNeq -> "fneq"
    | FLt  -> "flt"
    | FLe  -> "fle"
    | FGt  -> "fgt"
    | FGe  -> "fge"

  let alpha_name op = name op

  let arity op =
    match op with
    | FAdd | FSub | FMul | FDiv | FEq | FNeq | FLt | FLe | FGt | FGe -> 2

  let cps_value_n op =
    match op with
    | FEq | FNeq | FLt | FLe | FGt | FGe -> 0
    | FAdd | FSub | FMul | FDiv -> 1

  let cps_cont_n op =
    match op with
    | FAdd | FSub | FMul | FDiv -> 1
    | FEq | FNeq | FLt | FLe | FGt | FGe -> 2

  let type_of op =
    match op with
    | FAdd | FSub | FMul | FDiv -> (0, [TReal; TReal], TReal)
    | FEq  | FNeq | FLt  | FLe | FGt | FGe -> (0, [TReal; TReal], TBool)
end

module Mem = struct
  type t =
  | Deref | Subscript | OrdOf
  | Assign | UnboxedAssign
  | Update | UnboxedUpdate | Store
  | MakeRef | MakeRefUnboxed
  | AllocArray | AllocUnboxedArray | AllocByteArray

  let all_values =
    [ Deref; Subscript; OrdOf; Assign; UnboxedAssign
    ; Update; UnboxedUpdate; Store; MakeRef; MakeRefUnboxed
    ; AllocArray; AllocUnboxedArray; AllocByteArray
    ]

  let is_operator op =
    match op with
    | Deref | Assign -> true
    | Subscript | OrdOf | UnboxedAssign | Update | UnboxedUpdate | Store 
    | MakeRef | MakeRefUnboxed | AllocArray | AllocUnboxedArray 
    | AllocByteArray -> false

  let name op =
    match op with
    | Deref          -> "!"
    | Subscript      -> "subscript"
    | OrdOf          -> "ord_of"
    | Assign         -> ":="
    | UnboxedAssign  -> "unboxed_assign"
    | Update         -> "update"
    | UnboxedUpdate  -> "unboxed_update"
    | Store          -> "store"
    | MakeRef        -> "ref"
    | MakeRefUnboxed -> "ref_unboxed"
    | AllocArray     -> "alloc_array"
    | AllocUnboxedArray -> "alloc_unboxed_array"
    | AllocByteArray -> "alloc_byte_array"

  let alpha_name op =
    match op with
    | Deref  -> "deref"
    | Assign -> "assign"
    | Subscript | OrdOf | UnboxedAssign | Update | UnboxedUpdate | Store 
    | MakeRef | MakeRefUnboxed | AllocArray | AllocUnboxedArray 
    | AllocByteArray -> name op

  let arity op =
    match op with
    | Deref | MakeRef | MakeRefUnboxed | AllocArray | AllocUnboxedArray 
    | AllocByteArray -> 1
    | Subscript | OrdOf | Assign | UnboxedAssign -> 2
    | Update | UnboxedUpdate | Store -> 3

  let cps_value_n op =
    match op with
    | Assign | UnboxedAssign | Update | UnboxedUpdate | Store -> 0
    | Deref | Subscript | OrdOf | MakeRef | MakeRefUnboxed | AllocArray 
    | AllocUnboxedArray | AllocByteArray -> 1

  let cps_cont_n op =
    match op with
    | Deref | Subscript | OrdOf | Assign | UnboxedAssign 
    | Update | UnboxedUpdate | Store | MakeRef | MakeRefUnboxed 
    | AllocArray | AllocUnboxedArray | AllocByteArray -> 1

  let type_of op =
    match op with
    | Deref      -> (1, [TRef(TVar 0)], TVar 0)
    | Subscript  -> (1, [TArray(TVar 0); TInt], TVar 0)
    | OrdOf      -> (0, [TString; TInt], TInt)
    | Assign | UnboxedAssign -> (1, [TRef(TVar 0); TVar 0], TUnit)
    | Update | UnboxedUpdate -> (1, [TArray(TVar 0); TInt; TVar 0], TUnit)
    | Store -> (0, [TString; TInt; TInt], TUnit)
    | MakeRef | MakeRefUnboxed -> (1, [TVar 0], TRef (TVar 0))
    | AllocArray | AllocUnboxedArray -> (1, [TInt], TArray (TVar 0))
    | AllocByteArray -> (0, [TInt], TString)
end

module Repr = struct
  type t =
  | Boxed
  | ALength | SLength
  | PhysEq | PhysNEq

  let all_values =
    [ Boxed; ALength; SLength; PhysEq; PhysNEq ]

  let is_operator op =
    match op with
    | Boxed | ALength | SLength -> false
    | PhysEq | PhysNEq -> true

  let name op =
    match op with
    | Boxed   -> "boxed"
    | ALength -> "alength"
    | SLength -> "slength"
    | PhysEq  -> "=="
    | PhysNEq -> "!="

  let alpha_name op =
    match op with
    | Boxed | ALength | SLength -> name op
    | PhysEq  -> "pheq"
    | PhysNEq -> "phneq"

  let arity op =
    match op with
    | Boxed | ALength | SLength -> 1
    | PhysEq | PhysNEq -> 2

  let cps_value_n op =
    match op with
    | Boxed | PhysEq | PhysNEq -> 0
    | ALength | SLength -> 1

  let cps_cont_n op =
    match op with
    | Boxed | PhysEq | PhysNEq -> 2
    | ALength | SLength -> 1

  let type_of op =
    match op with
    | Boxed   -> (1, [TVar 0], TBool)
    | ALength -> (1, [TArray (TVar 0)], TInt)
    | SLength -> (0, [TString], TInt)
    | PhysEq | PhysNEq -> (1, [TVar 0; TVar 0], TBool)
end

module Exn = struct
  type t =
  | GetHdlr
  | SetHdlr

  let all_values =
    [ GetHdlr; SetHdlr ]

  let is_operator op = false

  let name op =
    match op with
    | GetHdlr -> "get_hdlr"
    | SetHdlr -> "set_hdlr"

  let alpha_name op = name op

  let arity op =
    match op with
    | GetHdlr -> 0
    | SetHdlr -> 1

  let cps_value_n op =
    match op with
    | SetHdlr -> 0
    | GetHdlr -> 1

  let cps_cont_n op =
    match op with
    | GetHdlr | SetHdlr -> 1

  let type_of op =
    match op with
    | GetHdlr -> (0, [], TExn)
    | SetHdlr -> (0, [TExn], TUnit)
end

module Control = struct
  type t =
  | CallCC
  | Throw

  let all_values =
    [ CallCC; Throw ]

  let is_operator op = false
  let name op =
    match op with
    | CallCC -> "call_cc"
    | Throw  -> "throw"

  let alpha_name op = name op

  let arity op =
    match op with
    | CallCC | Throw -> 1

  let cps_value_n op = 0
  let cps_cont_n  op = 0

  let type_of op =
    match op with
    | CallCC -> (1, [TArrow(TCont (TVar 0), TVar 0)], TVar 0)
    | Throw  -> (2, [TCont (TVar 0)], TArrow(TVar 0, TVar 1))
end
