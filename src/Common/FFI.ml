type 'a view =
  { val_to_int64  : 'a -> int64
  ; val_to_string : 'a -> string
  ; int64_to_val  : int64 -> 'a
  ; string_to_val : string -> 'a
  }

type foreign = { call : 'a . 'a view -> 'a list -> 'a }

let f_print view args =
  match args with
  | [ s ] ->
    print_string (view.val_to_string s);
    view.int64_to_val 0L
  | _ -> failwith "FFI print: invalid number of arguments"

let by_name = function
  | "print" -> { call = f_print }
  | name    -> failwith "Invalid FFI function name."
