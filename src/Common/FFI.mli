type 'a view =
  { val_to_int64  : 'a -> int64
  ; val_to_string : 'a -> string
  ; int64_to_val  : int64 -> 'a
  ; string_to_val : string -> 'a
  }

type foreign = { call : 'a . 'a view -> 'a list -> 'a }

val by_name : string -> foreign
