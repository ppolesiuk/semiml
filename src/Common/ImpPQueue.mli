
module type OrderedType = sig
  type t
  val compare : t -> t -> int
end

module type S = sig
  type key
  type 'a t

  val create : unit -> 'a t

  val is_empty : 'a t -> bool

  val pop  : 'a t -> key * 'a
  val push : 'a t -> key -> 'a -> unit
end

module Make(Key : OrderedType) : S with type key = Key.t
