
exception Fatal_error
exception Not_implemented of string

let error_counter = ref 0

let error_p pos msg =
  error_counter := !error_counter + 1;
  Printf.eprintf ("%s: error: " ^^ msg ^^ "\n")
    (Common.Position.to_string pos)

let warning_p pos msg =
  Printf.eprintf ("%s: warning: " ^^ msg ^^ "\n")
    (Common.Position.to_string pos)

let note_p pos msg =
  Printf.eprintf ("%s: note: " ^^ msg ^^ "\n")
    (Common.Position.to_string pos)

let error_lp pos msg =
  error_p (Common.Position.of_lexing pos) msg

let warning_lp pos msg =
  warning_p (Common.Position.of_lexing pos) msg

let note_lp pos msg =
  note_p (Common.Position.of_lexing pos) msg

let error_pp p1 p2 msg =
  error_p (Common.Position.of_pp p1 p2) msg

let warning_pp p1 p2 msg =
  warning_p (Common.Position.of_pp p1 p2) msg

let note_pp p1 p2 msg =
  note_p (Common.Position.of_pp p1 p2) msg

let error_no_pos msg =
  error_counter := !error_counter + 1;
  Printf.eprintf ("error: " ^^ msg ^^ "\n")

let warning_no_pos msg =
  Printf.eprintf ("warning: " ^^ msg ^^ "\n")

let note_no_pos msg =
  Printf.eprintf ("note: " ^^ msg ^^ "\n")

let pos_of_tag_option tag =
  match tag with
  | None -> None
  | Some tag -> LangMetaData.Position.try_get tag

let error ?tag msg =
  match pos_of_tag_option tag with
  | None     -> error_no_pos msg
  | Some pos -> error_p pos msg

let warning ?tag msg =
  match pos_of_tag_option tag with
  | None     -> warning_no_pos msg
  | Some pos -> warning_p pos msg

let note ?tag msg =
  match pos_of_tag_option tag with
  | None     -> note_no_pos msg
  | Some pos -> note_p pos msg

let prefix_box tag level =
  match pos_of_tag_option tag with
  | None     -> Printing.Box.text level
  | Some pos ->
    Printing.Box.box 
      [ Printing.Box.text (Common.Position.to_string pos)
      ; Printing.Box.text ": "
      ; Printing.Box.text level
      ]

let error_b ?tag boxes =
  error_counter := !error_counter + 1;
  Printing.Box.print_stderr
    (Printing.Box.box
      [ prefix_box tag "error:"
      ; Printing.Box.indent 2 (Printing.Box.white_sep 
          (Printing.Box.box boxes))
      ])

let warning_b ?tag boxes =
  Printing.Box.print_stderr
    (Printing.Box.box
      [ prefix_box tag "warning:"
      ; Printing.Box.indent 2 (Printing.Box.white_sep 
          (Printing.Box.box boxes))
      ])

let note_b ?tag boxes =
  Printing.Box.print_stderr
    (Printing.Box.box
      [ prefix_box tag "note:"
      ; Printing.Box.indent 2 (Printing.Box.white_sep 
          (Printing.Box.box boxes))
      ])

let not_implemented name =
  error "Not implemented: %s" name;
  raise (Not_implemented name)
