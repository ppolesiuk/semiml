open MetaCommand

let beta_contraction =
  MetaCmd_RequireC[Transform.CPS.BetaContraction.c_beta_contraction]
let record_opt =
  MetaCmd_RequireC[Transform.CPS.RecordOpt.c_record_opt]
let sfkr =
  MetaCmd_RequireC[Transform.CPS.Sfkr.c_sfkr]
let folding =
  MetaCmd_RequireC[Transform.CPS.Folding.c_folding]
let flatten =
  MetaCmd_RequireC[Transform.CPS.Flatten.c_flatten]
let dead_var_elim =
  MetaCmd_RequireC[Transform.CPS.DeadVarElim.c_dead_var_elim]
let drop_unused_arg =
  MetaCmd_RequireC[Transform.CPS.DropUnusedArg.c_drop_unused_arg]
let bis =
  MetaCmd_RequireC[Transform.CPS.Bis.c_bis]
let eta_split =
  MetaCmd_RequireC[Transform.CPS.EtaSplit.c_eta_split]
let eta_reduction =
  MetaCmd_RequireC[Transform.CPS.EtaReduction.c_eta_reduction]
let uncurring =
  MetaCmd_RequireC[Transform.CPS.Uncurring.c_uncurry]

let basic_phase1_loop_bound = 32
let basic_phase2_loop_bound = 32
let contraction_count_bound = 1

let basic_phase1 =
  MetaCmd_Seq
  [ beta_contraction
  ; record_opt
  ; sfkr
  ; folding
  ; flatten
  ; dead_var_elim
  ; drop_unused_arg
  ; bis
  ]

let basic_phase1_loop =
  MetaCmd_Dynamic(fun _ ->
    let step = ref 0 in
    MetaCmd_DoWhile(basic_phase1, fun _ ->
      step := !step + 1;
      let contraction_count = MetaData.CPS.CountContraction.get() in
      CompilerLog.info "step %d of basic_phase1 loop: %d contractions."
        !step contraction_count;
      !step < basic_phase1_loop_bound &&
      contraction_count >= contraction_count_bound))

let basic_phase2 =
  MetaCmd_Seq
  [ eta_split
  ; basic_phase1
  ; eta_reduction
  ; uncurring
  ]

let basic_phase2_loop =
  MetaCmd_Dynamic(fun _ ->
    let step = ref 0 in
    MetaCmd_DoWhile(basic_phase2, fun _ -> 
      step := !step + 1;
      let contraction_count = MetaData.CPS.CountContraction.get() in
      CompilerLog.info "step %d of basic_phase2 loop: %d contractions."
        !step contraction_count;
      !step < basic_phase2_loop_bound &&
      contraction_count >= contraction_count_bound))

let basic =
  MetaCmd_Seq
  [ basic_phase1_loop
  ; basic_phase2_loop
  ; basic_phase1_loop
  ]
