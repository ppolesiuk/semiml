open Lang.Lambda.Ast

module VarMap = Common.Var.Map

let build_fix_env env defs =
  List.fold_left (fun env (x, _) ->
    let y = Common.Var.copy x in
    VarMap.add x y env
  ) env defs

let rec tr_expr env expr =
  { expr with
    e_kind =
      match expr.e_kind with
      | Int _ | Real _ | String _ | Prim _ | FFI _ -> expr.e_kind
      | Var x -> Var(VarMap.find x env)
      | Fn(x, body) ->
        let y = Common.Var.copy x in
        Fn(y, tr_expr (VarMap.add x y env) body)
      | Fix(defs, rest) ->
        let env = build_fix_env env defs in
        let defs = List.map (fun (x, body) ->
            (VarMap.find x env, tr_expr env body)
          ) defs in
        Fix(defs, tr_expr env rest)
      | App(e1, e2) -> App(tr_expr env e1, tr_expr env e2)
      | Switch sw -> Switch
        { sw_expr           = tr_expr env sw.sw_expr
        ; sw_possible_cons  = sw.sw_possible_cons
        ; sw_con_cases      =
            List.map (fun (con, e) -> (con, tr_expr env e)) sw.sw_con_cases
        ; sw_constant_range = sw.sw_constant_range
        ; sw_constant_cases =
            List.map (fun (c, e) -> (c, tr_expr env e)) sw.sw_constant_cases
        ; sw_default_case   =
          begin match sw.sw_default_case with
          | None   -> None
          | Some e -> Some(tr_expr env e)
          end
        }
      | Con(con, e) -> Con(con, tr_expr env e)
      | Decon(con, e) -> Decon(con, tr_expr env e)
      | SwitchExn(e, cases, d) ->
        let cases = 
          List.map (fun (e1, e2) -> (tr_expr env e1, tr_expr env e2)) cases in
        SwitchExn(tr_expr env e, cases, tr_expr env d)
      | ConExn(e1, e2) -> ConExn(tr_expr env e1, tr_expr env e2)
      | ExnValue e -> ExnValue(tr_expr env e)
      | ExnName  e -> ExnName(tr_expr env e)
      | Record es -> Record(List.map (tr_expr env) es)
      | Select(i, e) -> Select(i, tr_expr env e)
      | Raise e -> Raise(tr_expr env e)
      | Handle(e1, e2) -> Handle(tr_expr env e1, tr_expr env e2)
  }

let transform expr =
  tr_expr VarMap.empty expr

let c_unique_vars = Contract.create
  ~description: "Restore unique_vars contract"
  ~languages: [Language.Lambda]
  "transform:unique_vars"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_Lambda
    ~target: Compiler.Lang_Lambda
    ~name:   "Lambda:unique_vars"
    ~require:
      [ Lang.Lambda.Contracts.right_scopes
      ]
    ~contracts:
      [ Lang.Lambda.Contracts.right_scopes
      ; Lang.Lambda.Contracts.unique_vars
      ; c_unique_vars
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.Lambda.Contracts.unique_tags
      ; Contract.saves_contract Lang.Lambda.Contracts.switch_well_formed
      ]
    transform
