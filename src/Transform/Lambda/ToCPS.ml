open Lang.CPS.Ast
open List
module L = Lang.Lambda.Ast

let (<|) f x = f x
let (<~) f g x = f(g(x))
let rec fresh_vars name = function
  | 0 -> []
  | n -> Common.Var.create ~name :: fresh_vars name (n-1)
  
let cast_primop : Lang.Lambda.Primop.t -> Lang.CPS.Primop.t = function
  | Lang.Lambda.Primop.Arith p -> Lang.CPS.Primop.Arith p
  | Lang.Lambda.Primop.FArith p -> Lang.CPS.Primop.FArith p
  | Lang.Lambda.Primop.Mem p -> Lang.CPS.Primop.Mem p
  | Lang.Lambda.Primop.Repr p -> Lang.CPS.Primop.Repr p
  | Lang.Lambda.Primop.Control _ -> failwith "ToCPS: internal error"

let arity : Lang.Lambda.Primop.t -> int =
  Lang.CPS.Primop.arity <~ cast_primop
      
let cps_value_n : Lang.Lambda.Primop.t -> int =
  Lang.CPS.Primop.cps_value_n <~ cast_primop

let cps_cont_n : Lang.Lambda.Primop.t -> int =
  Lang.CPS.Primop.cps_cont_n <~ cast_primop
      
let rec transf : (L.expr * (value -> expr)) -> expr =
  fun ({ L.e_tag; L.e_kind } as lexpr, c) ->
    let wrap_ekind ekind = { e_tag = e_tag; e_kind = ekind } in
    let record(x, y, z) = wrap_ekind <| Record(x, y, z) in
    let select(x, y, z, t) = wrap_ekind <| Select(x, y, z, t) in
    let app(x, y) = wrap_ekind <| App(x, y) in
    let fix(x, y) = wrap_ekind <| Fix(x, y) in
    let switch(x, y) =
      if length y = 1 then hd y
      else wrap_ekind <| Switch(x, y) in
    let ffi(name, args, ret, cont) = wrap_ekind <| FFI(name, args, ret, cont) in
    let primop(x, y, z, t) = wrap_ekind <| Primop(x, y, z, t) in
    let primopc(x, y, z, t) = primop(cast_primop x, y, z, t) in
    let gethdlr = Lang.CPS.Primop.Exn(Common.Primop.Exn.GetHdlr) in
    let sethdlr = Lang.CPS.Primop.Exn(Common.Primop.Exn.SetHdlr) in
    let eq = Lang.CPS.Primop.Repr(Common.Primop.Repr.PhysEq) in
    let boxed = Lang.CPS.Primop.Repr(Common.Primop.Repr.Boxed) in
    let int x = Int(Int64.of_int x) in
    let lwrap_ekind ekind = { L.e_tag = e_tag; L.e_kind = ekind } in
    let lvar x = lwrap_ekind <| L.Var(x) in
    let lfn(x, y) = lwrap_ekind <| L.Fn(x, y) in  
    let lapp(x, y) = lwrap_ekind <| L.App(x, y) in
    let lint x = lwrap_ekind <| L.Int(Int64.of_int x) in  
    let lrecord(x) = lwrap_ekind <| L.Record(x) in
    let lselect(x, y) = lwrap_ekind <| L.Select(x, y) in
    let lffi(name, arity) = lwrap_ekind <| L.FFI(name, arity) in
    let lprim(x) = lwrap_ekind <| L.Prim(x) in
    let eta_expand e =
      let x = Common.Var.create ~name:"x" () in
      lfn(x, lapp(e, lvar x))
    in
    let record_trim_eta_expand arity body arg =
      let x = Common.Var.create ~name: "x" () in
      let var_x = lvar x in
      let rec fields k =
        if k < arity then
          lselect(k, var_x) :: (fields <| k+1)
        else []
      in lapp(lfn(x, lapp(body, lrecord(fields 0))), arg)
    in
    let transfl : (L.expr list * (value list -> expr)) -> expr =
      fun (a, c) ->
        let rec g = function
          | (e :: r, w) -> transf(e, fun v -> g(r, v :: w))
          | ([], w) -> c <| rev w
        in g(a, [])
    in match e_kind with
    | L.Var(x) -> c (Var x)
    | L.Fn(v, e) ->
      let f = Common.Var.create ~name: "f" () in
      let k = Common.Var.create ~name: "k" () in
      fix([(f, [v; k], transf(e, fun z -> app(Var k, [z])))],
          c (Var f))
    | L.Fix(fs, e) ->
      let g (h, {L.e_kind = fn}) =
        match fn with
        | L.Fn(v, b) ->
          let w = Common.Var.create ~name: "w" () in
          (h, [v; w], transf(b, fun z -> app(Var w, [z])))
        | _ -> let str = "ToCPS: function definition expected in Fix" in
               CompilerLog.error "%s" str; failwith str
      in fix(map g fs, transf(e, c))
    | L.App({L.e_kind =
        L.Prim(Lang.Lambda.Primop.Control(Common.Primop.Control.CallCC))}, f) ->
      let h = Common.Var.create ~name: "h" () in
      let k = Common.Var.create ~name: "k" () in
      let x = Common.Var.create ~name: "x" () in
      let k' = Common.Var.create ~name: "k'" () in
      let x' = Common.Var.create ~name: "x'" () in
      primop(gethdlr, [], [h],
             [fix([(k, [x], c (Var x));
                   (k', [x'], primop(sethdlr, [Var h], [],
                                     [app(Var k, [Var x])]))],
                  transf(f, fun v -> app(v, [Var k'; Var k])))])
    | L.App({L.e_kind =
        L.Prim(Lang.Lambda.Primop.Control(Common.Primop.Control.Throw))}, e) ->
      let f = Common.Var.create ~name: "f" () in
      let x = Common.Var.create ~name: "x" () in
      let j = Common.Var.create ~name: "j" () in
      transf(e, fun k -> fix([(f, [x; j], app(k, [Var x]))],
                             c (Var f)))
    | L.App({L.e_kind = L.Prim(i)}, e) when cps_value_n i = 1 && arity i = 1 ->
      let w = Common.Var.create ~name: "w" () in
      transf(e, fun v -> primopc(i, [v], [w], [c (Var w)]))
    | L.App({L.e_kind = L.Prim(i)}, {L.e_kind = L.Record(a)})
        when cps_value_n i = 1 && arity i > 1 ->
      let w = Common.Var.create ~name: "w" () in
      transfl(a, fun a -> primopc(i, a, [w], [c (Var w)]))
    | L.App({L.e_kind = L.Prim(i)}, e) when cps_cont_n i > 1 && arity i = 1 ->
      let k = Common.Var.create ~name: "k" () in
      let x = Common.Var.create ~name: "x" () in
      transf(e, fun v -> fix([(k, [x], c (Var x))],
                             primopc(i, [v], [],
                                     [app(Var k, [int 1]);
                                      app(Var k, [int 0])])))
    | L.App({L.e_kind = L.Prim(i)}, {L.e_kind = L.Record(a)})
        when cps_cont_n i > 1 && arity i > 1 ->
      let k = Common.Var.create ~name: "k" () in
      let x = Common.Var.create ~name: "x" () in
      transfl(a, fun a -> fix([(k, [x], c (Var x))],
                              primopc(i, a, [],
                                      [app(Var k, [int 1]);
                                       app(Var k, [int 0])])))
    | L.App({L.e_kind = L.Prim(i)}, e) when cps_value_n i = 0 && arity i = 1 ->
      transf(e, fun v -> primopc(i, [v], [], [c (int 0)]))
    | L.App({L.e_kind = L.Prim(i)}, {L.e_kind = L.Record(a)})
        when cps_value_n i = 0 && arity i > 1 ->
      transfl(a, fun a -> primopc(i, a, [], [c (int 0)]))
    | L.App({L.e_kind = L.Prim(i)}, e) when arity i > 1 -> (* e not a record *)
      transf(record_trim_eta_expand (arity i) (lprim i) e, c)
    | L.App({L.e_kind = L.FFI(name, 0)}, e) ->
      let w = Common.Var.create ~name: "w" () in
      transf(e, fun _ -> ffi(name, [], w, c (Var w)))
    | L.App({L.e_kind = L.FFI(name, 1)}, e) ->
      let w = Common.Var.create ~name: "w" () in
      transf(e, fun v -> ffi(name, [v], w, c (Var w)))
    | L.App({L.e_kind = L.FFI(name, arity)}, {L.e_kind = L.Record(e)}) when arity > 1->
      let w = Common.Var.create ~name: "w" () in
      transfl(e, fun v -> ffi(name, v, w, c (Var w)))
    | L.App({L.e_kind = L.FFI(name, arity)}, e) when arity > 1 -> (* e not a record *)
      transf(record_trim_eta_expand arity (lffi(name, arity)) e, c)
    | L.App(f, e) ->
      let r = Common.Var.create ~name: "r" () in
      let x = Common.Var.create ~name: "x" () in
      fix([(r, [x], c (Var x))],
          transf(f, fun v1 ->
            transf(e, fun v2 ->
              app(v1, [v2; Var r]))))
    | L.Int(i) -> c (Int i)
    | L.Real(r) -> c (Real r)
    | L.String(s) -> c (String s)
    | L.Switch(e) -> 
      let { L.sw_expr ; L.sw_possible_cons ;
            L.sw_con_cases; L.sw_constant_range ;
            L.sw_constant_cases; L.sw_default_case } = e in
      let r = Common.Var.create ~name: "r" () in
      let x = Common.Var.create ~name: "x" () in
      let c' = fun v -> app(Var r, [v]) in
      let d = Common.Var.create ~name: "d" () in
      let default = app(Var d, []) in
      let default_used = ref false in
      let gen_branches : (int * L.expr) list -> int -> expr list =
        fun cases max ->
          let rec repeat_default n =
            if n = 0 then [] else default :: (repeat_default <| n-1) in
          let rec aux k cases = 
            if k > max then []
            else
              match cases with
              | (i, e') :: cs ->
                if k = i then
                  transf(e', c') :: (aux (k+1) cs)
                else
                  (default_used := true;
                   default :: (aux (k+1) cases))
              | [] -> default_used := true;
                      repeat_default (max - k + 1) in
          let cmp c1 c2 =
            match c1, c2 with
            | (i1, _), (i2, _) -> compare i1 i2
          in aux 0 (sort cmp cases) in
      transf(sw_expr, fun v ->
        try
          transf(assoc L.C_Transparent sw_con_cases, c)
        with Not_found ->
          let boxed_cont =
            try
              transf(assoc L.C_TransB sw_con_cases, c')
            with Not_found ->
              let max_tag =
                let g con max =
                  match con with
                  | L.C_Tagged(i) -> if i > max then i else max
                  | _ -> max
                in fold_right g sw_possible_cons (-1) in
              let tag_cases =
                let h x rest =
                  match x with
                  | (L.C_Tagged(i), e) -> (i, e) :: rest
                  | _ -> rest
                in fold_right h sw_con_cases [] in
              let branches =
                gen_branches tag_cases max_tag in
              let w = Common.Var.create ~name: "w" ()
              in select(1, v, w, switch(Var w, branches)) in
          let unboxed_cont =
            try
              transf(assoc L.C_TransU sw_con_cases, c')
            with Not_found ->
              match sw_constant_range with
              | Some n ->
                let cases =
                  map (fun (i, e) -> (Int64.to_int i, e)) sw_constant_cases in
                let max = (Int64.to_int n) - 1 in 
                let branches = gen_branches cases max
                in switch(v, branches)
              | None ->                (* switch on integers *)
                let int_branch (j, e') rest =
                  primop(eq, [v; Int j], [], [transf(e', c'); rest])
                in default_used := true;
                   fold_right int_branch sw_constant_cases default in
          let default_fixdef =
            if !default_used then
              match sw_default_case with
              | Some e' -> [(d, [], transf(e', c'))]
              | None -> let str = "ToCPS: a default case was expected" in
                        CompilerLog.error "%s" str; failwith str
            else [] in
          fix((r, [x], c (Var x)) :: default_fixdef,
              if sw_possible_cons = [] ||
                sw_possible_cons = [ L.C_TransU ] then
                unboxed_cont
              else if sw_constant_range = Some 0L &&
                     not <| mem L.C_TransU sw_possible_cons then
                boxed_cont
              else primop(boxed, [v], [], [boxed_cont; unboxed_cont])))
    | L.Con(L.C_Tagged i, e) -> transf(lrecord([e; lint i]), c)
    | L.Con(_, e) -> transf(e, c)
    | L.Decon(L.C_Tagged i, e) -> transf(lselect(0, e), c)
    | L.Decon(_, e) -> transf(e, c)
    | L.SwitchExn(ex, l, d) ->
      let w = Common.Var.create ~name: "w" () in
      let r = Common.Var.create ~name: "r" () in
      let x = Common.Var.create ~name: "x" () in
      let c' = fun v -> app(Var r, [v]) in
      let branch (e1, e2) rest =
        transf(e1, fun n -> primop(eq, [Var w; n], [],
                                   [transf(e2, c'); rest])) in
      let d_cont = transf(d, c') in
      fix([(r, [x], c (Var x))],
          transf(ex, fun r ->
            select(1, r, w, fold_right branch l d_cont)))
    | L.ConExn(e1, e2) -> let w = Common.Var.create ~name: "w" () in
                          transf(e2, fun n ->
                            transf(e1, fun v ->
                              record([(v, Offp 0); (n, Offp 0)], w, c (Var w))))
    | L.ExnValue(e) -> let w = Common.Var.create ~name: "w" () in
                       transf(e, fun r -> select(0, r, w, c (Var w)))
    | L.ExnName(e) -> let w = Common.Var.create ~name: "w" () in
                      transf(e, fun r -> select(1, r, w, c (Var w)))
    | L.Record([]) -> c (Int Int64.zero)
    | L.Record(a) -> let x = Common.Var.create ~name: "x" () in
                     let g v = (v, Offp 0) in
                     transfl(a, fun a -> record(map g a, x, c (Var x)))
    | L.Select(i, e) -> let w = Common.Var.create ~name: "w" () in
                        transf(e, fun v -> select(i, v, w, c (Var w)))
    | L.Raise(e) -> let h = Common.Var.create ~name: "h" () in
                    transf(e, fun w ->
                      primop(gethdlr, [], [h], [app(Var h, [w])]))
    | L.Handle(a, b) ->
      let h = Common.Var.create ~name: "h" () in
      let k = Common.Var.create ~name: "k" () in
      let x = Common.Var.create ~name: "x" () in
      let e = Common.Var.create ~name: "e" () in
      let n = Common.Var.create ~name: "n" () in
      primop(gethdlr, [], [h],
             [fix([(k, [x], c (Var x));
                   (n, [e], primop(sethdlr, [Var h], [],
                                   [transf(b, fun f ->
                                     app(f, [Var e; Var k]))]))],
                  primop(sethdlr, [Var n], [],
                         [transf(a, fun v ->
                           primop(sethdlr, [Var h], [],
                                  [app(Var k, [v])]))]))])
    | L.Prim _ | L.FFI _ ->
      transf(eta_expand lexpr, c)
      
let rec transform : L.expr -> expr =
  fun ({ L.e_tag } as lexpr) ->
    transf (lexpr, fun v -> { e_tag = e_tag; e_kind = App(Var top_cont, [v])})
      
let c_to_cps = Contract.create
  ~description: "Transform program from Lambda to CPS"
  ~languages: [Language.CPS]
  "transform:to_cps"  

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_Lambda
    ~target: Compiler.Lang_CPS
    ~name:   "Lambda:to_cps"
    ~require:
      [ Lang.Lambda.Contracts.switch_well_formed
      ]    
    ~contracts:
      [ c_to_cps
      ]
    ~contract_rules:
      [ Contract.saves_contract Common.Contracts.right_scopes
      ; Contract.saves_contract Common.Contracts.unique_vars
      ]
    transform
