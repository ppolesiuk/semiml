
val infer_type :
  Common.Tag.t ->
  Env.t ->
  Lang.RawMiniML.Ast.arg ->
    Env.t *
    (Lang.MiniML.Ast.expr -> Lang.MiniML.Ast.expr) *
    (Lang.MiniML.Type.t   -> Lang.MiniML.Type.t)

val infer_types :
  Common.Tag.t ->
  Env.t ->
  Lang.RawMiniML.Ast.arg list ->
    Env.t *
    (Lang.MiniML.Ast.expr -> Lang.MiniML.Ast.expr) *
    (Lang.MiniML.Type.t   -> Lang.MiniML.Type.t)

val check_types :
  Common.Tag.t ->
  Env.t ->
  Lang.RawMiniML.Ast.arg list ->
  Lang.MiniML.Type.t ->
    Env.t *
    (Lang.MiniML.Ast.expr -> Lang.MiniML.Ast.expr) *
    Lang.MiniML.Type.t
