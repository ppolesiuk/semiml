
module S = Lang.RawNuL.Ast
module T = Lang.NuL.Ast

module StrMap = Map.Make(String)

let c_scope_check = Contract.create
  ~description: "Scope checking"
  ~languages: [Language.NuL]
  "transform:scope_check"

let extend env x =
  let y = Common.Var.create ~name: x () in
  (StrMap.add x y env, y)

let mk_expr tag kind =
  { T.e_tag  = tag
  ; T.e_kind = kind
  }

let check_var env tag x =
  try mk_expr tag (T.Var (StrMap.find x env)) with
  | Not_found ->
    Errors.error ~tag: tag
      "Unbound variable `%s'." x;
    raise Errors.Fatal_error

let rec check_expr env expr =
  let tag = expr.S.e_tag in
  match expr.S.e_kind with
  | S.Succ  -> mk_expr tag T.Succ
  | S.Var x -> check_var env tag x
  | S.Num n -> mk_expr tag (T.Num n)
  | S.Abs(args, body) ->
    check_abs env tag args body
  | S.App(e1, e2) -> 
    mk_expr tag (T.App(check_expr env e1, check_expr env e2))
  | S.Case(expr, case_fun, case_zero, case_succ) ->
    mk_expr tag (T.Case(
      check_expr env expr,
      check_case_fun  env case_fun,
      check_case_zero env case_zero,
      check_case_succ env case_succ))

and check_abs env tag args body =
  match args with
  | [] -> check_expr env body
  | x :: args ->
    let (env, x) = extend env x.S.id_name in
    mk_expr tag (T.Abs(x, check_abs env tag args body))

and check_case_fun env (x, body) =
  let (env, x) = extend env x.S.id_name in
  (x, check_expr env body)

and check_case_zero env body =
  check_expr env body

and check_case_succ env (x, body) =
  let (env, x) = extend env x.S.id_name in
  (x, check_expr env body)

let check source =
  let env = StrMap.empty in
  check_expr env source

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_RawNuL
    ~target: Compiler.Lang_NuL
    ~name:   "NuL:scope_check"
    ~contracts:
      [ Lang.NuL.Contracts.right_scopes
      ; Lang.NuL.Contracts.unique_vars
      ; c_scope_check
      ]
    check
