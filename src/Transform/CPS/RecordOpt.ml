open Lang.CPS.Ast
open Lang.CPS
open Common.Primop.Repr

exception Not_reduce

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("recordopt::unwrap_label -- application of non function")

let rec reduce expr =
  match expr.e_kind with 
  | Select(i, v, x, e) -> 
    (match e.e_kind with 
    | Select(i',v',x', e') -> Selp(i, reduce e)
    | Record(vl, _, _) -> 
      (try let _,a = List.find (fun (v, a') -> 
        match v with 
        | Var v' -> x = v'
        | Label v' -> x = v'
        | _ -> false
        ) vl in
        Selp(i, a)
      with
      | Not_found -> raise Not_reduce)
    | _ -> raise Not_reduce)
  | _ -> raise Not_reduce
  

let rec subst expr xx path =
  match expr.e_kind with 
  | Select(i, v, x, e) ->
    (match e.e_kind with 
    | Select(i',v',x', e') -> subst e xx path
    | Record(vl, v, c) -> 
      Record(List.map (fun (v, a') -> 
        match v with
        | Var v' | Label v' -> if v' == x then (xx,path) else (v,a')
        | _ -> (v, a')
        ) vl, v ,c)
  | _ -> raise Not_reduce)
  | _ -> raise Not_reduce
  

let transform program = 
let rec trans expr = 
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, trans e)
  | Select(i, v, x, e) -> (try let path = reduce expr in
                              subst expr v path
                          with
                          | Not_reduce -> Select(i, v, x, e))
  | Offset(i, v, x, e) -> Offset(i, v, x, trans e)
  | App(f, xs) -> expr.e_kind
  | Fix(decls, e) -> 
    let reduce_body = fun (fname, vs, body) -> (fname, vs,  trans body) in
    Fix(List.map reduce_body decls, trans e)
  | Switch(v, es) -> Switch(v, List.map trans es)
  | FFI(f, vs, x, e) -> FFI(f, vs, x, trans e)
  | Primop(op, vs, xs, es) -> Primop(op, vs, xs, List.map trans es)
  } 

in trans program 

let c_record_opt = Contract.create 
  ~description: "Record optimization"
  ~languages: [Language.CPS]
  "transform:record_opt"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:record_opt"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_record_opt
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ]
    transform
