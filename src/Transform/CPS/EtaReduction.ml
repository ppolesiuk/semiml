open Lang.CPS.Ast


let printFreeVars set =
  Common.Var.Set.iter (fun x -> print_string (Common.Var.string_of_var x)) set

let find args arg = 
  List.fold_left  (fun a -> fun x -> a || x == arg)  false args

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("EtaReduction::unwrap_label -- application of non function")

let same_vars formals args =
  List.length formals = List.length args &&
  List.for_all2 (fun x v ->
    match v with
    | Var(y) | Label(y) -> Common.Var.equal x y
    | _ -> false
  ) formals args

let replace_var f f' e =
  BetaContraction.replace_vars ([f,f'],[]) e

let replace_in_fix f f' decls =
  List.map (fun (fname, args, body) ->
    (fname, args, replace_var f f' body)
  ) decls

let transform program = 
let rec trans expr =  
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, trans e)
  | Select(i, v, x, e) -> Select(i, v, x, trans e)
  | Offset(i, v, x, e) -> Offset(i, v, x, trans e)
  | App(f, xs) -> expr.e_kind
  | Fix(decls, e) ->
    let rec trans_fix acc decls e =
      match decls with
      | [] -> (List.rev acc, e)
      | (fname, formals, { e_kind = App(fname', args) }) :: decls 
          when same_vars formals args 
            && not (List.memq (unwrap_label fname') (fname :: formals)) ->
        let acc   = replace_in_fix fname fname' acc in
        let decls = replace_in_fix fname fname' decls in
        let e     = replace_var    fname fname' e in
        trans_fix acc decls e
      | (fname, formals, body) :: decls ->
        trans_fix ((fname, formals, trans body) :: acc) decls e
    in
    let (decls, e) = trans_fix [] decls e in
    Fix(decls, trans e)
  | Switch(v, es) -> Switch(v, List.map trans es)
  | FFI(f, vs, x, e) -> FFI(f, vs, x, trans e)
  | Primop(p, vs, xs, es) -> Primop(p, vs, xs, List.map trans es)
  }
in  trans program


let c_eta_reduction = Contract.create 
  ~description: " Eta reduction"
  ~languages: [Language.CPS]
  "transform:eta_reduction"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:eta_reduction"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_eta_reduction
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ]
    transform
