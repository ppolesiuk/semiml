open Lang.CPS.Ast
open List

type fn = (var * var list * expr)

let (<|) f x = f x

let mk_expr tag kind =
  { e_tag  = tag
  ; e_kind = kind
  }

let fn_name : fn -> var =
  fun (name, _, _) -> name

let rec trans : expr -> (expr * fn list) =
  fun {e_tag; e_kind} ->
    let mk kind = mk_expr e_tag kind in
    match e_kind with
    | Fix(fs, e) ->
      let (transE, transFs) = trans e in
      let transFs2 = flatten <| map transFun fs in
      (transE, transFs @ transFs2)
    | FFI(name, vs, x, e) ->
      let (transE, transF) = trans e in
      (mk <| FFI(name, vs, x, transE), transF)
    | App(v, args) -> (mk <| App(v, args), [])
    | Record(els, v, e) ->
      let (transE, transFs) = trans e in
      (mk <| Record(els, v, transE), transFs)
    | Select(i, r, v, e) ->
      let (transE, transFs) = trans e in
      (mk <| Select(i, r, v, transE), transFs)
    | Offset(i, r, v, e) ->
      let (transE, transFs) = trans e in
      (mk <| Offset(i, r, v, transE), transFs)
    | Switch(i, es) ->
      let (transEs, transFs) = transExprs es in
      (mk <| Switch(i, transEs), transFs)
    | Primop(p, vls, vrs, es) ->
      let (transEs, transFs) = transExprs es in
      (mk <| Primop(p, vls, vrs, transEs), transFs)

and transExprs : expr list -> (expr list * fn list) =
  fun es ->
    let acc (es, fs) (e, fs2) = (e::es, fs @ fs2) in
    fold_left acc ([], []) <| map trans es

and transFun : fn -> fn list =
  fun (name, args, body) ->
    let (transE, transFs) = trans body in
    (name, args, transE) :: transFs

let replaceVar : var list -> value -> value =
  fun vars value ->
    match value with
    | Var v -> if mem v vars || v = top_cont then Label v else Var v
    | v -> v

let replaceVarInRecord : var list -> (value * accesspath) -> (value * accesspath) =
  fun vars (value, ap) -> (replaceVar vars value, ap)

let rec varToLabel : var list -> expr -> expr =
  fun vars {e_tag; e_kind} ->
    let mk kind = mk_expr e_tag kind in
    match e_kind with
    | Fix(fs, e) ->
      mk <| Fix(map (replaceVarInFunction vars) fs, varToLabel vars e)
    | App(v, args) ->
      mk <| App(replaceVar vars v, map (replaceVar vars) args)
    | Record(els, v, e) ->
      mk <| Record(map (replaceVarInRecord vars) els, v, varToLabel vars e)
    | Select(i, r, v, e) ->
      mk <| Select(i, r, v, varToLabel vars e)
    | Offset(i, r, v, e) ->
      mk <| Offset(i, r, v, varToLabel vars e)
    | FFI(name, vs, x, e) ->
      mk <| FFI(name, vs, x, varToLabel vars e)
    | Switch(i, es) ->
      mk <| Switch(i, map (varToLabel vars) es)
    | Primop(p, vls, vrs, es) ->
      mk <| Primop(p, map (replaceVar vars) vls, vrs, map (varToLabel vars) es)
and replaceVarInFunction : var list -> fn -> fn =
  fun vars (name, args, body) -> (name, args, varToLabel vars body)

let transform : expr -> expr =
  fun ({e_tag; e_kind} as e) -> let (transE, transFs) = trans e in
    varToLabel (map fn_name transFs) <| mk_expr e_tag (Fix(transFs, transE))

let contract = Contract.create
  ~description: "Lift all function definition to the top level"
  ~languages: [Language.CPS]
  "transform:lift_definitions"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:lift_definitions"
    ~require:
      [ Lang.CPS.Contracts.closure_passing_style ]
    ~contracts:
      [ contract
      ; Lang.CPS.Contracts.definitions_lifted
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.right_scopes
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_passing_style
      ; Contract.saves_contract Lang.CPS.Contracts.unique_vars
      ]
    transform
