open Lang.CPS.Ast
open Lang.CPS
open Common.Primop.Repr
exception Not_reduce

let rec join_path p1 p2 =
  match p1, p2 with
  | Offp i, Offp j -> Offp(i + j)
  | Offp i, Selp(j, path) -> Selp(i + j, path)
  | Selp(i, path), p2 -> Selp(i, join_path path p2)

let rec path_length p =
  match p with
  | Offp _ -> 0
  | Selp(_, p) -> 1 + path_length p

let rec trans_record_elem elem =
  match elem with
  | Var x, Selp(i, path) ->
    begin match MetaData.CPS.RecordInfo.try_get x with
    | None -> elem
    | Some(j, vs) ->
      let index = i + j in
      if index >= 0 && index < List.length vs then begin
        let (v, path') = List.nth vs index in
        let (v, new_path) = trans_record_elem (v, join_path path' path) in
        if path_length new_path <= path_length path then (v, new_path)
        else elem
      end else elem
    end
  | _ -> elem

let rec trans expr =
  let mk_expr kind = { expr with e_kind = kind } in
  let trans_select i y x e =
    match MetaData.CPS.RecordInfo.try_get y with
    | None -> mk_expr (Select(i, Var y, x, trans e))
    | Some(off, vs) ->
      let index = i + off in
      if index >= 0 && index < List.length vs then
        begin match List.nth vs index with
        | v, Offp 0 -> 
          trans (BetaContraction.replace_vars ([x,v],[]) e)
        | v, Offp j ->
          mk_expr (Offset(j, v, x, trans e))
        | v, Selp(j, Offp 0) ->
          trans (mk_expr (Select(j, v, x, trans e)))
        | _ -> mk_expr (Select(i, Var y, x, trans e))
        end
      else mk_expr (Select(i, Var y, x, trans e))
  in
  match expr.e_kind with
  | Record(vs, x, e) ->
    mk_expr (Record(List.map trans_record_elem vs, x, trans e))
  | Select(i, Var y, x, e) -> trans_select i y x e
  | Select(i, v, x, e) ->
    mk_expr (Select(i, v, x, trans e))
  | Offset(0, v, x, e) ->
    trans (BetaContraction.replace_vars ([x,v],[]) e)
  | Offset(i, v, x, e) ->
    mk_expr (Offset(i, v, x, trans e))
  | App(f, xs) -> expr
  | Fix(decls, e) -> 
    let reduce_body (fname, vs, body) = (fname, vs, trans body) in
    mk_expr (Fix(List.map reduce_body decls, trans e))
  | Switch(v, es) ->
    mk_expr (Switch(v, List.map trans es))
  | FFI(f, vs, x, e) ->
    mk_expr (FFI(f, vs, x, trans e))
  | Primop(op, vs, xs, es) ->
    mk_expr (Primop(op, vs, xs, List.map trans es))

let transform program = trans program

let c_sfkr = Contract.create
  ~description: "Selection from known record"
  ~languages: [Language.CPS] 
  "transform:sfkr"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:sfkr"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.RecordInfo.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.RecordInfo.contract
      ; c_sfkr
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.unique_vars
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_passing_style
      ; Contract.saves_contract Analysis.CPS.RecordInfo.contract
      ]
    transform
