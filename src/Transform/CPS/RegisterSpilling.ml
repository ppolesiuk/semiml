open Lang.CPS.Ast
open List

let registers_num = ContractCheck.CPS.RegistersSpilled.registers_num

(* Data structures *)
module VarSet = Common.Var.Set
module VarMap = Common.Var.Map

let set_to_list s = VarSet.fold (fun x xs -> x :: xs) s []


module SpillRecord :
sig
  type t

  val create : Common.Var.t -> Common.Var.t list -> t

  (* Returns the set of variables in the spill record. *)
  val contents : t -> VarSet.t

  (* The ariable that this spill record is bound to. *)
  val var : t -> Common.Var.t

  (* Gets the index of a variable. Throws Not_found if not found. *)
  val get_index : t -> Common.Var.t -> int

  val try_get_index : t -> Common.Var.t -> int option
end
=
struct
  type t = Common.Var.t * int VarMap.t

  let create s vs =
    (s, fold_left (fun vm (v, i) -> VarMap.add v i vm) (VarMap.empty) (mapi (fun i v -> (v, i)) vs))
  let contents (_, vm) = VarMap.fold (fun k _ vs -> VarSet.add k vs) vm (VarSet.empty)
  let var (s, _) = s
  let get_index (_, vm) v = VarMap.find v vm

  let try_get_index ((_, vm) as s) v =
    if VarMap.mem v vm then
      Some(get_index s v)
    else
      None
end

let rec get_access_paths s variables =
  match variables with
  | v :: vs ->
      ( match SpillRecord.try_get_index s v with
      | Some(i) -> Some(Selp(i, Offp(0)))
      | None -> None
      ) :: get_access_paths s vs
  | [] -> []

module Registers :
sig
  type t

  (* Empty registers object. *)
  val empty : int -> t

  (* Truncates the registers contents to a given set of variables *)
  val truncate_to : t -> VarSet.t -> t

  (* Tells how much unoccupied registers is there left *)
  val place_left : t -> int

  (* Check whether a given set of variables will fit in the registers *)
  val will_fit : t -> VarSet.t -> bool

  (* Returns the spill_record, if any *)
  val spill_record : t -> SpillRecord.t option

  val contents : t -> VarSet.t

  val contents_no_spill : t -> VarSet.t

  val unique : t -> VarSet.t

  val duplicates : t -> VarSet.t

  (* Puts a set of variables in the registers.
   * !!! NO CHECKING IF IT FITS IS DONE !!! *)
  val add : t -> VarSet.t -> t

  (* add_one x = add (VarSet.singleton x) *)
  val add_one : t -> VarSet.elt -> t

  (* Updates unique and duplicate sets. Also, puts the pointer to the spill
   * record in the unique set. *)
  val apply_new_spill : t -> SpillRecord.t -> t
end
=
struct
  type t =
    { spill_record : SpillRecord.t option
    ; unique : VarSet.t
    ; duplicates : VarSet.t
    ; n : int
    }

  let empty n =
    { spill_record = None
    ; unique = VarSet.empty
    ; duplicates = VarSet.empty
    ; n = n
    }

  let unique r = r.unique
  let duplicates r = r.duplicates
  let spill_record r = r.spill_record

  let cleanup r needed =
    match r.spill_record with
    | None -> r
    | Some(s) ->
      let needed_in_spillrec = VarSet.inter (SpillRecord.contents s) needed in
      let only_in_spillrec = VarSet.diff needed_in_spillrec r.duplicates in
      if VarSet.is_empty only_in_spillrec then
        { r with
          spill_record = None
        ; unique =
            VarSet.remove (SpillRecord.var s) (VarSet.union r.unique r.duplicates)
        ; duplicates = VarSet.empty
        }
      else
        r

  let truncate_to r vs =
    cleanup
      { r with
        unique = VarSet.inter r.unique vs
      ; duplicates = VarSet.inter r.duplicates vs
      }
      vs

  let contents r =
    VarSet.union r.unique r.duplicates

  let contents_no_spill r =
    match spill_record r with
    | None -> contents r
    | Some(s) -> VarSet.remove (SpillRecord.var s) (contents r)

  let place_left r =
      r.n - (VarSet.cardinal r.unique + VarSet.cardinal r.duplicates)

  (* This is a very dumb way of shrinking the duplicates. It just takes
   * whatever there is and throws it out until the set is of the right size. *)
  let rec shrink n ds =
    match n with
    | 0 -> ds
    | n' -> shrink (n' - 1) (VarSet.remove (VarSet.choose ds) ds)

  let fit r =
    let to_convict = -(place_left r) in
    if to_convict > 0 then
      { r with duplicates = shrink to_convict r.duplicates }
    else
      r

  let apply_new_spill r s =
    fit
      { r with
        spill_record = Some(s)
      ; unique = VarSet.singleton (SpillRecord.var s)
      ; duplicates = VarSet.union r.unique r.duplicates
      }


  let add r vs =
    let spill_contents =
      match r.spill_record with
      | Some(s) -> SpillRecord.contents s
      | None -> VarSet.empty
    in
    let (d_vs, u_vs) =
      VarSet.partition (fun x -> VarSet.mem x spill_contents) vs
    in fit
      { r with
        unique = VarSet.union r.unique u_vs
      ; duplicates = VarSet.union r.duplicates d_vs
      }

  let add_one r v =
    let spill_contents =
      match r.spill_record with
      | Some(s) -> SpillRecord.contents s
      | None -> VarSet.empty
    in
    let (u, d) =
      if VarSet.mem v spill_contents then
        (r.unique, VarSet.add v r.duplicates)
      else
        (VarSet.add v r.unique, r.duplicates)
    in fit { r with unique = u; duplicates = d }

  let will_fit r vs =
    place_left r >= VarSet.cardinal (VarSet.diff vs (contents r))

end

  ;;

(* Utils *)
let get_opt = function Some(x) -> x | _ -> failwith "Empty Option value"

let get_fv expr = MetaData.CPS.FreeVars.get expr

let unwrap_singleton xs =
  match xs with
  | [x] -> x
  | _ -> failwith "Single element list expected (unwrap_singleton)."

let change_continuations expr_kind cs =
  match expr_kind with
  | Record(x, y, _) -> Record(x, y, unwrap_singleton cs)
  | Select(x, y, z, _) -> Select(x, y, z, unwrap_singleton cs)
  | Offset(x, y, z, _) -> Offset(x, y, z, unwrap_singleton cs)
  | Switch(x, _) -> Switch(x, cs)
  | FFI(x, y, z, _) -> FFI(x, y, z, unwrap_singleton cs)
  | Primop(x, y, z, _) -> Primop(x, y, z, cs)
  | App(_, _) as app -> (
      match cs with
      | [] -> app
      | _ -> failwith "Non-empty list of continuations passed to Lang.CPS.Ast.App"
    )
  | Fix(_, _) -> failwith "Non toplevel Fix found."

let flip f = fun x y -> f y x

(* Substitutes one variable to another in an expression. *)
let rec subst expr v_from v_to =
  let subst' x =
    match x with
    | Var(v) when v = v_from -> Var(v_to)
    | x -> x
  in
  let new_kind =
    match expr.e_kind with
    | Record(vas, x, y) ->
        let access_paths = map fst vas |> map subst' |> (flip combine) (map snd vas) in
        Record(access_paths, x, y)
    | Select(x, v, y, z) -> Select(x, subst' v, y, z)
    | Offset(x, v, y, z) -> Offset(x, subst' v, y, z)
    | Switch(v, x) -> Switch(subst' v, x)
    | App(v, vs) ->
        App(subst' v, map subst' vs)
    | FFI(f, vs, y, z) -> FFI(f, map subst' vs, y, z)
    | Primop(x, vs, y, z) ->
        Primop(x, map subst' vs, y, z)
    | Fix(_, _) -> failwith "Non toplevel Fix found"
  in
  { expr with e_kind =
    map (fun c -> subst c v_from v_to) (get_continuations expr)
    |> change_continuations new_kind
  }

let create_new_spill regs =
  let s' = Common.Var.create ~name: "s" ()
  and vs = set_to_list (Registers.contents_no_spill regs) in
  let access_paths =
    match Registers.spill_record regs with
    | Some(s) ->
        get_access_paths s vs
        |> map (function Some(p) -> p | None -> Offp(0))
        |> combine (map (fun x -> Var(x)) vs)
    | None ->
        map (fun _ -> Offp(0)) vs
        |> combine (map (fun x -> Var(x)) vs)
  and regs' =
    Registers.apply_new_spill regs (SpillRecord.create s' vs)
  in
  (s', access_paths, regs')

let get_var_args expr =
  get_args expr
  |> filter (fun x -> match x with | Var(_) -> true | _ -> false)
  |> map (function | Var(v) -> v | _ -> assert false)

let sanity_check var_args =
  if VarSet.cardinal var_args >= registers_num - 1 then
    failwith "Too many arguments to fit in the registers."
  else
    ()

let rec spill ~(regs : Registers.t) expr =
  let free_variables = get_fv expr.e_tag
  and var_args = VarSet.of_list (get_var_args expr) in
  let _ = sanity_check var_args in
  let results = VarSet.of_list (get_results expr) in
  let regs = Registers.truncate_to regs free_variables in
  let need_new_spill =
       not (Registers.will_fit regs var_args)
    || not (Registers.will_fit regs results)
  in
    { expr with e_kind =
      if need_new_spill then
        let (s, access_paths, regs) = create_new_spill regs
        in Record(access_paths, s, spill expr ~regs: (Registers.add_one regs s))
      else
        let f = VarSet.diff var_args (Registers.contents regs) in
        if VarSet.is_empty f then
          (* No fetches needed. Everything is already in registers. *)
          get_continuations expr
          |> map (fun c -> spill c ~regs: (Registers.add regs results))
          |> change_continuations expr.e_kind
        else
          (* We need to fetch. *)
          let v = VarSet.choose f in
          let v' = Common.Var.create ~name: ("fetched_" ^ (Common.Var.name v)) ()
          and spill_record = Registers.spill_record regs |> get_opt in
          let expr_with_subst = (subst expr v v') in
          let _ = Analysis.CPS.FreeVars.analyse expr_with_subst in
          Select(
            SpillRecord.get_index spill_record v,
            Var(SpillRecord.var spill_record),
            v',
            spill expr_with_subst ~regs: (Registers.add_one regs v')
          )
    }

let rec take n xs =
  match (n, xs) with
  | (0, ys) -> ys
  | (n, y :: ys) -> take (n - 1) ys
  | _ -> failwith "List too short (take)."

let transform expr =
  if ContractCheck.CPS.RegistersSpilled.check_program expr then
    expr
  else
    let empty_regs = Registers.empty registers_num in
    let spill' args fun_body =
      spill
        fun_body
        ~regs: (Registers.add empty_regs (VarSet.of_list args))
    in
    match expr.e_kind with
    (* Skipping the toplevel fix here *)
    | Fix(v_vl_e, e) ->
      { expr with e_kind =
        let bindings = map (fun (name, args, body) -> (name, args, spill' args body)) v_vl_e
        and continuation = spill' [] e
        in Fix(bindings, continuation)
      }
    | _ -> spill empty_regs expr

let contract = Contract.create
  ~description:
    "Reduce number of live variables of each expression"
  ~languages: [Language.CPS]
  "transform:register_spilling"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:register_spilling"
    ~require:
      [ Lang.CPS.Contracts.closure_passing_style
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.FreeVars.contract
      ; Lang.CPS.Contracts.definitions_lifted
      ]
    ~contracts: 
      [ contract
      ; Lang.CPS.Contracts.registers_spilled 
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_passing_style
      ; Contract.saves_contract Lang.CPS.Contracts.definitions_lifted
      ; Contract.saves_contract Lang.CPS.Contracts.right_scopes
      ; Contract.saves_contract Lang.CPS.Contracts.unique_vars
      ]
    transform
