open Lang.CPS.Ast
open MetaData.CPS.FlatInfo

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("Flatten::unwrap_label -- application of non function")

let (<<) f g x = f(g(x))

let transform program = 
let rec trans expr = { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, trans e)
  | Select(i, v, x, e) -> Select(i, v, x, trans e)
  | Offset(i, v, x, e) -> Offset(i, v, x, trans e)
  | App(Var(f), xs) -> 
        (match MetaData.CPS.FlatInfo.try_get f with
          | Some(FNinfo({arity=al; alias=  { contents =  Some(f')};_}))->
              let rec loop arg = 
                match arg with
                | ((COUNT(cnt, _))::r, v::vl, args) ->
                  let rec g i args = 
                    if i == cnt then
                      loop(r,vl,args)
                    else
                      let z = Common.Var.create() in
                       Select(i,v,z, { e_tag = Common.Tag.fresh(); e_kind = (g (i+1)  ((Var(z))::args))})
                  in g 0 args
                | (_::r,v::vl,args) -> loop(r,vl,v::args)
                | (_,_,args) -> App(Var(f'), List.rev args)
              in loop(!al,  xs, []) 
          | _ -> App(Var(f), xs)
        ) 
  | App(f, xs) ->  App(f, xs)
  | Fix(decls, e) -> 
      let rec vars n l = 
        match n with
        | 0 -> l
        | n -> vars (n-1) ((Common.Var.create())::l)
      in
      let rec newargs l vl =
        match l, vl with
        | (COUNT(j,_))::r, v::v' -> 
          let b = vars j  [] in
            let vl',bt' = newargs r v' in
            let bodytransform body = 
              {e_tag = Common.Tag.fresh();
               e_kind = Record(List.map (fun x-> (Var(x), Offp(0))) b, v, body)}
          in (b@vl', bodytransform << bt')
        | _::r, v::vl -> 
          let vl', bt' = newargs r vl in
            (v::vl', bt')
        | _ -> ([],fun x->x)
      in
      let rec process_args args =
        match args with
        | (fname, vl, body)::rest -> 
          (match MetaData.CPS.FlatInfo.try_get fname with
            | Some(FNinfo({arity=al; alias=  { contents =  Some(f')};_}))->
                let nargs, bt = newargs !al vl in
                let wl = List.map Common.Var.copy vl in
                (fname,wl,{e_tag = Common.Tag.fresh();
                           e_kind = App(Var(fname), List.map (fun x ->Var(x) ) wl)})::
                                      ((f', nargs, bt body) ::(process_args rest))
            | _ -> (fname, vl, body)::(process_args rest))
        | [] -> []
      in
      let reduce_body = fun (f, vl, body) -> (f, vl, trans body) in
      Fix(List.map reduce_body (process_args decls), trans e)
  | Switch(v, es) -> Switch(v, List.map trans es)
  | FFI(f, vs, x, e) -> FFI(f, vs, x, trans e)
  | Primop(p, vs, xs, es) -> Primop(p, vs, xs, List.map trans es)
  } 
in trans program

let c_flatten = Contract.create 
  ~description: "Flattening function arguments"
  ~languages: [Language.CPS]
  "transform:flatten"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:flatten"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
        ; Analysis.CPS.FlattenInfo.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.FlattenInfo.contract
      ; c_flatten
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Analysis.CPS.FlattenInfo.contract
      ]
    transform
