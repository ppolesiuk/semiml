
val transform : Lang.CPS.Ast.expr -> Lang.CPS.Ast.expr
val c_beta_expansion : Contract.t

val register : unit -> unit
