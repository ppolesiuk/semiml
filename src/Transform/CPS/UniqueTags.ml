open Lang.CPS.Ast

let used_tags = Hashtbl.create 32

let unique_tag tag =
  if Hashtbl.mem used_tags tag then begin
    let tag = MetaData.Tags.copy tag in
    Hashtbl.add used_tags tag ();
    tag
  end else begin
    Hashtbl.add used_tags tag ();
    tag
  end

let rec tr_expr expr =
  { e_tag  = unique_tag expr.e_tag
  ; e_kind =
    match expr.e_kind with
    | Record(vs, x, e) -> Record(vs, x, tr_expr e)
    | Select(n, v, x, e) -> Select(n, v, x, tr_expr e)
    | Offset(n, v, x, e) -> Offset(n, v, x, tr_expr e)
    | App _ as kind -> kind
    | Fix(defs, e) ->
      let defs = List.map (fun (x, xs, body) -> (x, xs, tr_expr body)) defs in
      Fix(defs, tr_expr e)
    | Switch(v, es) -> Switch(v, List.map tr_expr es)
    | FFI(name, args, ret, cont) -> FFI(name, args, ret, tr_expr cont)
    | Primop(op, vs, xs, es) -> Primop(op, vs, xs, List.map tr_expr es)
  }

let transform expr =
  Hashtbl.reset used_tags;
  tr_expr expr

let c_unique_tags = Contract.create
  ~description: "Restore unique_tags contract"
  ~languages: [Language.CPS]
  "transform:unique_tags"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:unique_tags"
    ~weight: 0.1
    ~contracts:
      [ Lang.CPS.Contracts.unique_tags
      ; c_unique_tags
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.right_scopes
      ; Contract.saves_contract Lang.CPS.Contracts.unique_vars
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_passing_style
      ; Contract.saves_contract Lang.CPS.Contracts.definitions_lifted
      ]
    transform
