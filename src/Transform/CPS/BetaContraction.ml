open Lang.CPS.Ast

let fst3 (a,b,c) = a

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("BetaContraction::unwrap_label -- application of non function")

let rec replace_val (val_map, var_map) v = match v with
  | Label(x) ->
    if List.mem_assoc x val_map
    then List.assoc x val_map
    else if List.mem_assoc x var_map
    then Label(List.assoc x var_map)
    else v
  | Var(x) ->
    if List.mem_assoc x val_map
    then List.assoc x val_map
    else if List.mem_assoc x var_map
    then Var(List.assoc x var_map)
    else v
  | other -> other

let add_var_to_args_map (val_map, var_map) x y =
  let var_map' = (x, y) :: var_map
  in (val_map, var_map')

let add_val_to_args_map (val_map, var_map) x y =
  let val_map' = (x, y) :: val_map
  in (val_map', var_map)

let rec replace_vars args_map expr =
  let rec repl_val = replace_val args_map
  and replace_expr e_kind = match e_kind with
    | Record(vs, x, e) ->
      (*
      * What is accesspath precisely and should we do anything with it here?
      *)
      let vs' = List.map (fun (v, ap) -> (repl_val v, ap)) vs
      in let x' = Common.Var.copy x
      in let args_mapping' = add_var_to_args_map args_map x x'
      in let e' = replace_vars args_mapping' e
      in Record(vs', x', e')
    | Select(i, v, x, e) ->
      let v' = repl_val v
      in let x' = Common.Var.copy x
      in let args_mapping' = add_var_to_args_map args_map x x'
      in let e' = replace_vars args_mapping' e
      in Select(i, v', x', e')
    | Offset(i, v, x, e) ->
      let v' = repl_val v
      in let x' = Common.Var.copy x
      in let args_mapping' = add_var_to_args_map args_map x x'
      in let e' = replace_vars args_mapping' e
      in Offset(i, v', x', e')
    | App(f, xs) ->
      let f' = repl_val f
      in let xs' = List.map repl_val xs
      in App(f', xs')
    | Fix(decls, e) ->
      let old_names = List.map fst3 decls in
      let new_names = List.map Common.Var.copy old_names in
      let args_map' =
        List.fold_left2 add_var_to_args_map args_map old_names new_names in
      let decls' = List.mapi (fun i (name, formals, body) ->
        let name' = List.nth new_names i in
        let formals' = List.map Common.Var.copy formals in
        let args_map'' =
          List.fold_left2 add_var_to_args_map args_map' formals formals'
        in (name', formals', replace_vars args_map'' body)) decls
      in let e' = replace_vars args_map' e
      in Fix(decls', e')
    | FFI(name, vs, x, e) ->
      let vs' = List.map repl_val vs in
      let x' = Common.Var.copy x in
      let args_mapping' = add_var_to_args_map args_map x x' in
      let e' = replace_vars args_mapping' e in
      FFI(name, vs', x', e')
    | Switch(v, es) ->
      let v' = repl_val v
      and es' = List.map (replace_vars args_map) es
      in Switch(v', es')
    | Primop(p, vs, xs, es) ->
      let vs' = List.map repl_val vs
      in let xs' = List.map Common.Var.copy xs
        (* We don't really need to add all (xs,xs') to all continuations,
         * each continuation needs only one pair from such list *)
      in let args_map' = List.fold_left2 add_var_to_args_map args_map xs xs'
      in let es' = List.map (replace_vars args_map') es
      in Primop(p, vs', xs', es')
  in { e_kind = replace_expr expr.e_kind; e_tag = Common.Tag.fresh () }

let rec increase_inline_count new_count expr =
  let do_recursively () = match expr.e_kind with
    | Record(_, _, e)
    | Select(_, _, _, e)
    | FFI(_, _, _, e)
    | Offset(_, _, _, e) -> increase_inline_count new_count e
    | Switch(_, es)
    | Primop(_, _, _, es) -> List.iter (increase_inline_count new_count) es
    | App(_, _) -> ()
    | Fix(decls, e) -> begin
        List.iter (fun (_,_,b) -> increase_inline_count new_count b) decls;
        increase_inline_count new_count e;
      end
  in begin
    MetaData.CPS.BetaExpandedCount.set expr.e_tag new_count;
    do_recursively ();
  end

let inlined_function expr = match expr.e_kind with
  | App(f, xs) -> 
    let f_name = unwrap_label f
    in let (body, formals) = MetaData.CPS.FunName2Def.get f_name
    in let val_map = List.combine formals xs
    in let previous_count = MetaData.CPS.BetaExpandedCount.get expr.e_tag
    in let new_body =
      body
      |> replace_vars (val_map,[])
    in begin
      increase_inline_count (previous_count + 1) new_body;
      (* Printf.printf "=== INLINED retrieved body\n";
      Printing.Box.print_stdout (Lang.CPS.Pretty.pretty_program body); 
      Printf.printf "=== INLINED to\n";
      Printing.Box.print_stdout (Lang.CPS.Pretty.pretty_program new_body); 
      Printf.printf "=== INLINED end\n\n"; *)
      new_body.e_kind;
    end
  | _ -> failwith "Transform::CPS::BetaExpansion::inlined_function: Non-App node given"

let rec transform expr =  
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, transform e )
  | Select(i, v, x, e) -> Select(i, v, x, transform e)
  | Offset(i, v, x, e) -> Offset(i, v, x, transform e)
  | App(f, xs) -> if MetaData.CPS.FuncCallCount.get (unwrap_label f) == 1 
                  && not (MetaData.CPS.SplitFunction.try_get (unwrap_label f))
                  && MetaData.CPS.FunctionEscape.known (unwrap_label f) then 
                  begin
                    MetaData.CPS.FuncCallCount.dec (unwrap_label f);
                    MetaData.CPS.CountContraction.inc ();
                    inlined_function expr
                  end
                  else expr.e_kind
  | Fix(decls, e) ->
      let reduce = fun (fname, formals, body) ->
        begin
          let body' = transform body in
          MetaData.CPS.FunName2Def.set fname (body',formals);
          (fname, formals, body')
        end
      in let decls' =  List.map reduce decls in
        let body' = transform e in 
        let decls'' = List.filter (fun (fname, a, s) -> 
                                  MetaData.CPS.FunctionEscape.escaping fname ||
                                  (try MetaData.CPS.FuncCallCount.get fname with
                                    | raise_not_found -> 0) != 0)  decls' in
        if List.length decls'' == 0 then body'.e_kind
      else Fix(decls'', body') 
  | Switch(v, es) -> Switch(v, List.map transform es)
  | FFI(f, vs, x, e) -> FFI(f, vs, x, transform e)
  | Primop(op, vs, xs, es) -> Primop(op, vs, xs, List.map transform es)
  }

let transform expr =
  MetaData.CPS.CountContraction.reset ();
  transform expr

let c_beta_contraction = Contract.create
  ~description: "Beta Contraction"
  ~languages: [Language.CPS] 
  "transform:beta_contraction"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:beta_contraction"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ; Analysis.CPS.FunctionEscape.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ; Analysis.CPS.FunctionEscape.contract
      ; c_beta_contraction
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_passing_style
      ; Contract.saves_contract Analysis.CPS.FunName2Def.contract
      ; Contract.saves_contract Analysis.CPS.FuncCallCount.contract
      ; Contract.saves_contract Analysis.CPS.Var2Def.contract
      ; Contract.saves_contract Analysis.CPS.FunctionEscape.contract
      ]
    transform
