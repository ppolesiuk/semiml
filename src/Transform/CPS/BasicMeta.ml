open Lang.CPS.Ast

let bound = 100

let betacontraction program = 
  begin
    Analysis.CPS.FunName2Def.clean_analyse program;
    Analysis.CPS.FuncCallCount.clean_analyse program;
    Analysis.CPS.Var2Def.clean_analyse program;
    Analysis.CPS.FunctionEscape.clean_analyse program;
    BetaContraction.transform program
  end

let recordopt program = 
  begin
    RecordOpt.transform program
  end

let sfkr program = 
  begin
    Analysis.CPS.RecordInfo.clean_analyse program;
    Sfkr.transform program
  end

let folding program =
  begin
    Folding.transform program
  end

let bis program = 
  begin
    Bis.transform program
  end

let flatten program = 
  begin
    Analysis.CPS.FlattenInfo.clean_analyse program;
    Flatten.transform program
  end

let deadvarelim program =
  begin
    Analysis.CPS.VarUseCount.clean_analyse program;
    DeadVarElim.transform program
  end

let dropunusedarg program =
  begin
    Analysis.CPS.FunctionEscape.clean_analyse program;
    Analysis.CPS.FunName2Def.clean_analyse program;
    Analysis.CPS.VarUseCount.clean_analyse program;
    DropUnusedArg.transform program
  end

let phase1 program =
    let p1 = betacontraction program in
    let p2 = recordopt p1 in
    let p3 = sfkr p2 in
    let p4 = folding p3 in
    let p5 = flatten p4 in
    let p6 = deadvarelim p5 in
    let p7 = dropunusedarg p6 in
    let p8 = bis p7
  in p8

let etasplit program = 
  begin
    EtaSplit.transform program
  end

let etareduction program = 
  begin
    EtaReduction.transform program
  end

let uncurring program =
  begin
    Uncurring.transform program
  end

let phase2 program =
    let p1 = etasplit program in
    let p2 = etareduction p1 in
    let p3 = uncurring p2 in
    p3


let bound = 100
let contraction = 1

let rec loop n program =
  let count = MetaData.CPS.CountContraction.get() in 
  if n > bound && count < contraction 
  then 
    phase1 program 
  else
    let p1 = phase1 program in
    let p2 = phase2 p1 in
  loop (n+1) p2

let transform expr = loop 0 expr

let c_basicmeta = Contract.create
  ~description: "Meta-transformation responsible for invoking basic optimization"
  ~languages: [Language.CPS]
  "transform:basicmeta"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:basicmeta"
    ~weight: 10.0
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_basicmeta
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ]
    transform
