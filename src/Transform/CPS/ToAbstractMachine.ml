open Lang.AbstractM.Ast
open List
module C = Lang.CPS.Ast
module P = Common.Primop
module M = Map.Make(Common.Var)
module H = Hashtbl

let (<|) f x = f x
let (<~) f g x = f(g(x))

type v2r_t = reg M.t * int

let empty_v2r : v2r_t =
  (M.empty, 0)

let get_v2r (m, _) v : value =
  if M.mem v m
    then Reg (M.find v m)
    else begin
      (* CompilerLog.debug "no value for %s" (Common.Var.string_of_var v); *)
      Label v
    end

let next_reg_v2r (_, n) : reg =
  reg_by_id n

let map_next_v2r (m, n) v : v2r_t =
  (M.add v (reg_by_id n) m, succ n)

let wrap expr instr_kind =
  { i_tag = expr.C.e_tag
  ; i_kind = instr_kind
  }

let rec get_expr_alloc_size expr =
  match expr.C.e_kind with
  | C.Record(vs, x, e)   -> get_expr_alloc_size e + 4 * length vs
  | C.Select(n, v, x, e) -> get_expr_alloc_size e
  | C.Offset(n, v, x, e) -> get_expr_alloc_size e
  | C.App(f, vs)         -> 0
  | C.Fix(defs, e)       -> get_expr_alloc_size e
  | C.Switch(v, es)      ->
    fold_left (fun a e -> max a (get_expr_alloc_size e)) 0 es
  | C.FFI(f, vs, x, e) ->
    get_expr_alloc_size e
  | C.Primop(op, vs, xs, es) ->
    fold_left (fun a e -> max a (get_expr_alloc_size e)) 0 es

let rec tr_accesspath ap =
  match ap with
  | C.Offp n       -> Offp n
  | C.Selp (n, ap) -> Selp (n, tr_accesspath ap)

let tr_value v2r value =
  match value with
  | C.Var v    -> get_v2r v2r v
  | C.Label v  -> Label v
  | C.Int n    -> Immediate n
  | C.Real r   -> Errors.not_implemented "tr_value Real"
  | C.String s -> Errors.not_implemented "tr_value String"

let tr_arith_primop op vs rs ls =
  let open Common.Primop.Arith in
  match op with
  | Add | Sub | Mul | Div | Mod
  | RShift | LShift | Orb | Andb | Xorb ->
    begin match (vs, rs, ls) with
    | ([v1 ; v2], [r], []) ->
      begin match op with
      | Add    -> [ IPrimop (Addt, v1, v2, r) ]
      | Sub    -> [ IPrimop (Subt, v1, v2, r) ]
      | Mul    -> [ IPrimop (Mult, v1, v2, r) ]
      | Div    -> [ IPrimop (Divt, v1, v2, r) ]
      | Mod    -> [ IPrimop (Modt, v1, v2, r) ]
      | RShift -> [ IPrimop (Ashr, v1, v2, r) ]
      | LShift -> [ IPrimop (Ashl, v1, v2, r) ]
      | Orb    -> [ IPrimop (Orb, v1, v2, r) ]
      | Andb   -> [ IPrimop (Andb, v1, v2, r) ]
      | Xorb   -> [ IPrimop (Xorb, v1, v2, r) ]
      | _ ->
        CompilerLog.error "binary arith primop";
        failwith ""
      end
    | _ ->
      CompilerLog.error "binary arith primop";
      failwith ""
    end
  | Neg | Notb ->
    begin match (vs, rs, ls) with
    | ([v], [r], []) ->
      begin match op with
      | Neg  -> [ IPrimopU (Neg, v, r) ]
      | Notb -> [ IPrimopU (Notb, v, r) ]
      | _ ->
        CompilerLog.error "unary arith primop";
        failwith ""
      end
    | _ ->
      CompilerLog.error "unary arith primop";
      failwith ""
    end
  | Lt | Le | Gt | Ge ->
    begin match (vs, rs, ls) with
    | ([v1 ; v2], [], [l]) ->
      begin match op with
      | Lt -> [ IBranch (Lt, v1, v2, Label l) ]
      | Le -> [ IBranch (Le, v1, v2, Label l) ]
      | Gt -> [ IBranch (Gt, v1, v2, Label l) ]
      | Ge -> [ IBranch (Ge, v1, v2, Label l) ]
      | _ ->
        CompilerLog.error "cond arith primop";
        failwith ""
      end
    | _ ->
      CompilerLog.error "cond arith primop";
      failwith ""
    end
  | RangeCheck ->
    Errors.not_implemented "tr_arith_primop RangeCheck"

let tr_farith_primop op vs rs ls =
  Errors.not_implemented "tr_farith_primop"

let tr_mem_primop op vs rs ls =
  Errors.not_implemented "tr_mem_primop"

let tr_repr_primop op vs rs ls =
  Errors.not_implemented "tr_repr_primop"

let tr_exn_primop op vs rs ls =
  Errors.not_implemented "tr_exn_primop"

let tr_primop op vs rs ls =
  let open Lang.CPS.Primop in
  match op with
  | Arith op  -> tr_arith_primop op vs rs ls
  | FArith op -> tr_farith_primop op vs rs ls
  | Mem op    -> tr_mem_primop op vs rs ls
  | Repr op   -> tr_repr_primop op vs rs ls
  | Exn op    -> tr_exn_primop op vs rs ls

let rec tr_expr expr v2r =
  match expr.C.e_kind with
  | C.Record(vs, x, e) ->
    [ wrap expr <| Record (
      map (fun (v, ap) -> (tr_value v2r v, tr_accesspath ap)) vs,
      next_reg_v2r v2r)
    ] @ tr_expr e (map_next_v2r v2r x)
  | C.Select(n, v, x, e) ->
    [ wrap expr <| Select (n, tr_value v2r v, next_reg_v2r v2r)
    ] @ tr_expr e (map_next_v2r v2r x)
  | C.Offset(n, v, x, e) ->
    [ wrap expr <| Offset (n, tr_value v2r v, next_reg_v2r v2r)
    ] @ tr_expr e (map_next_v2r v2r x)
  | C.App(f, vs) ->
    let trans = H.create 16 in (* regs to transfer *)
    let usage = H.create 16 in (* regs that are used in RHS *)
    let safe = ref [] in (* regs that are not used in RHS *)
    let res = ref [] in  (* resulting command list (reversed) *)
    let jmpval = ref (tr_value v2r f) in
    iteri begin fun i v ->
      let lhs = reg_by_id i in
      let rhs = tr_value v2r v in
      if rhs <> Reg lhs then
        H.add trans lhs rhs
    end vs;
    begin match !jmpval with
    | Reg r ->
      let len = length vs in
      if r < (reg_by_id len) then
        (* we need to move jmpval so that it doesn't get clobbered *)
        H.add trans (reg_by_id len) (Reg r);
        jmpval := Reg (reg_by_id len)
    | _ -> ()
    end;
    H.iter begin fun _ v ->
      match v with
      | Reg r -> H.add usage r ()
      | _ -> ()
    end trans;
    H.iter begin fun r _ ->
      if not (H.mem usage r) then
        safe := r :: !safe
    end trans;
    while H.length trans > 0 do
      while !safe <> [] do
        let r = hd !safe in
        let v = H.find trans r in
        res := Mov (v, r) :: !res;
        safe := tl !safe;
        H.remove trans r;
        match v with
        | Reg r ->
          H.remove usage r;
          if not (H.mem usage r) && (H.mem trans r) then
            safe := r :: !safe
        | _ -> ()
      done;
      if H.length trans > 0 then
        (* we need to break a cycle now *)
        match H.fold (fun k v _ -> Some (k, v)) trans None with
        | Some (rdest, Reg rsrc) ->
          let tmpreg = reg_by_id (-1) in
          res := Mov (Reg rsrc, tmpreg) :: !res;
          H.replace trans rdest (Reg tmpreg);
          H.add usage tmpreg ();
          H.remove usage rsrc;
          if not (H.mem usage rsrc) && (H.mem trans rsrc) then
            safe := rsrc :: !safe
          else (
            CompilerLog.error "reg cycle break failed";
            failwith ""
          )
        | _ ->
          CompilerLog.error "reg cycle break failed";
          failwith ""
    done;
    rev_map (wrap expr) !res
    @ [ wrap expr <| Jmp !jmpval ]
  | C.Fix(defs, e) ->
    tr_expr e v2r
    @ [ wrap expr <| Unreachable ]
    @ concat (map
      begin fun (y, xs, e) ->
        [ wrap expr <| DefineLabel y
        ; wrap expr <| CheckLimit (get_expr_alloc_size e)
        ] @ tr_expr e (fold_left map_next_v2r empty_v2r xs)
      end defs)
  | C.Switch(v, es) ->
    Errors.not_implemented "tr_expr Switch"
  | C.FFI(f, vs, e, es) ->
    Errors.not_implemented "tr_expr FFI"
  | C.Primop(op, vs, xs, es) ->
    let (rs, v2r') = fold_left
      begin fun (rs, v2r') v ->
        (next_reg_v2r v2r' :: rs, map_next_v2r v2r' v)
      end ([], v2r) xs in
    let ls = (* ls just contains (length es)-1 new labels *)
      begin match es with
      | [] -> []
      | _ :: es -> mapi
        begin fun i _ ->
          Common.Var.create ~name: ("br" ^ (string_of_int i)) ()
        end es
      end in
    let primexpr = map (wrap expr)
      (tr_primop op (map (tr_value v2r) vs) rs ls) in
    begin match es with
    | []  -> primexpr @ [ wrap expr <| Halt ]
    | [e] -> primexpr @ tr_expr e v2r'
    | [e1 ; e2] ->
      primexpr
      @ tr_expr e2 v2r'
      @ [ wrap expr <| DefineLabel (hd ls) ]
      @ tr_expr e1 v2r'
    | _ ->
      CompilerLog.error "ternary primop";
      failwith ""
    end

let tr_expr_top expr v2r =
  match expr.C.e_kind with
  | C.Fix(defs, e) ->
    tr_expr e v2r
    @ [ wrap expr <| Unreachable ]
    @ concat (map
      begin fun (y, xs, e) ->
        [ wrap expr <| DefineLabel y
        ; wrap expr <| BeginStdFn
        ; wrap expr <| CheckLimit (get_expr_alloc_size e)
        ] @ tr_expr e (fold_left map_next_v2r empty_v2r xs)
      end defs)
  | _ ->
    tr_expr expr v2r

let transform : C.expr -> prog =
  fun e -> tr_expr_top e empty_v2r

let c_to_abstract_machine = Contract.create
  ~description: "Transform program from CPS to Abstract Machine"
  ~languages: [Language.AbstractM]
  "transform:to_abstract_machine"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_AbstractM
    ~name:   "CPS:to_abstract_machine"
    ~require:
      [ Lang.CPS.Contracts.closure_passing_style
(*      ; Lang.CPS.Contracts.registers_spilled *)
      ; Lang.CPS.Contracts.unique_vars
      ]
    ~contracts:
      [ c_to_abstract_machine
      ]
    ~contract_rules:
      [ Contract.saves_contract Common.Contracts.unique_tags
      ]
    transform
