open Lang.CPS.Ast

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("BetaContraction::unwrap_label -- application of non function")

let transform program = 
let rec trans expr = 
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, trans e)
  | Select(i, v, x, e) -> Select(i, v, x, trans e)
  | Offset(i, v, x, e) -> Offset(i, v, x, trans e)
  | App(f, xs) -> expr.e_kind
  | Fix(decls, e) -> 
    let reduce = fun (fname, formals, body)->
      let formals' = List.map Common.Var.copy formals in
      let fname' = Common.Var.copy fname in
      MetaData.CPS.SplitFunction.set fname';
      ( match body.e_kind with
        | Fix([(g,[b;k], a)], cexp) ->
           (match cexp.e_kind with
            | App(c,[g])-> 
               let gname' = Common.Var.copy (unwrap_label g) in
               let formals'' = [Common.Var.copy b; Common.Var.copy k]  in 
               let c' = Common.Var.copy (List.hd formals)
               in [(fname, formals', {e_tag = Common.Tag.fresh();
                                      e_kind = Fix([(gname',formals'', 
                                                    {e_tag = Common.Tag.fresh();
                                                    e_kind = App( Var(fname'), 
                                                              (List.map (fun x ->Var(x)) 
                                                                  (formals'@formals'')))})],
                                                    {e_tag = Common.Tag.fresh();
                                                    e_kind = App(Var(c'), [Var(gname')])})});
                  (fname', formals@[unwrap_label g; b; k], a)]        
            | _ -> [(fname, formals, body)])
        | _ -> [(fname, formals, body)])
    in Fix(List.flatten(List.map reduce decls), trans e)
  | Switch(v, es) -> Switch(v, List.map trans es)
  | FFI(f, vs, x, e) -> FFI(f, vs, x, trans e)
  | Primop(p, vs, xs, es) -> Primop(p, vs, xs, List.map trans es)
  } 
in trans program


let c_uncurry = Contract.create 
  ~description: "Uncurring function"
  ~languages: [Language.CPS]
  "transform:uncurry"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:uncurry"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_uncurry
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ]
    transform
