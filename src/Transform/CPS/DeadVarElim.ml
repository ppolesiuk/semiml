open Lang.CPS.Ast
open Lang.CPS
exception Not_reduce

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("DeadVarElim::unwrap_label -- application of non function")

let usezerotimes name =
  match MetaData.CPS.VarUseCount.try_get name with
  | Some(count) ->  count > 0
  | None -> false

let  useless v = 
  match v with 
  | Var v'-> MetaData.CPS.VarUseCount.dec v' 
  | Label v' -> MetaData.CPS.VarUseCount.dec v' 
  | _ -> ()

let rec drop_body expr =
  match expr.e_kind with
  | Record(vl, _, subexpr) -> 
    List.iter (fun (v, _) -> useless v) vl ; drop_body subexpr
  | Select(_, v, _, subexpr)    -> useless v; drop_body subexpr
  | Offset(_, v, _, subexpr)    -> useless v; drop_body subexpr
  | Switch(v, subexprs)         -> useless v; List.iter drop_body subexprs
  | FFI(_, vl, _, subexpr)      -> List.iter useless vl; drop_body subexpr
  | Primop(_, vl, _, subexprs)   -> List.iter useless vl; List.iter drop_body subexprs
  | Fix(funcs, subexpr)         ->
      List.iter (fun (fname, vl, body) -> drop_body body) funcs;
      drop_body subexpr
  | App(fname, args) -> useless fname; List.iter useless args


let transform program = 
let rec trans expr = 
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> 
      if not (usezerotimes x) then (trans e).e_kind else Record(vs, x, trans e)
  | Select(i, v, x, e) -> if not(usezerotimes x) then (trans e).e_kind else Select(i, v, x, trans e)
  | Offset(i, v, x, e) -> if not(usezerotimes x) then (trans e).e_kind else Offset(i, v, x, trans e)
  | App(f, xs) -> expr.e_kind
  | Fix(decls, e) -> 
    let keep = fun (fname, vs, body) -> 
      begin
        if not(usezerotimes fname) then
          begin
            drop_body body;
            false
          end
        else
          true
      end
    in
    let reduce_body = fun (fname ,vs, body) -> (fname, vs, trans body)
  in
    Fix(List.map reduce_body (List.filter keep decls), trans e)
  | Switch(v, es) -> Switch(v, List.map trans es)
  | FFI(f, vs, x, e) ->
      FFI(f, vs, x, trans e)
  | Primop(op, vs, xs, es) -> (* TODO *)
      Primop(op, vs, xs, List.map trans es)
  } 

in trans program 




let c_dead_var_elim = Contract.create
  ~description: "Dead variable elimination"
  ~languages: [Language.CPS] 
  "transform:dead_var_elim"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:dead_var_elim"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.VarUseCount.contract 
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.VarUseCount.contract 
      ; c_dead_var_elim
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_passing_style
      ; Contract.saves_contract Analysis.CPS.VarUseCount.contract 
      ]
    transform
