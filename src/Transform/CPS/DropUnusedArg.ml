open Lang.CPS.Ast
open Lang.CPS
exception Not_reduce

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("DeadVarElim::unwrap_label -- application of non function")

let known fname = 
  match MetaData.CPS.FunctionEscape.try_get fname with
  | None ->  false
  | Some(r) ->  not !r



let transform program = 
  let rec trans expr = 
      { expr with e_kind = match expr.e_kind with 
    | Record(vs, x, e) -> Record(vs, x, trans e)
    | Select(i, v, x, e) -> Select(i, v, x, trans e)
    | Offset(i, v, x, e) -> Offset(i, v, x, trans e) 
    | App(f, xs) ->
      begin
        match MetaData.CPS.FunName2Def.try_get (unwrap_label f), known (unwrap_label f) with
        |Some(_, formals), true-> 
          let rec new_arg xs formals =
            match xs, formals with
            | x::xrest, f::frest ->
              if MetaData.CPS.VarUseCount.try_get f  == None 
                then
                 begin
                    new_arg xrest frest
                 end
              else x::(new_arg xrest frest)
            | _ ,_ -> []
          in App(f, new_arg xs formals)
        | _,_ -> App(f, xs)
      end
       
    | Fix(decls, e) -> 
      begin
        let reduce_arglist = fun (fname, formals, body) ->
          let rec new_arg formals =
            match formals with
            |  f::frest ->
              if MetaData.CPS.VarUseCount.try_get f  == None && known fname
                then
                 begin
                    new_arg frest
                 end
              else f::(new_arg frest)
            | _ -> []
          in (fname, new_arg formals, trans body)
        in
        Fix(List.map reduce_arglist decls, trans e)
      end
    | Switch(v, es) -> Switch(v, List.map trans es)
    | FFI(f, vs, x, e) -> FFI(f, vs, x, trans e)
    | Primop(op, vs, xs, es) -> 
        Primop(op, vs, xs, List.map trans es)
    } 
  in  trans program 
 



let c_drop_unused_arg= Contract.create
  ~description: "Dropping unused arguments"
  ~languages: [Language.CPS]
  "transform:drop_unused_arg"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:drop_unused_arg"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
        ; Analysis.CPS.FunctionEscape.contract 
        ; Analysis.CPS.FunName2Def.contract
        ; Analysis.CPS.VarUseCount.contract 
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.FunctionEscape.contract 
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.VarUseCount.contract 
      ; c_drop_unused_arg
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Analysis.CPS.FunctionEscape.contract 
      ; Contract.saves_contract Analysis.CPS.FunName2Def.contract
      ; Contract.saves_contract Analysis.CPS.VarUseCount.contract 
      ]
    transform
