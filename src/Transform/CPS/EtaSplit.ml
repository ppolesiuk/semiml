open Lang.CPS.Ast

let rec transform expr = 
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, transform e)
  | Select(i, v, x, e) -> Select(i, v, x, transform e)
  | Offset(i, v, x, e) -> Offset(i, v, x, transform e)
  | App(f, xs) -> expr.e_kind
  | Fix(decls, e) -> 
    let etasplit = fun (fname, vs, b) ->
      let fname' = Common.Var.copy fname
      in let vs' = List.map Common.Var.copy vs
      in let args' = List.map (fun x -> Var x) vs'
      in let expr = {e_tag = Common.Tag.fresh(); e_kind = App(Var(fname'), args')} 
      in MetaData.CPS.SplitFunction.set fname';
      [(fname, vs', expr);(fname', vs, transform b)];
    in let decls' = List.flatten (List.map etasplit decls) 
    in Fix(decls', transform e)
  | Switch(v, es) -> Switch(v, List.map transform es)
  | FFI(f, vs, x, e) -> FFI(f, vs, x, transform e)
  | Primop(p, vs, xs, es) -> Primop(p, vs, xs, List.map transform es)
  } 

let c_eta_split = Contract.create
  ~description: "inverse eta reduction"
  ~languages: [Language.CPS] 
  "transform:eta_split"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:eta_split"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_eta_split
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ]
    transform
