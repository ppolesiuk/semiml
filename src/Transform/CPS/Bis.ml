open Lang.CPS.Ast
open Lang.CPS
open Common.Primop.Repr
exception Not_reduce

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("Bis::unwrap_label -- application of non function")

let transform program = 
let rec trans expr = 
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, trans e)
  | Select(i, v, x, e) -> Select(i, v, x, trans e)
  | Offset(i, v, x, e) -> Offset(i, v, x, trans e)
  | App(f, xs) -> expr.e_kind
  | Fix([(fname, [z], body)], cexp) -> 
    (try reduce fname z body cexp with
     | Not_reduce -> Fix([(fname, [z], body)], cexp))
  | Fix(decls, e) ->
    let reduce' = fun (fname, vs, body) ->
      (fname, vs, trans body) in
    Fix(List.map reduce' decls, trans e)
  | Switch(v, es) -> Switch(v, List.map trans es)
  | FFI(f, vs, x, e) -> FFI(f, vs, x, trans e)
  | Primop(op, vs, xs, es) -> Primop(op, vs, xs, List.map trans es)
  } 
and
  reduce fname z body cexp = 
    match body.e_kind with
      | Primop(ope, [z; Int l], [], [e1;e2]) ->
       ( match ope with
          | Primop.Repr ss -> 
           (match ss with 
            | PhysNEq -> 
          (match cexp.e_kind with
          | Primop(op,[a;b], [], [{e_kind=App(z1, [Int i]);_}; {e_kind=App(z2,[Int j]);_}]) ->
            if fname == (unwrap_label z1) && fname == (unwrap_label z2) && 
              Int64.compare i (Int64.of_int 1)  = 0&&
              Int64.compare l (Int64.of_int 0)  = 0 && 
              Int64.compare j (Int64.of_int 0) = 0 then
              begin
                Primop(op, [a;b], [], [trans e1; trans e2])
              end
            else
              raise Not_reduce 
          | _ -> raise Not_reduce
          )
          | _ -> raise Not_reduce
        )
           | _ -> raise Not_reduce
        )
      | _ -> raise Not_reduce
in trans program 

let c_bis = Contract.create
  ~description: "Boolean idiom simplification"
  ~languages: [Language.CPS] 
  "transform:bis"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:bis"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_bis
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ]
    transform
