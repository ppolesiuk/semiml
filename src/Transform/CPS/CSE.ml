open Lang.CPS.Ast

module VarMap = Common.Var.Map

let wrap_fresh e = let x = Common.Tag.fresh ()
 in { e_kind = e; e_tag = x }

(* DEPRECATED *)
module ExprTbl = Hashtbl.Make(struct
  type t = expr
  let equal e1 e2 = match (e1.e_kind, e2.e_kind) with
    | (Record(vs, _, _), Record(vs', _, _)) -> vs = vs'
    | (Select(i, v, _, _), Select(i', v', _, _)) 
    | (Offset(i, v, _, _), Offset(i', v', _, _)) -> i = i' && v = v'
    | (App(f, vs), App(f', vs')) -> false (*f = f' && vs = vs'*)
    | (Fix(decls, _), Fix(decls', _)) -> (* functions declared in a fix
                                         can appear in arbitrary order *)
      false (* TODO *)
    | (Primop(p, vs, _, _), Primop(p', vs', _, _)) -> p = p' && vs = vs'
    | _ -> false
  let hash = Hashtbl.hash
end)
(* DEPRECATED *)
let cse_table = ExprTbl.create 32

let findtbl tbl expr = match List.find (fun e ->
    match (expr.e_kind, e.e_kind) with
    | (Record(vs, _, _), Record(vs', _, _)) -> vs = vs'
    | (Select(i, v, _, _), Select(i', v', _, _)) 
    | (Offset(i, v, _, _), Offset(i', v', _, _)) -> i = i' && v = v'
    | (App(f, vs), App(f', vs')) -> false (*f = f' && vs = vs'*)
    | (Fix(decls, _), Fix(decls', _)) -> (* functions declared in a fix
                                         can appear in arbitrary order *)
      false (* TODO or maybe cse'ing Fixes is too much? *)
    | (Primop(p, vs, _, _), Primop(p', vs', _, _)) -> p = p' && vs = vs'
    | _ -> false
  ) tbl with
  | exception Not_found -> None
  | e -> Some e

let addtbl tbl e = e::tbl

let is_purely_functional op = let open Lang.CPS.Primop
  in let open Common.Primop.Repr
  in let open Common.Primop.Arith
  in let open Common.Primop.FArith
  in match op.e_kind with
  | Select(_, _, _, _)
  | Offset(_, _, _, _) -> true
  | Primop(Repr r, _, _, _) when (r = ALength || r = SLength)
    -> true
  | Primop(Arith a, _, _, _) when (List.mem a [Lt; Le; Gt; Ge])
    -> true
  | Primop(FArith a, _, _, _) when List.mem a
                              [FEq; FNeq; FLt; FLe; FGt; FGe]
    -> true
  | Switch(_, _)
  | Fix(_, _) -> true
  | _ -> false

let rec replace oldnewlst expr = 
  let aux onlst v = match v with
    | Var x -> (match List.assoc x onlst with
      | exception Not_found -> v
      | x' -> Var x'
      )
    | Label x -> (match List.assoc x onlst with
      | exception Not_found -> v
      | x' -> Label x'
      )
    | _ -> v
  in match expr.e_kind with
  | Record(vs, x, e) -> let vs' = List.map (fun (v, a) ->
      (aux oldnewlst v, a)
    ) vs in
   wrap_fresh (Record(vs', x, replace oldnewlst e))
  | Select(i, v, x, e) -> wrap_fresh 
    (Select(i, aux oldnewlst v, x, replace oldnewlst e))
  | Offset(i, v, x, e) -> wrap_fresh 
    (Offset(i, aux oldnewlst v, x, replace oldnewlst e))
  | FFI(name, vs, x, e) ->
    wrap_fresh (FFI(name, List.map (aux oldnewlst) vs, x, replace oldnewlst e))
  | App(f, vs) -> wrap_fresh
    (App(aux oldnewlst f, List.map (aux oldnewlst) vs))
  | Fix(decls, e) -> let decls' = List.map (fun (f, xs, b) ->
      (f, xs, replace oldnewlst b)
    ) decls in
    wrap_fresh (Fix(decls', replace oldnewlst e))
  | Switch(v, es) -> let es' = List.map (replace oldnewlst) es in
    wrap_fresh (Switch(aux oldnewlst v, es'))
  | Primop(p, vs, xs, es) -> let vs' = List.map (aux oldnewlst) vs in
    let es' = List.map (replace oldnewlst) es in
    wrap_fresh (Primop(p, vs', xs, es'))

let rec cse tbl expr =
  let recurse tbl olde newe should_replace = if should_replace then
  match (olde.e_kind, newe.e_kind) with
    | (Record(_, x, _), Record(_, x', e'))
    | (Select(_, _, x, _), Select(_, _, x', e'))
    | (Offset(_, _, x, _), Offset(_, _, x', e')) ->
      let e'' = replace [(x', x)] e' in
      cse tbl e''
    | (Primop(_, _, xs, _), Primop(p, vs, xs', es')) ->
      let es'' = List.map (fun e ->
        cse tbl (replace (List.combine xs' xs) e)) es' in
      (* an interesting case: what should we return as a result
         if we are to delete the root being a branching Primop?
         It would be nice to have sth like branching NoOp ;)
      *)
      wrap_fresh (Primop(p, vs, xs', es''))
    | _ -> (failwith "cse::recurse - should_replace true when should not")
    
  else match newe.e_kind with
    | Record(vs, x, e) -> wrap_fresh (Record(vs, x, cse tbl e))
    | Select(i, v, x, e) -> wrap_fresh (Select(i, v, x, cse tbl e))
    | Offset(i, v, x, e) -> wrap_fresh (Offset(i, v, x, cse tbl e))
    | FFI(name, vs, x, e) -> wrap_fresh (FFI(name, vs, x, cse tbl e))
    | App(f, vs) -> newe
    | Fix(decls, e) -> let decls' = List.map (fun (f, xs, e') ->
         (f, xs, cse tbl e')
      ) decls in
      wrap_fresh (Fix(decls', cse tbl e))
    | Switch(v, es) -> let es' = List.map (cse tbl) es in
      wrap_fresh (Switch(v, es'))
    | Primop(p, vs, xs, es) -> let es' = List.map (cse tbl) es in
      wrap_fresh (Primop(p, vs, xs, es'))
  in
  if is_purely_functional expr then
  match (findtbl tbl expr) with
  | None -> let ntbl = addtbl tbl expr in    
    recurse ntbl expr expr false
  | Some expr' -> recurse tbl expr' expr true
  else recurse tbl expr expr false
  
let transform expr = cse [] expr

let c_cse = Contract.create
  ~description: "One pass of Common Subexpression Elimination"
  ~languages: [Language.CPS]
  "transform:cse"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:cse"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ; c_cse
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Analysis.CPS.FunName2Def.contract
      ; Contract.saves_contract Analysis.CPS.FuncCallCount.contract
      ; Contract.saves_contract Analysis.CPS.Var2Def.contract
      ]
    transform
