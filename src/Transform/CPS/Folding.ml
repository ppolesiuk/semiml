open Lang.CPS.Ast
open Lang.CPS

exception Not_reduce

let minint  = Int64.of_int (-1000294734)
let maxint  = Int64.of_int 1000294734
let minreal = Common.Real.of_sml_string "1e-50"
let maxreal = Common.Real.of_sml_string "1e50"

let boxed arg = 
  match arg with 
  | String _ -> true
  | Int _  -> false
  | Real _ -> Common.Real.is_boxed ()
  | _ -> raise Not_reduce

let checkOpInt op xs =
  let open Common.Primop.Arith in
    match op, xs with
      | Le, [Int i; Int j] -> (Int64.compare i j) <= 0
      | Ge, [Int i; Int j] -> (Int64.compare i j) >= 0
      | Lt, [Int i; Int j] -> (Int64.compare i j) < 0
      | Gt, [Int i; Int j] -> (Int64.compare i j) > 0
      | _ -> raise Not_reduce

let checkOpReal op xs =
  let open Common.Primop.FArith in
    match op, xs with
      | FEq, [Real i; Real j] -> Common.Real.equal i j 
      | FNeq, [Real i; Real j]-> Common.Real.equal i j 
      | FLe, [Real i; Real j] -> Common.Real.le i j 
      | FGe, [Real i; Real j] -> Common.Real.ge i j
      | FLt, [Real i; Real j] -> Common.Real.lt i j
      | FGt, [Real i; Real j] -> Common.Real.gt i j
      | _ -> raise Not_reduce

let checkOpRepr op xs =
  let open Common.Primop.Repr in
    match op, xs with
    | PhysEq, [Int i; Int j] -> Int64.compare i j == 0
    | PhysNEq, [Int i; Int j] ->Int64.compare i j == 0
    | Boxed, [v] -> boxed v
    | _ -> raise Not_reduce

let checkOp op = 
  match op with
  | Primop.Arith op  -> checkOpInt op
  | Primop.FArith op -> checkOpReal op
  | Primop.Repr op   -> checkOpRepr op
  | _ -> raise Not_reduce

let overflow n =
  if Int64.compare n minint < 0 || Int64.compare n maxint > 0
  then raise Not_reduce
  else Int n

let overflowr r =
  if Common.Real.lt r minreal || Common.Real.gt r maxreal
  then raise Not_reduce
  else Real r

let evalOpInt op xs =
  let open Common.Primop.Arith in
    match op, xs with
      | Add, [Int i; Int j] -> overflow(Int64.add i j)
      | Mul, [Int i; Int j] -> overflow(Int64.mul i j )
      | Sub, [Int i; Int j] -> overflow(Int64.sub i j )
      | Neg, [Int i] -> overflow(Int64.neg i)
      | Div, [Int i; Int j] -> 
        if Int64.compare j Int64.zero = 0 
        then
          raise Not_reduce
        else
          overflow(Int64.div i j)
      | _ -> raise Not_reduce

let evalOpReal op xs =
  let open Common.Primop.FArith in
    match op, xs with
      | FAdd, [Real i; Real j] -> overflowr(Common.Real.add i j)
      | FSub, [Real i; Real j] -> overflowr(Common.Real.sub i j)
      | FMul, [Real i; Real j] -> overflowr(Common.Real.mul i j)
      | FDiv, [Real i; Real j] -> overflowr(Common.Real.div i j)
      | _ -> raise Not_reduce

let evalOpRepr op xs =
  let open Common.Primop.Repr in
    match op, xs with
    | SLength, [String a] -> Int (Int64.of_int (String.length a))
    | _ -> raise Not_reduce

let evalOpMem op xs =
  let open Common.Primop.Mem in
    match op, xs with
    | OrdOf, [String a; Int i]->
      Int (Int64.of_int (Char.code (a.[Int64.to_int i])))
    | _ -> raise Not_reduce

let evalOp op = 
  match op with
  | Primop.Arith op  -> evalOpInt op
  | Primop.FArith op -> evalOpReal op
  | Primop.Repr op   -> evalOpRepr op
  | Primop.Mem op -> evalOpMem op
  | _ -> raise Not_reduce

let transform program = 
let rec trans expr = 
    { expr with e_kind = match expr.e_kind with 
  | Record(vs, x, e) -> Record(vs, x, trans e)
  | Select(i, v, x, e) -> Select(i, v, x, trans e)
  | Offset(i, v, x, e) -> Offset(i, v, x, trans e)
  | App(f, xs) -> expr.e_kind
  | Fix(decls, e) -> 
    let reduce = fun (fname, vs, body) ->
      (fname, vs, trans body) in
    Fix(List.map reduce decls, trans e)
  | Switch(Int v, es) -> 
    let e = List.nth es (Int64.to_int v) in (trans e).e_kind
  | Switch(v, es) -> Switch(v, List.map trans es)
  | FFI(f, vs, x, e) -> FFI(f, vs, x, trans e)
  | Primop(op, vs, xs, [c1;c2]) -> 
        (try 
          (match checkOp op vs with
          | true -> (trans c1).e_kind
          | false -> (trans c2).e_kind)
        with
        | Not_reduce -> Primop(op, vs, xs, [trans c1; trans c2]))
  | Primop(op, vs, [x], [e]) -> 
        (try let nw = evalOp op vs in
          let e' = trans e in
            (BetaContraction.replace_vars ([(x, nw)],[]) e').e_kind
        with
        | Not_reduce -> Primop(op, vs, [x], [trans e]))
  | Primop(op, vs, xs, es) -> Primop(op, vs, xs, es)  
  } 
in trans program 

let c_folding = Contract.create
  ~description: "Arithmetic, switch, comparison operator folding"
  ~languages: [Language.CPS] 
  "transform:folding"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name: "CPS:folding"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_folding
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ]
    transform
