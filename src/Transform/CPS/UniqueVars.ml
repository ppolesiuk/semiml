open Lang.CPS.Ast

module VarMap = Common.Var.Map

let tr_var env x =
  let y = Common.Var.copy x in
  (VarMap.add x y env, y)

let rec tr_vars env xs =
  match xs with
  | [] -> (env, [])
  | x :: xs ->
    let (env, y) = tr_var env x in
    let (env, ys) = tr_vars env xs in
    (env, y :: ys)

let tr_value env v =
  match v with
  | Var   x -> Var(VarMap.find x env)
  | Label x -> Label(VarMap.find x env)
  | Int _ | Real _ | String _ -> v

let build_fix_env env defs =
  List.fold_left (fun env (x, _, _) ->
    let y = Common.Var.copy x in
    VarMap.add x y env
  ) env defs

let rec tr_expr env expr =
  { expr with
    e_kind =
      match expr.e_kind with
      | Record(vs, x, e) ->
        let vs = List.map (fun (v, ap) -> (tr_value env v, ap)) vs in
        let y  = Common.Var.copy x in
        Record(vs, y, tr_expr (VarMap.add x y env) e)
      | Select(n, v, x, e) ->
        let y = Common.Var.copy x in
        Select(n, tr_value env v, y, tr_expr (VarMap.add x y env) e)
      | Offset(n, v, x, e) ->
        let y = Common.Var.copy x in
        Offset(n, tr_value env v, y, tr_expr (VarMap.add x y env) e)
      | App(v, vs) ->
        App(tr_value env v, List.map (tr_value env) vs)
      | Fix(defs, e) ->
        let env = build_fix_env env defs in
        let defs = List.map (fun (x, xs, body) ->
            let (env', ys) = tr_vars env xs in
            (VarMap.find x env, ys, tr_expr env' body)
          ) defs in
        Fix(defs, tr_expr env e)
      | Switch(v, es) ->
        Switch(tr_value env v, List.map (tr_expr env) es)
      | FFI(name, args, ret, cont) ->
        let args = List.map (tr_value env) args in
        let (env, ret) = tr_var env ret in
        FFI(name, args, ret, tr_expr env cont)
      | Primop(op, vs, xs, es) ->
        let vs = List.map (tr_value env) vs in
        let (env, xs) = tr_vars env xs in
        let es = List.map (tr_expr env) es in
        Primop(op, vs, xs, es)
  }

let transform expr =
  tr_expr (VarMap.singleton top_cont top_cont) expr

let c_unique_vars = Contract.create
  ~description: "Restore unique_vars contract"
  ~languages: [Language.CPS]
  "transform:unique_vars"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:unique_vars"
    ~weight: 0.1
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; c_unique_vars
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_passing_style
      ; Contract.saves_contract Lang.CPS.Contracts.definitions_lifted
      ]
    transform
