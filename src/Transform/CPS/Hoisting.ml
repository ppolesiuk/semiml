open Lang.CPS.Ast

module VarMap = Common.Var.Map
module VarSet = Common.Var.Set

let uk_table = Hashtbl.create 32
let uk_table_set_is_known tag b = Hashtbl.replace uk_table tag b

let has_all_known fixtag = Hashtbl.find uk_table fixtag

let eqvv vr vl = match vl with
  | Var v | Label v -> Common.Var.equal vr v
  | _ -> false

(* assumption: function escapes iff it is not known *)
(* this function expects that fvar is a variable
   bound to a CPS function but it doesn't check it!
*)
let rec is_escaping (fvar : Lang.CPS.Ast.var) program = 
  let open Lang.CPS.Primop in 
  let open Common.Primop.Mem in
  match program.e_kind with
  | Record(vs, x, e) -> List.exists (fun (vl, _) ->
      eqvv fvar vl
    ) vs
    || is_escaping fvar e
  | Select(_, _, _, e)
  | FFI(_, _, _, e)
  | Offset(_, _, _, e) -> is_escaping fvar e
  | App(v, vs) -> List.exists (fun vl -> eqvv fvar vl) vs
  | Fix(decls, e) -> List.fold_left (fun acc (_, _, b) ->
      acc || is_escaping fvar b
    ) false decls
    || is_escaping fvar e
  | Switch(v, es) -> List.fold_left (fun acc e ->
      acc || is_escaping fvar e
    ) false es
  | Primop(Mem p, vs, xs, es) when List.mem p [Assign; UnboxedAssign;
      Update; UnboxedUpdate; Store; MakeRef; MakeRefUnboxed;
      AllocArray; AllocUnboxedArray] ->
      (* TODO: triple check which primops cause its argument
         to become escaping and refine the implementation! *)
    List.exists (fun vl -> eqvv fvar vl) vs
    ||
    List.fold_left (fun acc e ->
      acc || is_escaping fvar e
    ) false es
  | Primop(p, vs, xs, es) -> List.fold_left (fun acc e ->
      acc || is_escaping fvar e
    ) false es

let is_known fvar program = not @@ is_escaping fvar program

let not_a_fix e = match e.e_kind with
  | Fix(_, _) -> false
  | _ -> true

let is_purely_functional op = let open Lang.CPS.Primop
  in let open Common.Primop.Repr
  in let open Common.Primop.Arith
  in let open Common.Primop.FArith
  in match op.e_kind with
  | Select(_, _, _, _)
  | Offset(_, _, _, _) -> true
  | Primop(Repr r, _, _, _) when (r = ALength || r = SLength)
    -> true
  | Primop(Arith a, _, _, _) when (List.mem a [Lt; Le; Gt; Ge])
    -> true
  | Primop(FArith a, _, _, _) when List.mem a
                              [FEq; FNeq; FLt; FLe; FGt; FGe]
    -> true
  | Switch(_, _)
  | Fix(_, _) -> true
  | _ -> false

let is_impure_primop op = match op.e_kind with
  | Primop(_, _, _, [_]) -> not (is_purely_functional op)
  | _ -> false

let rec snoc xs x = match xs with
  | [] -> [x]
  | y::ys -> y::(snoc ys x)

(* yields (l1, l2), where forall e in l1 p e = false,
   l1 @ l2 = xs
   and l1 is longest possible
ft -> first true
*)
let split_on_ft p xs =
  let rec aux pr s = match s with
    | [] -> (pr, [])
    | x::xs -> if p x then (pr, s)
               else aux (x::pr) xs
  in let (pr, s) = aux [] xs
  in (List.rev pr, s)

let wrap_fresh e = let x = Common.Tag.fresh ()
 in { e_kind = e; e_tag = x }

let rec delete expr todel =
  match expr.e_kind with
  | Record(vs, x, e) -> let e' = delete e todel in
    if List.mem expr.e_tag todel then e' else wrap_fresh (Record(vs, x, e'))
  | Select(i, v, x, e) -> let e' = delete e todel in
    if List.mem expr.e_tag todel then e'
    else wrap_fresh (Select(i, v, x, e'))
  | Offset(i, v, x, e) -> let e' = delete e todel in
    if List.mem expr.e_tag todel then e' else wrap_fresh (Offset(i, v, x, e'))
  | FFI(name, vs, x, e) -> let e' = delete e todel in
    if List.mem expr.e_tag todel then e' else wrap_fresh (FFI(name, vs, x, e'))
  | App(_, _) -> expr
  | Fix(decls, e) -> let decls' = List.map (fun (f, vs, b) ->
      (f, vs, delete b todel)
    ) decls in let e' = delete e todel in
    wrap_fresh (Fix(decls', e'))
  | Switch(v, es) -> let es' = List.map (fun e -> delete e todel) es in
    wrap_fresh (Switch(v, es'))
  | Primop(p, vs, xs, [e]) -> let e' = delete e todel in
    if List.mem expr.e_tag todel then e' else wrap_fresh (Primop(p, vs, xs, [e']))
  | Primop(p, vs, xs, es) -> let es' = List.map (fun e -> delete e todel) es in
    wrap_fresh (Primop(p, vs, xs, es'))

let hoist_below_branching expr =
  let put_above expr abovev = 
    let open Common.Tag in
    match abovev.e_kind with
      | Record(vs, x, _) -> wrap_fresh (Record(vs, x, expr))
      | Select(i, v, x, _) -> wrap_fresh (Select(i, v, x, expr))
      | Offset(i, v, x, _) -> wrap_fresh (Offset(i, v, x, expr))
      | FFI(name, vs, x, _) -> wrap_fresh (FFI(name, vs, x, expr))
      | App(_, _)
      | Fix(_, _)
      | Switch(_, _) -> let s = "Hoisting::hoist_below_branching"
               ^ "::put_above -- "
               ^ "App/Fix/Switch node cannot be above"
               in CompilerLog.error "%s" s; failwith s
      | Primop(p, vs, xs, es) -> if List.length es = 1 then
        wrap_fresh (Primop(p, vs, xs, [expr]))
        else let s = "Hoisting::hoist_below_branching"
               ^ "::put_above -- "
               ^ "branching Primop cannot be put above"
             in CompilerLog.error "%s" s; failwith s  
  (* new_used_here : var Set
     impure : expr list
  *)
  in let impures_split new_used_here impure =
    let impures_used = VarSet.filter (fun vr ->
      let veo = MetaData.CPS.Var2Def.try_get vr in
      match veo with
        | Some ve -> not (is_purely_functional ve)
        | None -> false
    ) new_used_here in
    let impures_used_tags = VarSet.fold (fun vr acc ->
      let veo = MetaData.CPS.Var2Def.try_get vr in
      match veo with
        | Some ve -> (ve.e_tag)::acc
        | None -> acc
    (*  (MetaData.CPS.Var2Def.get vr).e_tag::acc *)
    ) impures_used [] in
    let (imp_stay, imp_spillr) = split_on_ft (fun e ->
      List.mem e.e_tag impures_used_tags (* TODO: double check! *)
    ) impure
    in let imp_spill = List.rev imp_spillr
    in (imp_stay, imp_spill)
    
  (* hbb - hoist below branching (i.e. hoist down)
     dnu - accumulator for vars Declared and Not Used (: var Set)
     dau - accumulator for vars Declared and Already Used (: var Set)
     impure - accumulator for impure operators encountered so far
              (youngest in head) (: expr list)
              (it is not disjoint with dnu! list is needed because order
              of occurence is important for impure operators)
     todel - accumulator for operators to be deleted after `hbb` finishes
     (: Common.Tag.t list)
  *)
  in let rec hbb expr dnu dau impure todel = 
    let update_accums vs xs =
      let new_used_here = VarSet.diff
        (List.fold_left (fun acc vl ->
           match vl with
            | Var v | Label v -> (*if VarSet.mem v dnu
              then*) VarSet.add v acc
              (* if not in dnu then it's probably a formal parameter
                 so it doesn't have a definition! *)
              (*else acc*)
            | _ -> acc
          
          (*ainp vl acc (* acc : var Set *)*)
        ) VarSet.empty vs) dau in
      let dau' = VarSet.union new_used_here dau in
      let dnu' = VarSet.diff (VarSet.union (VarSet.of_list xs) dnu) dau' in
      let impure' = if is_purely_functional expr then
        impure
        else expr::impure in
      (new_used_here, dau', dnu', impure')
    in
    let spill_needed nexpr new_used_here impure_to_spill =
      let nexpr' = List.fold_left put_above nexpr impure_to_spill in
      let nexpr'' = VarSet.fold (fun vr acc -> 
        let veo = MetaData.CPS.Var2Def.try_get vr
        in match veo with
          | Some ve ->
            if is_purely_functional ve &&
               not_a_fix ve then
            put_above acc ve
            else acc
          | None -> acc
      ) new_used_here nexpr'
      in nexpr''
    in
    let aux_template vs xs e = 
      let (new_used_here, dau', dnu', impure') = update_accums vs xs in
      let (impure_rest, impure_to_spill) = impures_split new_used_here impure' in
      let (nexpr, ntodel) = hbb e dnu' dau' impure_rest (expr.e_tag::todel) in
      let nexpr'' = spill_needed nexpr new_used_here impure_to_spill
      in (nexpr'', ntodel)
    in
    let aux_template_branching vs xs es mkexp = 
      let to_spill_here_due2branches = VarSet.fold (fun vr acc ->
        let nbbranches = List.fold_left (fun acc e ->
          if VarSet.mem vr (Analysis.CPS.FreeVars.calc_on_demand e)
          then 1 + acc
          else acc
        ) 0 es in
        if nbbranches >= 2 then VarSet.add vr acc
        else acc
      ) dnu VarSet.empty in
      let tshd2b = List.map (fun v -> Var v) (VarSet.elements to_spill_here_due2branches) in
      let (new_used_here, dau', dnu', impure') = update_accums (vs @ tshd2b) xs in
      let es'_todels = List.map (fun e ->
        hbb e dnu' dau' impure []
      ) es in
      let (es', todels) = List.split es'_todels in
      let todel' = List.concat todels in
      let (impure_rest, impure_to_spill) = impures_split new_used_here impure' in
      let nexpr = wrap_fresh (mkexp es') in
      let nexpr' = spill_needed nexpr new_used_here impure_to_spill in
      (nexpr', todel')
      
    (* actual body of hbb function *) 
    in match expr.e_kind with
    | Record(vs, x, e) -> aux_template (List.map fst vs) [x] e
    | Select(_, v, x, e) -> aux_template [v] [x] e
    | Offset(_, v, x, e) -> aux_template [v] [x] e
    | FFI(_, vs, x, e) -> aux_template vs [x] e
    | App(v, vs) -> 
      let (new_used_here, dau', dnu', impure') = update_accums (v::vs) [] in
      let (impure_rest, impure_to_spill) = impures_split new_used_here impure' in
      let nexpr'' = spill_needed expr new_used_here(*dau'*) impure_to_spill in
      (nexpr'', todel)
    | Fix(decls, e) -> (* in current implementation, we don't push
                          below Fix'es -- this is to make life easier
                          in CSE optimization, but may be altered later *)
      let decls'_todels' = List.map (fun (f, vs, b) ->
        let (nb, ntd) = hbb b VarSet.empty VarSet.empty [] []
        in ((f, vs, nb), ntd)
      ) decls in
      let (decls', todels') = List.split decls'_todels' in
      let ntodel = todel @ (List.concat todels') in
      let pure_to_spill = VarSet.filter (fun vr ->
        let veo = MetaData.CPS.Var2Def.try_get vr
        in match veo with
          | Some ve -> is_purely_functional ve
          | None -> false
      ) (VarSet.inter (Analysis.CPS.FreeVars.calc_on_demand expr) dnu) in
      let (e', ntodel') = hbb e dau dnu [] ntodel in
      let expr' = wrap_fresh (Fix(decls', e')) in
      let nexpr = spill_needed expr' pure_to_spill impure
      in (nexpr, ntodel')
    | Switch(v, es) -> 
      aux_template_branching [v] [] es (fun es' -> Switch(v, es'))
    | Primop(p, vs, xs, [e]) -> aux_template vs xs e
    | Primop(p, vs, xs, es) -> (* branching primop *)
      aux_template_branching vs xs es (fun es' -> Primop(p, vs, xs, es'))
  
  (* actual body of hoist_below_branching function *)
  in let (nexpr, todel) = hbb expr VarSet.empty VarSet.empty [] []
  in delete nexpr todel

let rec split_fixes prog expr = match expr.e_kind with
  | Record(vs, x, e) -> wrap_fresh (Record(vs, x, split_fixes prog e))
  | Select(i, v, x, e) -> wrap_fresh (Select(i, v, x, split_fixes prog e))
  | Offset(i, v, x, e) -> wrap_fresh (Offset(i, v, x, split_fixes prog e))
  | App(_, _) -> expr
  | Fix(decls, e) -> let (knownfs, unknownfs) = List.partition 
      (fun (f, xs, b) -> is_known f prog) decls in
    if knownfs = [] then let _ = uk_table_set_is_known expr.e_tag false in expr
    else if unknownfs = [] then let _ = uk_table_set_is_known expr.e_tag true in expr
    else let knownfv = List.fold_left (fun acc (f, xs, b) ->
      VarSet.union acc (VarSet.diff 
        (Analysis.CPS.FreeVars.calc_on_demand b)
        (VarSet.of_list xs)
      )
    ) VarSet.empty knownfs in
    let unknownfv = List.fold_left (fun acc (f, xs, b) ->
      VarSet.union acc (VarSet.diff 
        (Analysis.CPS.FreeVars.calc_on_demand b)
        (VarSet.of_list xs)
      )
    ) VarSet.empty unknownfs in
    let knusesunkn = List.exists (fun (f, xs, b) ->
      VarSet.mem f knownfv
    ) unknownfs in
    let unknuseskn = List.exists (fun (f, xs, b) ->
      VarSet.mem f unknownfv
    ) knownfs in
    if knusesunkn && (not unknuseskn) then
      let inner = wrap_fresh (Fix(knownfs, e)) in
      let outer = wrap_fresh (Fix(unknownfs, inner)) in
      uk_table_set_is_known inner.e_tag true;
      uk_table_set_is_known outer.e_tag false;
      outer
    else if (not knusesunkn) && unknuseskn then
      let inner = wrap_fresh (Fix(unknownfs, e)) in
      let outer = wrap_fresh (Fix(knownfs, inner)) in
      uk_table_set_is_known inner.e_tag false;
      uk_table_set_is_known outer.e_tag true;
      outer
    else if knusesunkn && unknuseskn then
      let _ = uk_table_set_is_known expr.e_tag false in
      expr
    else (* arbitrarily, let unknown be higher *)
      let inner = wrap_fresh (Fix(knownfs, e)) in
      let outer = wrap_fresh (Fix(unknownfs, inner)) in
      uk_table_set_is_known inner.e_tag true;
      uk_table_set_is_known outer.e_tag false;
      outer
  | _ -> expr

let rec hoist_fixes expr = 
  let swap_template x decls fix einside newe = let s =
    VarSet.diff (Analysis.CPS.FreeVars.calc_on_demand fix)
                (Analysis.CPS.FreeVars.calc_on_demand einside) in
    if VarSet.mem x s then expr
    else wrap_fresh (Fix(decls, wrap_fresh newe))
  in
  let rec swap expr = match expr.e_kind with
    | Record(vs, x, ({ e_kind = Fix(decls, e); e_tag = fixtag} as fix)) ->
      swap_template x decls fix e (Record(vs, x, e))
    | Select(i, v, x, ({ e_kind = Fix(decls, e); e_tag = fixtag} as fix)) ->
      swap_template x decls fix e (Select(i, v, x, e))
    | Offset(i, v, x, ({ e_kind = Fix(decls, e); e_tag = fixtag} as fix)) ->
      swap_template x decls fix e (Offset(i, v, x, e))
    | Primop(p, vs, xs, [{ e_kind = Fix(decls, e); e_tag = fixtag} as fix]) -> 
      let s = VarSet.diff (Analysis.CPS.FreeVars.calc_on_demand fix)
                          (Analysis.CPS.FreeVars.calc_on_demand e) in
      if List.exists (fun x -> VarSet.mem x s) xs
      then expr
      else wrap_fresh (Fix(decls, wrap_fresh (Primop(p, vs, xs, [e]))))
    | _ -> expr (* App and branching expressions are left intact *)
  in
  let rec merge expr = match expr.e_kind with
    | Fix(decls1, { e_kind = Fix(decls2, e); e_tag = fix2tag}) ->
      let outervars = List.concat (List.map (fun (f, xs, b) ->
          xs
        ) decls1
      ) in
      let freeinbodies = List.fold_left (fun acc (f, xs, b) ->
        VarSet.union acc (Analysis.CPS.FreeVars.calc_on_demand b)
      ) VarSet.empty decls2 in
      if not (List.exists (fun x -> VarSet.mem x freeinbodies) outervars)
         && has_all_known expr.e_tag = has_all_known fix2tag
      then wrap_fresh (Fix(decls1 @ decls2, e))
      else expr
    | _ -> expr
  in
  let aux e mke = let e' = hoist_fixes e in
    let expr' = swap (wrap_fresh (mke e')) in
    merge expr'
  in
  match expr.e_kind with
  | Record(vs, x, e) -> aux e (fun e' -> Record(vs, x, e'))
  | Select(i, v, x, e) -> aux e (fun e' -> Select(i, v, x, e'))
  | Offset(i, v, x, e) -> aux e (fun e' -> Offset(i, v, x, e'))
  | FFI(name, vs, x, e) -> aux e (fun e' -> FFI(name, vs, x, e'))
  | App(_, _) -> expr
  | Fix(decls, e) -> let decls' = List.map (fun (f, xs, b) ->
      (f, xs, hoist_fixes b)
    ) decls in
    aux e (fun e' -> Fix(decls', e'))
  | Switch(v, es) -> let es' = List.map hoist_fixes es in
    wrap_fresh (Switch(v, es'))
  | Primop(p, vs, xs, [e]) -> aux e (fun e' -> Primop(p, vs, xs, [e']))
  | Primop(p, vs, xs, es) -> let es' = List.map hoist_fixes es in
    wrap_fresh (Primop(p, vs, xs, es'))
  
let transform expr =
  hoist_fixes (split_fixes expr (hoist_below_branching expr))

let c_hoisting = Contract.create
  ~description: ("Hoisting (rearranging order of operations "
              ^ "and merging/splitting FIXes)")
  ~languages: [Language.CPS]
  "transform:hoisting"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:hoisting"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.Var2Def.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.Var2Def.contract
      ; c_hoisting
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Analysis.CPS.Var2Def.contract
      ]
    transform
