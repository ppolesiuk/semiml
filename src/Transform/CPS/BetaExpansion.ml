open Lang.CPS.Ast

module VarMap = Common.Var.Map

let foi = float_of_int
let count p xs = List.length (List.filter p xs)
let sum = List.fold_left (+) 0
let fsum = List.fold_left (+.) 0.0
let fst3 (a,b,c) = a

let magic_C_const = 4.0
let magic_D_const = 6.0
let magic_E_const = 10.0
let rounds_so_far = ref 0

let fail s = begin
  CompilerLog.error "%s" s;
  failwith s;
end

let eqvv vr vl = match vl with
  | Var v | Label v -> Common.Var.equal vr v
  | _ -> false
let rec number_of_escapes (fvar : Lang.CPS.Ast.var) program = 
  let open Lang.CPS.Primop in 
  let open Common.Primop.Mem in
  match program.e_kind with
  | Record(vs, _, e) -> (count (fun (vl, _) -> eqvv fvar vl) vs) + (number_of_escapes fvar e)
  | Select(_, _, _, e)
  | Offset(_, _, _, e) -> number_of_escapes fvar e
  | App(_, vs) -> count (fun vl -> eqvv fvar vl) vs
  | Fix(decls, e) -> (sum (List.map (fun (_, _, b) -> number_of_escapes fvar b) decls)) + (number_of_escapes fvar e)
  | FFI(_, vs, x, e) ->
    (count (fun y -> eqvv fvar y) vs)
    + if Common.Var.equal fvar x then 1 else 0
    + number_of_escapes fvar e
  | Switch(_, es) -> sum (List.map (fun e -> number_of_escapes fvar e) es)
  | Primop(Mem p, vs, xs, es) when List.mem p [Assign; UnboxedAssign;
      Update; UnboxedUpdate; Store; MakeRef; MakeRefUnboxed;
      AllocArray; AllocUnboxedArray] ->
      (* TODO: triple check which primops cause its argument
         to become escaping and refine the implementation! *)
    (count (fun vl -> eqvv fvar vl) vs) + (sum (List.map (number_of_escapes fvar) es))
  | Primop(p, vs, xs, es) -> sum (List.map (fun e -> number_of_escapes fvar e) es)

let is_escaping f expr = (number_of_escapes f expr) > 0

let bigO = Analysis.CPS.ApproxFuncCost.expr_cost

let unwrap_label value = match value with
  | Var(l)
  | Label(l) -> l
  | _ -> raise (Failure ("BetaExpansion::unwrap_label -- application of non function"))

let rec times_used_in_select acc p body = match body.e_kind with
  | Record(_, _, e) -> times_used_in_select acc p e
  | Select(_, v, _, e) -> (match v with
    | Var x | Label x -> if x = p then times_used_in_select (acc + 1) p e
                         else times_used_in_select acc p e
    | _ -> times_used_in_select acc p e
    )
  | Offset(_, _, _, e) -> times_used_in_select acc p e
  | App(_, _) -> acc
  | Fix(decls, e) -> let sum = List.fold_left (fun a (v, vs, e') ->
      a + times_used_in_select 0 p e'
    ) 0 decls in
    times_used_in_select (acc + sum) p e
  | FFI(_, _, _, e) ->
    times_used_in_select acc p e
  | Switch(_, es) | Primop(_, _, _, es) -> 
    let sum = List.fold_left (fun a e' ->
      a + times_used_in_select 0 p e'
    ) 0 es in
    acc + sum

let rec savings_arg_of_branching acc p body = match body.e_kind with
  | Record(_, _, e) | Offset(_, _, _, e)
  | Select(_, _, _, e) -> savings_arg_of_branching acc p e
  | App(_, _) -> acc
  | Fix(decls, e) -> let sum = List.fold_left (fun a (v, vs, e') ->
      a +. savings_arg_of_branching 0.0 p e'
    ) 0.0 decls in
    savings_arg_of_branching (sum +. acc) p e
  | FFI(_, _, _, e) ->
    savings_arg_of_branching acc p e
  | Switch(v, es) -> let add = (match v with
    | Var x | Label x -> if x = p then let scont = List.fold_left
        (fun a e' -> a + bigO e') 0 es in
      let n = foi (List.length es) in
      4.0 +. (n -. 1.0) /. n *. (foi scont)
      else 0.0
    | _ -> 0.0
    ) in
    let sum = List.fold_left (fun a e' -> a +. savings_arg_of_branching 0.0 p e')
      0.0 es in
    add +. sum +. acc
  (* ten kod nie sprawdza czy porownujemy ze stala, a taki jest warunek tutaj *)
  | Primop(Lang.CPS.Primop.Arith a, vs, _, es) -> 
    let open Common.Primop.Arith in
    let add = if List.exists (fun v ->
      match v with
        | Var x | Label x -> x = p
        | _ -> false
    ) vs then (match a with
    | Lt | Le | Gt | Ge -> (let scont = List.fold_left
          (fun a e' -> a + bigO e') 0 es in
        let n = foi (List.length es) in
        2.0 +. (n -. 1.0) /. n *. (foi scont)
      )
    | _ -> 0.0
    ) else 0.0 in
    let sum = List.fold_left (fun a e' -> a +. savings_arg_of_branching 0.0 p e')
      0.0 es in
    add +. sum +. acc
  | Primop(Lang.CPS.Primop.FArith a, vs, _, es) ->
    let open Common.Primop.FArith in
    let add = if List.exists (fun v ->
      match v with
        | Var x | Label x -> x = p
        | _ -> false
    ) vs then (match a with
    | FLt | FLe | FGt | FGe | FEq | FNeq -> (let scont = List.fold_left
          (fun a e' -> a + bigO e') 0 es in
        let n = foi (List.length es) in
        2.0 +. (n -. 1.0) /. n *. (foi scont)
      )
    | _ -> 0.0
    ) else 0.0 in
    let sum = List.fold_left (fun a e' -> a +. savings_arg_of_branching 0.0 p e')
      0.0 es in
    add +. sum +. acc
  | Primop(_, _, _, es) -> let sum = List.fold_left (fun a e' ->
      a +. savings_arg_of_branching 0.0 p e'
    ) 0.0 es in
    sum +. acc

let rec savings_arg_of_primop_const acc p body = match body.e_kind with
  | Record(_, _, e) | Offset(_, _, _, e)
  | Select(_, _, _, e) -> savings_arg_of_primop_const acc p e
  | App(_, _) -> acc
  | Fix(decls, e) -> let sum = List.fold_left (fun a (v, vs, e') ->
      a +. savings_arg_of_primop_const 0.0 p e'
    ) 0.0 decls in
    savings_arg_of_primop_const (sum +. acc) p e
  | FFI(_, _, _, e) ->
    savings_arg_of_primop_const acc p e
  | Switch(_, es) -> let sum = List.fold_left (fun a e' ->
      a +. savings_arg_of_primop_const 0.0 p e'
    ) 0.0 es in
    sum +. acc
  | Primop(prim, ([Int _; v] as vs), xs, es)
  | Primop(prim, ([Real _; v] as vs), xs, es)
  | Primop(prim, ([v; Int _] as vs), xs, es)
  | Primop(prim, ([v; Real _] as vs), xs, es)
  | Primop(prim, ([v] as vs), xs, es) -> let add = (match v with
        | Var x | Label x -> if x = p then
                             List.length vs + List.length xs
                             else 0
        | _ -> 0
      ) in 
      let sum = List.fold_left (fun a e' -> 
          a +. savings_arg_of_primop_const 0.0 p e'
        ) 0.0 es in
      (foi add) +. sum +. acc
  | Primop(_, _, _, es) -> let sum = List.fold_left (fun a e' ->
      a +. savings_arg_of_primop_const 0.0 p e'
    ) 0.0 es in
    sum +. acc

let rec savings_arg_of_primop_nonconst acc p body = match body.e_kind with
  | Record(_, _, e) | Offset(_, _, _, e)
  | Select(_, _, _, e) -> savings_arg_of_primop_nonconst acc p e
  | App(_, _) -> acc
  | Fix(decls, e) -> let sum = List.fold_left (fun a (v, vs, e') ->
      a +. savings_arg_of_primop_nonconst 0.0 p e'
    ) 0.0 decls in
    savings_arg_of_primop_nonconst (sum +. acc) p e
  | FFI(_, _, _, e) ->
    savings_arg_of_primop_nonconst acc p body
  | Switch(_, es) -> let sum = List.fold_left (fun a e' ->
      a +. savings_arg_of_primop_nonconst 0.0 p e'
    ) 0.0 es in
    sum +. acc
  | Primop(prim, [v1; v2], xs, es) -> let add = (match (v1, v2) with
        | (Var x1, Var x2)
        | (Var x1, Label x2)
        | (Label x1, Var x2)
        | (Label x1, Label x2) -> if x1 = p || x2 = p then
                             (2.0 +. foi (List.length xs)) /. 4.0
                             else 0.0
        | _ -> 0.0
      ) in 
      let sum = List.fold_left (fun a e' -> 
          a +. savings_arg_of_primop_nonconst 0.0 p e'
        ) 0.0 es in
      add +. sum +. acc
  | Primop(_, _, _, es) -> let sum = List.fold_left (fun a e' ->
      a +. savings_arg_of_primop_nonconst 0.0 p e'
    ) 0.0 es in
    sum +. acc

let calc_cases program (a : Lang.CPS.Ast.value) (p : Lang.CPS.Ast.var) body case =
  match case with
  | 1 -> (match a with
    | Var v
    | Label v -> (match (MetaData.CPS.Var2Def.try_get v) with
      | Some defa -> (match defa.e_kind with
        | Fix(_, _) ->
          if number_of_escapes v program = 1 && not (is_escaping p program)
          then 6.0
          else 0.0
        | _ -> 0.0)
      | None -> 0.0)
    | _ -> 0.0)
  | 2 -> (match a with
    | Var v | Label v -> (match (MetaData.CPS.Var2Def.try_get v) with
      | Some defa -> (match defa.e_kind with
        | Record(_, _, _) -> foi (times_used_in_select 0 p body)
        | _ -> 0.0)
      | None -> 0.0)
    | _ -> 0.0)
  | 3 -> (match a with
    | Var v | Label v -> (match (MetaData.CPS.Var2Def.try_get v) with
      | Some defa -> (match defa.e_kind with
        | Record(vs, _, _) ->
          if number_of_escapes v program = 1 && not (is_escaping p program)
          then foi (List.length vs + 2)
          else 0.0
        | _ -> 0.0)
      | None -> 0.0)
    | _ -> 0.0)
  | 4 -> (match a with
    | Int _ -> savings_arg_of_branching 0.0 p body
    | _ -> 0.0
    )
  | 5 -> (match a with
    | Int _ | Real _ -> savings_arg_of_primop_const 0.0 p body
    | _ -> 0.0
    )
  | 6 -> (match a with
    | Int _ | Real _ -> savings_arg_of_primop_nonconst 0.0 p body
    | _ -> 0.0
    )
  | _ -> let s = "BetaExpansion::calc_cases -- "
               ^ "wrong number of case (" ^ string_of_int case ^ ")"
         in CompilerLog.error "%s" s; failwith s
  
let rec savings program e = match e.e_kind with
  | App(f, xs) ->
    let f_name = unwrap_label f
    in let (body, formals) = MetaData.CPS.FunName2Def.get f_name
    in List.map2 (savings_for program body) xs formals
      |> fsum
  | _ -> fail "BetaExpansion::savings -- non App node given"
and savings_for program body a p =
  [1; 2; 3; 4; 5; 6]
  |> List.map (calc_cases program a p body)
  |> fsum

let should_inline program e = match e.e_kind with
  | App(f, xs) ->
    (match MetaData.CPS.FunName2Def.try_get (unwrap_label f) with
      | None -> false
      | Some(body,_) ->
        let b = foi (bigO body)
        in let s = savings program e
        in let k = foi (bigO e)
        in let f_name = unwrap_label f
        in let m = foi (MetaData.CPS.FuncCallCount.get f_name)
        in let n = foi (MetaData.CPS.BetaExpandedCount.get e.e_tag)
        in let threshold = magic_C_const -.
          n *. magic_D_const -. (foi !rounds_so_far) *. magic_E_const
        in b -. s -. k -. b /. m  < threshold)
  | _ -> fail "BetaExpansion::should_inline -- non App node given"

let rec trans program expr = { expr with e_kind = match expr.e_kind with
  | Record(vs, x, e) -> Record(vs, x, trans program e)
  | Select(i, v, x, e) -> Select(i, v, x, trans program e)
  | Offset(i, v, x, e) -> Offset(i, v, x, trans program e)
  | App(f, xs) ->
      if should_inline program expr
      then BetaContraction.inlined_function expr
      else expr.e_kind
  | Fix(decls, e) -> let decls' = List.map (fun
      (fname, vs, b) -> (fname, vs, trans program b)
    ) decls in
    Fix(decls', trans program e)
  | FFI(name, vs, x, e) ->
    FFI(name, vs, x, trans program e)
  | Switch(v, es) -> Switch(v, List.map (trans program) es)
  | Primop(p, vs, xs, es) -> Primop(p, vs, xs, List.map (trans program) es)
  }

let transform expr =
  let transformed_expr = trans expr expr
  in begin
    rounds_so_far := !rounds_so_far + 1;
    transformed_expr;
  end

let c_beta_expansion = Contract.create
  ~description: "One pass of Beta-Expansion"
  ~languages: [Language.CPS]
  "transform:beta_expansion"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_CPS
    ~target: Compiler.Lang_CPS
    ~name:   "CPS:beta_expansion"
    ~require:
      [ Lang.CPS.Contracts.right_scopes
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ]
    ~contracts:
      [ Lang.CPS.Contracts.right_scopes
      ; Lang.CPS.Contracts.unique_vars
      ; Analysis.CPS.FunName2Def.contract
      ; Analysis.CPS.FuncCallCount.contract
      ; Analysis.CPS.Var2Def.contract
      ; c_beta_expansion
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.CPS.Contracts.unique_tags
      ; Contract.saves_contract Lang.CPS.Contracts.primop_arity
      ; Contract.saves_contract Lang.CPS.Contracts.closure_passing_style
      ; Contract.saves_contract Analysis.CPS.FunName2Def.contract
      ; Contract.saves_contract Analysis.CPS.FuncCallCount.contract
      ; Contract.saves_contract Analysis.CPS.Var2Def.contract
      ]
    transform
