module L = Language
include CompilerDB.Types

exception Bad_contract_language of L.t * Contract.t

let check_contract_language lang contr =
  match Contract.languages contr with
  | None    -> ()
  | Some ls ->
    if List.exists (fun l -> l = lang) ls then ()
    else raise (Bad_contract_language(lang, contr))

let check_contract_rule_language source target rule =
  match rule with
  | (cnd, (Contract.Add cs | Contract.Set cs | Contract.Remove cs)) ->
    List.iter (check_contract_language source) cnd;
    List.iter (check_contract_language target) cs

let print_bad_contract_language what name lang contr =
  let open Printing.Box in
  print_stderr (box
  [ box
    [ text ~attributes:[ Base BA_Underline; Base (BA_FgColor Red) ]
        "ERROR:"
    ; white_sep (text "in")
    ; white_sep (text what)
    ; white_sep (text ~attributes: [ Constant ] name)
    ; text ":"
    ] |> br
  ; indent 2 (box
    [ box
      [ text "Contract"
      ; white_sep (text ~attributes: [ Constant ] (Contract.name contr))
      ]
    ; box
      [ white_sep (text "is not available for the")
      ; white_sep (text ~attributes: [ Constant ] (L.name lang))
      ; white_sep (text "language.")
      ]
    ])
  ])

let register_analysis 
    ~lang 
    ~name 
    ?(weight=1.0)
    ?(require=[]) 
    ?(contracts=[])
    ?(contract_rules=[])
    analyse =
  begin try
    let lang = Language.to_language lang in
    List.iter (check_contract_language lang) require;
    List.iter (check_contract_language lang) contracts;
    List.iter (check_contract_rule_language lang lang) contract_rules
  with
  | Bad_contract_language(lang, c) ->
    print_bad_contract_language "analysis" name lang c
  end;
  let analysis_obj = CompilerDB.Analysis.Analysis(lang,
    { CompilerDB.Analysis.analyse        = analyse
    ; CompilerDB.Analysis.name           = name
    ; CompilerDB.Analysis.weight         = weight
    ; CompilerDB.Analysis.require        = require
    ; CompilerDB.Analysis.contract_rules = 
        ([], Contract.Add contracts) :: contract_rules
    })
  in
  CompilerDB.AnalysisDB.register_analysis analysis_obj

let register_contract_checker ~lang ~contract ~name checker =
  begin try
    let lang = Language.to_language lang in
    check_contract_language lang contract
  with
  | Bad_contract_language(lang, c) ->
    print_bad_contract_language "contract checker" name lang c
  end;
  let checker_obj = CompilerDB.ContractChecker.ContractChecker(lang,
    { CompilerDB.ContractChecker.checker  = checker
    ; CompilerDB.ContractChecker.contract = contract
    ; CompilerDB.ContractChecker.name     = name
    })
  in
  CompilerDB.ContractCheckerDB.register_contract_checker checker_obj

let register_evaluator ~lang ~name ?(require=[]) eval =
  begin try
    let lang = Language.to_language lang in
    List.iter (check_contract_language lang) require
  with
  | Bad_contract_language(lang, c) ->
    print_bad_contract_language "evaluator" name lang c
  end;
  let eval_obj = CompilerDB.Evaluator.Evaluator(lang,
    { CompilerDB.Evaluator.eval    = eval
    ; CompilerDB.Evaluator.name    = name
    ; CompilerDB.Evaluator.require = require
    })
  in
  CompilerDB.EvaluatorDB.register_evaluator eval_obj

let register_parser ~lang ~name ?extension ?(contracts=[]) parser_func =
  begin try
    let lang = Language.to_language lang in
    List.iter (check_contract_language lang) contracts
  with
  | Bad_contract_language(lang, c) ->
    print_bad_contract_language "parser" name lang c
  end;
  let parser_obj = CompilerDB.Parser.Parser(lang,
    { CompilerDB.Parser.parser_func = parser_func
    ; CompilerDB.Parser.name        = name
    ; CompilerDB.Parser.extension   = extension
    ; CompilerDB.Parser.contracts   = Contract.Set.of_list contracts
    })
  in
  CompilerDB.ParserDB.register_parser parser_obj

let register_pretty_printer ~lang ~name ?(require=[]) pretty =
  begin try
    let lang = Language.to_language lang in
    List.iter (check_contract_language lang) require
  with
  | Bad_contract_language(lang, c) ->
    print_bad_contract_language "pretty-printer" name lang c
  end;
  let pretty_obj = CompilerDB.Pretty.Pretty(lang,
    { CompilerDB.Pretty.pretty  = pretty
    ; CompilerDB.Pretty.name    = name
    ; CompilerDB.Pretty.require = require
    })
  in
  CompilerDB.PrettyDB.register_pretty_printer pretty_obj

let register_transformation ~source ~target 
    ~name
    ?(weight=1.0)
    ?(require=[])
    ?(contracts=[]) 
    ?(contract_rules=[]) 
    transform =
  begin try
    let source = Language.to_language source in
    let target = Language.to_language target in
    List.iter (check_contract_language source) require;
    List.iter (check_contract_language target) contracts;
    List.iter (check_contract_rule_language source target) contract_rules
  with
  | Bad_contract_language(lang, c) ->
    print_bad_contract_language "transformation" name lang c
  end;
  let transform_obj = CompilerDB.Transform.Transform(source, target,
    { CompilerDB.Transform.transform      = transform
    ; CompilerDB.Transform.name           = name
    ; CompilerDB.Transform.weight         = weight
    ; CompilerDB.Transform.require        = require
    ; CompilerDB.Transform.contract_rules = 
        ([], Contract.Set contracts) :: contract_rules
    }) in
  CompilerDB.TransformDB.register_transformation transform_obj

let pretty_state state =
  let module Box = Printing.Box in
  match state with
  | State(lang, _, contracts) ->
    begin match Contract.Set.elements contracts with
    | [] -> Box.text (Language.to_string lang)
    | contracts ->
      Box.box
      [ Box.box
        [ Box.text (Language.to_string lang)
        ; Box.white_sep (Box.text "with")
        ]
      ; Box.indent 2 (Box.box
        (List.map (fun c -> 
            Box.white_sep (Box.text (Contract.name c))
          ) contracts))
      ]
    end
