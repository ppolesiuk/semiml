
val set_check_contracts : bool -> unit
val get_check_contracts : unit -> bool

val set_internal_log_handle : out_channel option -> unit
val get_internal_log_handle : unit -> out_channel option
