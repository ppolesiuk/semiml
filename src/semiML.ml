
let _ =
  Components.init ()

let meta_commands = Queue.create ()

let comma_regexp = Str.regexp ","

let usage_string =
  "Usage: semiML [OPTION]... [FILE]...\nAvailable OPTIONs are:"

let cmd_args_options = Arg.align
  [ "-eval",
    Arg.Unit (fun () -> Queue.add MetaCommand.MetaCmd_Eval meta_commands),
    " Evaluate a program"
  ; "-pretty",
    Arg.Unit (fun () -> Queue.add MetaCommand.MetaCmd_Pretty meta_commands),
    " Pretty-print a program"
  ; "-require",
    Arg.String (fun str ->
      let cmd = MetaCommand.MetaCmd_Require (Str.split comma_regexp str) in
      Queue.add cmd meta_commands),
    "CONTRACTS Compile to a state satisfying given CONTRACTS, "
    ^ "where CONTRACTS is a comma-separated list of contract names."
  ; "-echo",
    Arg.String (fun str ->
      let cmd = MetaCommand.MetaCmd_Echo str in
      Queue.add cmd meta_commands),
    "STRING Just print the STRING."
  ; "-sep",
    Arg.Unit (fun () ->
      let cmd = MetaCommand.MetaCmd_Echo (String.make 80 '=') in
      Queue.add cmd meta_commands),
    " Print the separator (same as -echo ===...===)."
  ; "-check-contracts",
    Arg.Unit (fun () -> Settings.set_check_contracts true),
    " Dynamically checks all checkable contracts during the compilation."
  ; "-internal-log-file",
    Arg.String (fun filename ->
      match filename with
      | "stderr" -> Settings.set_internal_log_handle (Some stderr)
      | _        -> Settings.set_internal_log_handle (Some (open_out (filename)))
    ),
    "FILENAME|stderr Write internal compilation log to FILENAME or stderr"
  ; "-list-contracts",
    Arg.Unit (fun () -> Contract.iter (fun c ->
      Printing.Box.print_stdout (Contract.pretty c)
    )),
    " List all registered contracts"
  ; "-colors",
    Arg.Symbol(["no"; "auto"; "force"], fun policy ->
      Printing.ColorScheme.set_color_policy (
        match policy with
        | "no"    -> Printing.ColorScheme.NoColors
        | "auto"  -> Printing.ColorScheme.AutoDetect
        | "force" -> Printing.ColorScheme.ForceColors
        | _       -> assert false)),
    " Set coloring policy for pretty-printing."
  ; "-color-scheme",
    Arg.Symbol(["dark";"default"], fun scheme ->
      Printing.ColorScheme.set_color_scheme (
        match scheme with
        | "dark"    -> Printing.ColorScheme.dark_scheme
        | "default" -> Printing.ColorScheme.default_scheme
        | _         -> assert false)),
    " Set color scheme for pretty-printing."
  ; "-opt-cps-basic-phase1",
    Arg.Unit (fun () ->
      Queue.add MacroCmd.CPS_Opt.basic_phase1 meta_commands),
    " Perform phase1 of basic CPS optimizations"
  ; "-opt-cps-basic-phase1-loop",
    Arg.Unit (fun () ->
      Queue.add MacroCmd.CPS_Opt.basic_phase1_loop meta_commands),
    " Perform phase1 of basic CPS optimizations in a loop"
  ; "-opt-cps-basic",
    Arg.Unit (fun () ->
      Queue.add MacroCmd.CPS_Opt.basic meta_commands),
    " Perform basic CPS optimizations"
  ]

let proc_fname fname =
  let state = CompilerGraph.parse_file fname in
  let _ : Compiler.state =
    Queue.fold MetaCommand.run state meta_commands
  in ()

let _ =
  try Arg.parse cmd_args_options proc_fname usage_string with
  | Errors.Fatal_error -> ()
