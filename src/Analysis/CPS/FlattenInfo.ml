open Lang.CPS.Ast
open MetaData.CPS.FlatInfo

let unwrap_label value = match value with
  | Var(l) | Label(l) -> l
  | _ -> failwith ("Flatten::unwrap_label -- application of non function")

let (<<) f g x = f(g(x))

let rec select v i =
  match v with
  | Var(v') ->(match MetaData.CPS.FlatInfo.try_get v' with
              | Some(ARGinfo(j)) -> j := max i !j; ()
              | _ -> ())
  | Label(v') -> select (Var(v')) i; ()
  | _ -> ()

let rec escape v = 
  match v with
  | Var(v') ->(  match MetaData.CPS.FlatInfo.try_get v' with
              | Some(FNinfo({escape=r;_})) -> r := true;()
              | _ -> ())
  | Label(v') -> escape (Var(v')) ;()
  | _ -> ()

let botlist = List.map (fun _ -> BOT)

let enterFN  f =
  match f with 
  | (f, vl, _) ->  
    let info = FNinfo({arity = ref(botlist vl); alias = ref None; escape = ref false}) in
    MetaData.CPS.FlatInfo.set f info;
    List.iter (fun v -> MetaData.CPS.FlatInfo.set v  (ARGinfo(ref (-1) )) ) vl ;()

let field v = 
  match v with
  | (p, Selp(i, _)) -> select p i
  | (p, _) -> escape p


let maxregs = 15

exception Found

let rec findFetch v j body = 
  let rec f expr = 
    match expr.e_kind with
      | Record(vs, x, e) -> List.iter g vs; f e
      | Select(i, Var v', x, e) -> if v==v' && i == j then raise Found else f e
      | Select(i, _, x, e) -> f e
      | Offset(i, _, x, e) -> f e
      | Fix(decls, e) -> f e
      | Switch(_, es) -> not(List.exists (not << (findFetch v j)) es)
      | Primop(p, vs, xs, es) -> not(List.exists (not << (findFetch v j)) es)
      | _ -> false
  and g x =
   match x with
    | (Var v',Selp(i,_)) -> if v==v' && i == j then raise Found else ()
    | (Var v',Offp(i)) -> if v==v' && i == j then raise Found else ()
    | _ -> ()
  in try f body with
    | Found -> true
    | _ -> false

let checkFlatten = fun (fname, vl, body) ->
 ( match try_get fname with
  | Some(FNinfo({arity=a; alias; escape})) ->

    let rec loop arg =
      match arg with
      | v::vl, a::al, headroom -> 
        (match a, get v with
        | (COUNT(c, flag), ARGinfo(k)) ->
          if !k > (-1) && headroom - (c - 1) >= 0 &&

             (not(flag || !escape) || (!k==c-1 && findFetch v !k body))
          then(
            a::(loop(vl,al,headroom-c+1)))
          else
            TOP::(loop(vl,al,headroom))
        | _ -> TOP::(loop(vl,al,headroom)))
      | _ -> []
      in begin
        let a' = loop(vl, !a, maxregs -1- (List.length !a)) in
          a := a';
        if List.exists (fun x -> match x with 
                                 | COUNT(_,_)-> true 
                                 | _ -> false ) a' 
        then
          (alias := Some(Common.Var.copy fname))
        else
          ()
      end
      
  | _ -> ())

let rec analyse expr =
  match expr.e_kind with
  | Record(vl, w, e) -> (MetaData.CPS.FlatInfo.set w (RECinfo(List.length vl));
                        List.iter field vl;
                        analyse e)
  | Select(i, v, _, e)    -> select v i; analyse e
  | Offset(i, v, w, e)    -> escape v; analyse e
  | Switch(i, e)         -> escape i; List.iter analyse e
  | FFI(_, vl, _, e)     -> analyse e
  | Primop(_, vl, _, e)   -> List.iter analyse e
  | App(Var(name), vl) -> 
      (let rec loop arg =
      match arg with
      | t::r, (Var(v))::v', n ->
        (match t, try_get v with
          (BOT,Some(RECinfo sz)) -> loop(COUNT(sz,false)::r, (Var(v))::v', n)
          | (BOT,_) -> UNK::(loop(r,v',n+1))     
          | (UNK,Some(RECinfo sz)) -> loop(COUNT(sz,true)::r, (Var(v))::v', n)
          | (UNK,_) -> UNK::(loop(r,v',n+1))
          | (COUNT(a,_),Some(RECinfo sz)) -> 
              if a = sz then t::(loop(r,v',n+1))
              else TOP::(loop(r,v',n+1))
          | (COUNT(a,_),_) -> COUNT(a,true)::(loop(r,v',n+1))
          | _,_ -> TOP::(loop(r,v',n+1)))

      | (_::r, _::vl,n) -> TOP::(loop(r, vl,n+1))    
      | _ -> []
    in List.iter escape vl;
      (match MetaData.CPS.FlatInfo.try_get name with
      | Some(FNinfo({arity=a; _})) ->  a := (loop(!a, vl, 0));() 
      | _ -> () ))
  | App(f, vl) -> List.iter escape vl
  | Fix(decls, e) -> List.iter enterFN decls; 
                   List.iter (fun (_,_,body) -> analyse body) decls;
                   analyse e;
                   List.iter checkFlatten decls

let clean_analyse expr =
  MetaData.CPS.FlatInfo.reset();
  analyse expr

let contract = Contract.create
  ~description: "Gather info for flatening function"
  ~languages: [Language.CPS]
  "analyse:flatInfo"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:flatInfo"
    ~require:   [ ]
    ~contracts: [ contract ]
    analyse
