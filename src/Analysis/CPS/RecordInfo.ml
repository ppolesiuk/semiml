open Lang.CPS.Ast

type info = RECInfo of int * (value * accesspath) list

let rec analyse expr =
   match expr.e_kind with
  | Record(vs, x, e) -> MetaData.CPS.RecordInfo.set x (0, vs); analyse e
  | Select(i, v, x, e) -> analyse e
  | Offset(i, Var y, x, e) ->
    begin match MetaData.CPS.RecordInfo.try_get x with
    | Some (off, vs) -> MetaData.CPS.RecordInfo.set x (off + i, vs)
    | None -> ()
    end; analyse e
  | Offset(_, _, _, e) -> analyse e
  | Fix(decls, e) ->
    let analyse_body = fun (fname, vs, body) -> analyse body in
       List.iter analyse_body decls; analyse e
  | Switch(v, es) -> List.iter analyse es
  | Primop(op, vs, xs, es) -> List.iter analyse es
  | _ -> ()

let clean_analyse expr =
  MetaData.CPS.RecordInfo.reset();
  analyse expr

let contract = Contract.create
  ~languages: [Language.CPS]
  "analyse:recordinfo"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:recordinfo"
    ~require:   [ Common.Contracts.unique_vars ]
    ~contracts: [ contract ]
    clean_analyse
