open Lang.CPS.Ast

let rec use v = 
  match v with 
  | Var v'-> MetaData.CPS.VarUseCount.inc v' 
  | Label v' -> MetaData.CPS.VarUseCount.inc v' 
  | _ -> ()

let rec analyse expr = 
  match expr.e_kind with
  | Record(vl, _, subexpr) -> 
    List.iter (fun (v, _) -> use v) vl ; analyse subexpr
  | Select(_, v, _, subexpr)    -> use v; analyse subexpr
  | Offset(_, v, _, subexpr)    -> use v; analyse subexpr
  | Switch(v, subexprs)         -> use v; List.iter analyse subexprs
  | FFI(_, vl, _, subexpr)      -> List.iter use vl; analyse subexpr
  | Primop(_, vl, _, subexprs)  ->
    List.iter use vl; List.iter analyse subexprs
  | Fix(funcs, subexpr)         ->
      List.iter add_func funcs;
      analyse subexpr
  | App(fname, args) -> use fname; List.iter use args
and add_func (name, args, expr) = 
    analyse expr 

let clean_analyse expr =
  MetaData.CPS.VarUseCount.reset();
  analyse expr

let contract = Contract.create
  ~description: "Count how many times variable is used"
  ~languages: [Language.CPS]
  "analyse:var_use_count"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:var_use_count"
    ~require:   [ ]
    ~contracts: [ contract ]
    analyse
