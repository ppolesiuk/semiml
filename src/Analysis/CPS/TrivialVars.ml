open Lang.CPS.Ast

let set_as_trivial    x = MetaData.CPS.TrivialVars.set x true
and set_as_nontrivial x = MetaData.CPS.TrivialVars.set x false

let rec analyse expr =
  match expr.e_kind with
  | Record(_, var, subexpr)       ->
      set_as_nontrivial var;
      analyse subexpr
  | Select(_, _, var, subexpr)    ->
      set_as_trivial var;
      analyse subexpr
  | Offset(_, _, var, subexpr)    ->
      set_as_trivial var;
      analyse subexpr
  | Switch(_, subexprs)           ->
      List.iter analyse subexprs
  | Fix(funcs, subexpr)           ->
      List.iter analyse_func funcs;
      analyse subexpr
  | FFI(_, _, x, e) ->
    set_as_trivial x;
    analyse e
  | Primop(op, _, vars, subexprs) ->
      analyse_primop op vars;
      List.iter analyse subexprs
  | App(_, _)                     -> ()
and analyse_func func = 
  let (name, args, body) = func
  in
    set_as_nontrivial name;
    List.iter set_as_trivial args;
    analyse body
and analyse_primop op vars = match op with
  | Lang.CPS.Primop.Arith a -> 
      let open Common.Primop.Arith in (match a with
        | Add | Sub | Mul | Div | Neg -> 
          List.iter set_as_trivial vars
        | _ ->
          List.iter set_as_nontrivial vars
      )
  | Lang.CPS.Primop.FArith a -> 
      let open Common.Primop.FArith in (match a with
        | FAdd | FSub | FMul | FDiv -> 
          List.iter set_as_trivial vars
        | _ ->
          List.iter set_as_nontrivial vars
      )
  | Lang.CPS.Primop.Mem a -> 
      let open Common.Primop.Mem in (match a with
        | Deref | OrdOf | Subscript -> 
          List.iter set_as_trivial vars
        | _ ->
          List.iter set_as_nontrivial vars
      )
  | Lang.CPS.Primop.Repr a -> 
      let open Common.Primop.Repr in (match a with
        | ALength | SLength -> 
          List.iter set_as_trivial vars
        | _ ->
          List.iter set_as_nontrivial vars
      )
  | Lang.CPS.Primop.Exn a -> 
      let open Common.Primop.Exn in (match a with
        | GetHdlr -> 
          List.iter set_as_trivial vars
        | _ ->
          List.iter set_as_nontrivial vars
      )

let contract = Contract.create
  ~description: "Trivial variable analysis"
  ~languages: [Language.CPS]
  "analyse:trivial_vars"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:trivial_vars"
    ~require:   [ ]
    ~contracts: [ contract ]
    analyse

