open Lang.CPS.Ast

let rec expr_cost expr =
  match expr.e_kind with
    | Record(vals, _, subexpr) ->
        (List.length vals) + (expr_cost subexpr) + 2
    | Select(_, _, _, subexpr) | Offset(_, _, _, subexpr) ->
        (expr_cost subexpr) + 1
    | App(_, args) ->
        (List.length args) + 1
    | Fix(funcs, subexpr) ->
        (funcs_costs funcs) + (expr_cost subexpr)
    | FFI(_, _, _, e) ->
        expr_cost e + 10 (* TODO: how to calculate cost of a FFI? *)
    | Switch(_, subexprs) ->
        List.fold_left (+) 4 (List.map (fun subexpr -> (expr_cost subexpr) + 1) subexprs)
    | Primop(_, args, rets, subexprs) ->
        (List.length args) + (List.length rets) + (List.fold_left (+) 0 (List.map expr_cost subexprs))
and func_cost (_, _, body) =
  (expr_cost body) + 1
and funcs_costs funcs = 
  List.fold_left (+) 0 (List.map func_cost funcs)

