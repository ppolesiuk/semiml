open Lang.CPS.Ast

let rec analyse expr =
  match expr.e_kind with
  | Record(_, _, subexpr)       -> analyse subexpr
  | Select(_, _, _, subexpr)    -> analyse subexpr
  | Offset(_, _, _, subexpr)    -> analyse subexpr
  | Switch(_, subexprs)         -> List.iter analyse subexprs
  | Primop(_, _, _, subexprs)   -> List.iter analyse subexprs
  | Fix(funcs, subexpr)         ->
      List.iter add_func funcs;
      analyse subexpr
  | FFI(_, _, _, e)             -> analyse e
  | App(Var(name), _)           ->
      MetaData.CPS.FuncCallCount.inc name
  | App(Label(name), _)         ->
      MetaData.CPS.FuncCallCount.inc name
  | App(_, _)                   -> ()
and add_func (name, args, expr) =
  MetaData.CPS.FuncCallCount.add name;
  List.iter MetaData.CPS.FuncCallCount.add args;
  analyse expr

let clean_analyse expr =
  MetaData.CPS.FuncCallCount.reset();
  analyse expr

let contract = Contract.create
  ~description: "Count how much each function is approximately called"
  ~languages: [Language.CPS]
  "analyse:func_call_count"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:func_call_count"
    ~require:   [ ]
    ~contracts: [ contract ]
    clean_analyse
