open Lang.CPS.Ast

let rec escape v = 
  match v with
  | Var(v') ->(  match MetaData.CPS.FunctionEscape.try_get v' with
              | Some(e) -> e := true;()
              | _ -> ())
  | Label(v') -> escape (Var(v')) ;()
  | _ -> ()

let field v = 
  match v with
  | (p, Selp(i, _)) -> ()
  | (p, _) -> escape p

let enterFN  f =
  match f with 
  | (f, vl, _) -> MetaData.CPS.FunctionEscape.set f (ref false)


let rec analyse expr =
  match expr.e_kind with
  | Record(vl, w, e) -> List.iter field vl; analyse e
  | Select(i, v, _, e)    -> analyse e
  | Offset(i, v, w, e)    -> escape v; analyse e
  | Switch(i, e)          -> escape i; List.iter analyse e
  | FFI(_, vl, _, e)      -> List.iter escape vl; analyse e
  | Primop(_, vl, _, e)   -> List.iter analyse e
  | App(f, vl) -> List.iter escape vl
  | Fix(decls, e) -> (List.iter enterFN decls; 
                   List.iter (fun (_,_,body) -> analyse body) decls;
                   analyse e;())

let clean_analyse expr =
  MetaData.CPS.FunctionEscape.reset();
  analyse expr

let contract = Contract.create
  ~description: "Count how many times variable is used"
  ~languages: [Language.CPS]
  "analyse:var_use_count"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:var_use_count"
    ~require:   [ ]
    ~contracts: [ contract ]
    analyse
