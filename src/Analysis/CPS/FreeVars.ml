
open Lang.CPS.Ast
open List

module S = Common.Var.Set

let getVar : value -> S.t =
  fun (value) ->
    match value with
    | Var x -> S.singleton x
    | _ -> S.empty

let toSet : var list -> S.t =
 fun vars ->
   let add = fun set elem -> S.add elem set in
   fold_left add S.empty vars

let unionAll sets = fold_left S.union S.empty sets

let getVars : value list -> S.t =
  fun vals -> unionAll (map getVar vals)

let functionNames functions = toSet (map (fun (name, _, _) -> name) functions)

let rec analyse_expr on_demand expr =
  let free_vars =
    match expr.e_kind with
    | Record(vals, var, expr) -> S.remove var (analyse_expr on_demand expr)
    | Select(index, record, var, expr) ->
      S.union
        (getVar record)
        (S.remove var (analyse_expr on_demand expr))
    | Offset(offset, record, var, expr) ->
      S.union
        (getVar record)
        (S.remove var (analyse_expr on_demand expr))
    | App(func, vals) ->
      S.union
        (getVar func)
        (getVars vals)
    | Fix(functions, expr) ->
      S.diff
        (S.union
          (analyse_expr on_demand expr)
          (unionAll (map (analyseFun on_demand) functions)))
        (functionNames functions)
    | Switch (value, exprs) ->
      S.union
        (getVar value)
        (unionAll (map (analyse_expr on_demand) exprs))
    | FFI(_, vals, x, e) ->
      S.union (getVars vals) (S.remove x (analyse_expr on_demand e))
    | Primop (op, vals, vars, exprs) ->
      S.union
        (getVars vals)
        (S.diff
          (unionAll (map (analyse_expr on_demand) exprs))
          (toSet vars)
        )
  in
  let _ = if not on_demand then 
    MetaData.CPS.FreeVars.set expr.e_tag free_vars
  else ()
  in free_vars
and analyseFun on_demand (name, args, body) =
  S.remove name
    (S.diff (analyse_expr on_demand body) (toSet args))

let analyse expr =
  let _ : Common.Var.Set.t = analyse_expr false expr in ()

let calc_on_demand expr = analyse_expr true expr

let contract = Contract.create
  ~description: "Free variable analysis"
  ~languages: [Language.CPS]
  "analyse:free_vars"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:free_vars"
    ~require:   [ Common.Contracts.unique_tags ]
    ~contracts: [ contract ]
    analyse
