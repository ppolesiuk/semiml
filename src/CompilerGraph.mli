
type node = CompilerDB.GraphTypes.node =
| Node : 'lang Compiler.language * Contract.Set.t -> node

type command = CompilerDB.GraphTypes.command =
| Cmd_Analyse   of CompilerDB.Analysis.t
| Cmd_Eval      of CompilerDB.Evaluator.t
| Cmd_Pretty    of CompilerDB.Pretty.t
| Cmd_Transform of CompilerDB.Transform.t

type path = command list

val node_of_state : Compiler.state -> node

val find_path : node -> node -> path
val find_path_to_eval   : node -> path
val find_path_to_pretty : node -> path

val find_path_to_contracts : node -> Contract.t list -> path
val find_path_to_contract_names : node -> string list -> path

val run_path : path -> Compiler.state -> Compiler.state

val parse_file : string -> Compiler.state
