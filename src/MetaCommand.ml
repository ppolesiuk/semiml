module Box = Printing.Box

type t =
| MetaCmd_Eval
| MetaCmd_Pretty
| MetaCmd_Require  of string list
| MetaCmd_RequireC of Contract.t list
| MetaCmd_Echo     of string
| MetaCmd_Seq      of t list
| MetaCmd_Code     of (Compiler.state -> unit)
| MetaCmd_If       of (Compiler.state -> bool) * t * t
| MetaCmd_While    of (Compiler.state -> bool) * t
| MetaCmd_DoWhile  of t * (Compiler.state -> bool)
| MetaCmd_Dynamic  of (Compiler.state -> t)

let rec run state mc =
  let node = CompilerGraph.node_of_state state in
  match mc with
  | MetaCmd_Eval ->
    begin match CompilerGraph.find_path_to_eval node with
    | path -> CompilerGraph.run_path path state
    | exception Not_found ->
      Errors.error_b
      [ Box.text "Cannot evaluate program in state"
      ; Box.indent 2 (Box.white_sep (Compiler.pretty_state state))
      ];
      raise Errors.Fatal_error
    end
  | MetaCmd_Pretty ->
    begin match CompilerGraph.find_path_to_pretty node with
    | path -> CompilerGraph.run_path path state
    | exception Not_found ->
      Errors.error_b
      [ Box.text "Cannot pretty-print program in state"
      ; Box.indent 2 (Box.white_sep (Compiler.pretty_state state))
      ];
      raise Errors.Fatal_error
    end
  | MetaCmd_Require contracts ->
    begin match CompilerGraph.find_path_to_contract_names node contracts with
    | path -> CompilerGraph.run_path path state
    | exception Not_found ->
      Errors.error_b
      [ Box.text "Program in state"
      ; Box.indent 2 (Box.white_sep (Compiler.pretty_state state))
      ; Box.white_sep (Box.text "cannot simultaneously satisfy contract(s):")
      ; Box.indent 2 (Box.box 
          (List.map (fun c -> Box.white_sep (Box.text c)) contracts))
      ];
      raise Errors.Fatal_error
    end
  | MetaCmd_RequireC contracts ->
    begin match CompilerGraph.find_path_to_contracts node contracts with
    | path -> CompilerGraph.run_path path state
    | exception Not_found ->
      Errors.error_b
      [ Box.text "Program in state"
      ; Box.indent 2 (Box.white_sep (Compiler.pretty_state state))
      ; Box.white_sep (Box.text "cannot simultaneously satisfy contract(s):")
      ; Box.indent 2 (Box.box 
          (List.map (fun c ->
            Box.white_sep (Box.text (Contract.name c))
          ) contracts))
      ];
      raise Errors.Fatal_error
    end
  | MetaCmd_Echo str ->
    print_endline str;
    state
  | MetaCmd_Seq mcs -> List.fold_left run state mcs
  | MetaCmd_Code f ->
    f state;
    state
  | MetaCmd_If(f, mc1, mc2) ->
    if f state then run state mc1 else run state mc2
  | MetaCmd_While(f, mc_body) ->
    if f state then run (run state mc_body) mc
    else state
  | MetaCmd_DoWhile(mc_body, f) ->
    run (run state mc_body) (MetaCmd_While(f, mc_body))
  | MetaCmd_Dynamic f ->
    run state (f state)
