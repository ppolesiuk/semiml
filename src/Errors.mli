
exception Fatal_error
exception Not_implemented of string

val error_p   : Common.Position.t -> ('a, out_channel, unit) format -> 'a
val warning_p : Common.Position.t -> ('a, out_channel, unit) format -> 'a
val note_p    : Common.Position.t -> ('a, out_channel, unit) format -> 'a

val error_lp   : Lexing.position -> ('a, out_channel, unit) format -> 'a
val warning_lp : Lexing.position -> ('a, out_channel, unit) format -> 'a
val note_lp    : Lexing.position -> ('a, out_channel, unit) format -> 'a

val error_pp   : Lexing.position -> Lexing.position -> 
  ('a, out_channel, unit) format -> 'a
val warning_pp : Lexing.position -> Lexing.position -> 
  ('a, out_channel, unit) format -> 'a
val note_pp    : Lexing.position -> Lexing.position -> 
  ('a, out_channel, unit) format -> 'a

val error_no_pos   : ('a, out_channel, unit) format -> 'a
val warning_no_pos : ('a, out_channel, unit) format -> 'a
val note_no_pos    : ('a, out_channel, unit) format -> 'a

val error   : ?tag: Common.Tag.t -> ('a, out_channel, unit) format -> 'a
val warning : ?tag: Common.Tag.t -> ('a, out_channel, unit) format -> 'a
val note    : ?tag: Common.Tag.t -> ('a, out_channel, unit) format -> 'a

val error_b   : ?tag: Common.Tag.t -> Printing.Box.t list -> unit
val warning_b : ?tag: Common.Tag.t -> Printing.Box.t list -> unit
val note_b    : ?tag: Common.Tag.t -> Printing.Box.t list -> unit

val not_implemented : string -> 'a
