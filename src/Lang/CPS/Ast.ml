type var  = Common.Var.t

type accesspath =
| Offp      of int
| Selp      of int * accesspath

type value =
| Var       of var
| Label     of var
| Int       of int64
| Real      of Common.Real.t
| String    of string

type expr =
  { e_tag  : Common.Tag.t
  ; e_kind : expr_kind
  }
and expr_kind =
| Record    of (value * accesspath) list * var * expr
| Select    of int * value * var * expr
| Offset    of int * value * var * expr
| App       of value * value list
| Fix       of (var * var list * expr) list * expr
| Switch    of value * expr list
| FFI       of string * value list * var * expr
| Primop    of Primop.t * value list * var list * expr list

let top_cont = Common.Var.create ~name: "top_cont" ()

let get_continuations expr =
  match expr.e_kind with
  | Record(_, _, c)
  | Select(_, _, _, c)
  | Offset(_, _, _, c)
  | Fix(_, c) 
  | FFI(_, _, _, c) ->
      [c]
  | Switch(_, cs)
  | Primop(_, _, _, cs) ->
      cs
  | App(_, _) ->
      []

let get_args expr =
  match expr.e_kind with
  | Record(vas, _, _) ->
    List.map fst vas
  | Select(_, v, _, _)
  | Offset(_, v, _, _)
  | Switch(v, _) ->
      [v]
  | App(v, vs) ->
      v :: vs
  | Primop(_, values, _, _) | FFI(_, values, _, _) ->
      values
  | Fix(_, _) ->
      []

let get_results expr =
  match expr.e_kind with
  | Record(_, res, _)
  | Select(_, _, res, _)
  | Offset(_, _, res, _)
  | FFI(_, _, res, _) ->
      [res]
  | Primop(_, _, res, _) ->
      res
  | Fix(bindings, _) ->
      List.map (fun (r, _, _) -> r) bindings
  | Switch(_, _)
  | App(_, _) ->
      []
