type loc = int
let nextloc loc = loc + 1

type answer = unit

type dvalue =
| RECORD    of dvalue list * int
| INT       of int64
| REAL      of Common.Real.t
| FUNC      of (dvalue list -> store -> answer)
| STRING    of string
| BYTEARRAY of loc list
| ARRAY     of loc list
| UARRAY    of loc list
and store = (loc * (loc -> dvalue) * (loc -> int64))

let handler_ref = 0
let overflow_exn = INT (Int64.of_int 0)
let div_exn = INT (Int64.of_int 1)

let fetch (_,s,_) i   = s i
let upd   (n,s,u) i v = (n, (fun j -> if i = j then v else s j), u)

let fetchi (_,_,u) i   = u i
let updi   (n,s,u) i v = (n, s, (fun j -> if i = j then v else u j))

let ffi_view =
  let open Common.FFI in
  { val_to_int64  = 
    begin function
    | INT n -> n
    | _     -> failwith "bad FFI value"
    end
  ; val_to_string =
    begin function
    | STRING s -> s
    | _        -> failwith "bad FFI value"
    end
  ; int64_to_val  = (fun n -> INT n)
  ; string_to_val = (fun s -> STRING s)
  }

let rec eq a b =
  match a, b with
  | RECORD (a,i), RECORD (b,j)         -> i = j && eqlist a b
  | INT i, INT j                       -> Int64.compare i j = 0
  | REAL a, REAL b                     -> Common.Real.equal a b
  | STRING a, STRING b                 -> a = b
  | BYTEARRAY [], BYTEARRAY []         -> true
  | BYTEARRAY (a::_), BYTEARRAY (b::_) -> a == b
  | ARRAY [], ARRAY []                 -> true
  | ARRAY (a::_), ARRAY (b::_)         -> a == b
  | UARRAY [], UARRAY []               -> true
  | UARRAY (a::_), UARRAY (b::_)       -> a == b
  | FUNC _, FUNC _                     -> failwith "eq called on functions"
  | _                                  -> false
and eqlist al bl =
  match al, bl with
  | [], []       -> true
  | a::al, b::bl -> eq a b && eqlist al bl
  | _            -> false

let alloc_array s n =
  let rec aux acc s n =
    let (l,gs,us) = s in
    if n = 0 then (s, acc)
    else aux (l :: acc) (upd (nextloc l,gs,us) l (INT 0L)) (n-1)
  in aux [] s n

let alloc_unboxed_array s n =
  let rec aux acc s n =
    let (l,gs,us) = s in
    if n = 0 then (s, acc)
    else aux (l :: acc) (updi (nextloc l,gs,us) l 0L) (n-1)
  in aux [] s n

let boxed = function
  | RECORD _ | STRING _ | BYTEARRAY _ | ARRAY _ | UARRAY _ -> true
  | INT _  -> false
  | REAL _ -> Common.Real.is_boxed ()
  | FUNC _ -> failwith "boxed called on function"

let do_raise ex s =
  match fetch s handler_ref with
  | FUNC f -> f [ex] s
  | _ -> failwith "handler is not a function"

let minint  = Int64.of_int (-1000294734)
let maxint  = Int64.of_int 1000294734
let minreal = Common.Real.of_sml_string "1e-50"
let maxreal = Common.Real.of_sml_string "1e50"

let overflow n c =
  let res = n () in
  if Int64.compare res minint < 0 || Int64.compare res maxint > 0
  then do_raise overflow_exn
  else c [INT res]

let overflowr r c =
  let res = r () in
  if Common.Real.lt res minreal || Common.Real.gt res maxreal
  then do_raise overflow_exn
  else c [REAL res]

let bad_prim tag =
  Errors.error ~tag: tag "Bad primop.";
  raise Errors.Fatal_error

let eval_prim_arith tag op values conts =
  let open Common.Primop.Arith in
  match op, values, conts with
  | Add, [INT i; INT j], [c] ->
    overflow (fun () -> Int64.add i j) c
  | Sub, [INT i; INT j], [c] ->
    overflow (fun () -> Int64.sub i j) c
  | Neg, [INT i], [c] ->
    overflow (fun () -> Int64.neg i) c
  | Mul, [INT i; INT j], [c] ->
    overflow (fun () -> Int64.mul i j) c
  | Div, [INT i; INT j], [c] ->
    if Int64.compare j Int64.zero = 0
    then do_raise div_exn
    else overflow (fun () -> Int64.div i j) c
  | Mod, [INT i; INT j], [c] ->
    overflow (fun () -> Int64.rem i j) c
  | RShift, [INT i; INT j], [c] ->
    c [INT (Int64.shift_left i (Int64.to_int j))]
  | LShift, [INT i; INT j], [c] ->
    c [INT (Int64.shift_right i (Int64.to_int j))]
  | Orb, [INT i; INT j], [c] ->
    c [INT (Int64.logor i j)]
  | Xorb, [INT i; INT j], [c] ->
    c [INT (Int64.logxor i j)]
  | Andb, [INT i; INT j], [c] ->
    c [INT (Int64.logand i j)]
  | Notb, [INT i], [c] ->
    c [INT (Int64.lognot i)]
  | Le, [INT i; INT j], [t; f] ->
    if Int64.compare i j <= 0 then t [] else f []
  | Ge, [INT i; INT j], [t; f] ->
    if Int64.compare i j >= 0 then t [] else f []
  | Lt, [INT i; INT j], [t; f] ->
    if Int64.compare i j < 0 then t [] else f []
  | Gt, [INT i; INT j], [t; f] ->
    if Int64.compare i j > 0 then t [] else f []
  | RangeCheck, [INT i; INT j], [t; f] ->
    if Int64.compare i Int64.zero < 0
    then if Int64.compare j Int64.zero < 0
         then if Int64.compare i j < 0 then t [] else f []
         else t []
    else if Int64.compare i Int64.zero < 0
         then f []
         else if Int64.compare i j < 0 then t [] else f []
  | (Add | Sub | Neg | Mul | Div | Mod | RShift | LShift | Orb | Xorb | Andb
    | Notb | Le | Ge | Lt | Gt | RangeCheck), _, _ ->
    bad_prim tag

let eval_prim_farith tag op values conts =
  let open Common.Primop.FArith in
  match op, values, conts with
  | FAdd, [REAL i; REAL j], [c] ->
    overflowr (fun () -> Common.Real.add i j) c
  | FSub, [REAL i; REAL j], [c] ->
    overflowr (fun () -> Common.Real.sub i j) c
  | FMul, [REAL i; REAL j], [c] ->
    overflowr (fun () -> Common.Real.mul i j) c
  | FDiv, [REAL i; REAL j], [c] ->
    overflowr (fun () -> Common.Real.div i j) c
  | FEq, [REAL i; REAL j], [t; f] ->
    if Common.Real.equal i j then t [] else f []
  | FNeq, [REAL i; REAL j], [t; f] ->
    if Common.Real.equal i j then f [] else t []
  | FLe, [REAL i; REAL j], [t; f] ->
    if Common.Real.le i j then t [] else f []
  | FGe, [REAL i; REAL j], [t; f] ->
    if Common.Real.ge i j then t [] else f []
  | FLt, [REAL i; REAL j], [t; f] ->
    if Common.Real.lt i j then t [] else f []
  | FGt, [REAL i; REAL j], [t; f] ->
    if Common.Real.gt i j then t [] else f []
  | (FAdd | FSub | FMul | FDiv | FEq | FNeq | FLe | FGe | FLt | FGt), _, _ ->
    bad_prim tag

let rec eval_prim_mem tag op values conts =
  let open Common.Primop.Mem in
  match op, values, conts with
  | Deref, [a], [c] ->
    eval_prim_mem tag Subscript [a; INT Int64.zero] [c]
  | Subscript, [ARRAY a; INT n], [c] ->
    (fun s -> c [fetch s (List.nth a (Int64.to_int n))] s)
  | Subscript, [UARRAY a; INT n], [c] ->
    (fun s -> c [INT (fetchi s (List.nth a (Int64.to_int n)))] s)
  | Subscript, [RECORD (a,i); INT j], [c] ->
    c [List.nth a (i + Int64.to_int j)]
  | OrdOf, [STRING a; INT i], [c] ->
    c [INT (Int64.of_int (Char.code (a.[Int64.to_int i])))]
  | OrdOf, [BYTEARRAY a; INT i], [c] ->
    (fun s -> c [INT (fetchi s (List.nth a (Int64.to_int i)))] s)
  | Assign, [a; v], [c] ->
    eval_prim_mem tag Update [a; INT Int64.zero; v] [c]
  | Update, [ARRAY a; INT n; v], [c] ->
    (fun s -> c [] (upd s (List.nth a (Int64.to_int n)) v))
  | Update, [UARRAY a; INT n; INT v], [c] ->
    (fun s -> c [] (updi s (List.nth a (Int64.to_int n)) v))
  | UnboxedAssign, [a; v], [c] ->
    eval_prim_mem tag UnboxedUpdate [a; INT Int64.zero; v] [c]
  | UnboxedUpdate, [ARRAY a; INT n; INT v], [c] ->
    (fun s -> c [] (upd s (List.nth a (Int64.to_int n)) (INT v)))
  | UnboxedUpdate, [UARRAY a; INT n; INT v], [c] ->
    (fun s -> c [] (updi s (List.nth a (Int64.to_int n)) v))
  | Store, [BYTEARRAY a; INT i; INT v], [c] ->
    if Int64.compare v 0L < 0 || Int64.compare v 256L >= 0
    then failwith "UNDEFINDED"
    else (fun s -> c [] (updi s (List.nth a (Int64.to_int i)) v))
  | MakeRef, [v], [c] ->
    (fun (n,s,u) -> c [ARRAY [n]] (upd (nextloc n, s, u) n v))
  | MakeRefUnboxed, [INT v], [c] ->
    (fun (n,s,u) -> c [UARRAY [n]] (updi (nextloc n, s, u) n v))
  | AllocArray, [INT n], [c] ->
    let n = Int64.to_int n in
    if n < 0 then failwith "UNDEFINDED"
    else (fun s ->
      let (s,a) = alloc_array s n in
      c [ARRAY a] s) 
  | AllocUnboxedArray, [INT n], [c] ->
    let n = Int64.to_int n in
    if n < 0 then failwith "UNDEFINDED"
    else (fun s ->
      let (s,a) = alloc_unboxed_array s n in
      c [UARRAY a] s) 
  | AllocByteArray, [INT n], [c] ->
    let n = Int64.to_int n in
    if n < 0 then failwith "UNDEFINDED"
    else (fun s ->
      let (s,a) = alloc_unboxed_array s n in
      c [BYTEARRAY a] s)
  | (Deref | Subscript | OrdOf | Assign | Update | UnboxedAssign
    | UnboxedUpdate | Store | MakeRef | MakeRefUnboxed | AllocArray
    | AllocUnboxedArray | AllocByteArray), _, _ ->
    bad_prim tag

let eval_prim_repr tag op values conts =
  let open Common.Primop.Repr in
  match op, values, conts with
  | Boxed, [v], [t; f] ->
    if boxed v then t [] else f []
  | ALength, [ARRAY a], [c] ->
    c [INT (Int64.of_int (List.length a))]
  | ALength, [UARRAY a], [c] ->
    c [INT (Int64.of_int (List.length a))]
  | SLength, [BYTEARRAY a], [c] ->
    c [INT (Int64.of_int (List.length a))]
  | SLength, [STRING a], [c] ->
    c [INT (Int64.of_int (String.length a))]
  | PhysEq, [x; y], [t; f] ->
    if eq x y then t [] else f []
  | PhysNEq, [x; y], [t; f] ->
    if not (eq x y) then t [] else f []
  | (Boxed | ALength | SLength | PhysEq | PhysNEq), _, _ ->
    bad_prim tag

let eval_prim_exn tag op values conts =
  let open Common.Primop.Exn in
  match op, values, conts with
  | GetHdlr, [], [c] ->
    (fun s -> c [fetch s handler_ref] s)
  | SetHdlr, [h], [c] ->
    (fun s -> c [] (upd s handler_ref h))
  | (SetHdlr | GetHdlr), _, _ -> 
    bad_prim tag

let eval_prim tag op =
  match op with
  | Primop.Arith op  -> eval_prim_arith tag op
  | Primop.FArith op -> eval_prim_farith tag op
  | Primop.Mem op    -> eval_prim_mem tag op
  | Primop.Repr op   -> eval_prim_repr tag op
  | Primop.Exn op    -> eval_prim_exn tag op

type env = Common.Var.t -> dvalue

let eval_value env v =
  match v with
  | Ast.Int i    -> INT i
  | Ast.Real r   -> REAL r
  | Ast.String s -> STRING s
  | Ast.Var v    -> env v
  | Ast.Label v  -> env v

let bind env x v = fun y -> if Common.Var.equal x y then v else env y
let rec bindn env xs vs =
  match (xs, vs) with
  | ([], [])       -> env
  | (x::xs, v::vs) -> bindn (bind env x v) xs vs
  | _              -> failwith "bindn: lists of different length"

let rec eval_field x p =
  match (x, p) with
  | x, Ast.Offp 0                  -> x
  | RECORD (l, i), Ast.Offp j      -> RECORD (l, i+j)
  | RECORD (l, i), Ast.Selp (j, p) -> eval_field (List.nth l (i+j)) p
  | _ -> failwith "Invalid argument to eval_field"

let rec eval_expr env e =
  match e.Ast.e_kind with
  | Ast.Select (i,v,w,e_cont) ->
    begin match eval_value env v with
    | RECORD (l, j) ->
      let index = i + j in
      if index < 0 || index >= List.length l then begin
        Errors.error ~tag: e.Ast.e_tag
          "Selecting field, which is out of the record (%d)." index;
        raise Errors.Fatal_error
      end;
      eval_expr (bind env w (List.nth l index)) e_cont
    | _ ->
      Errors.error ~tag: e.Ast.e_tag
        "Select argument is not a record";
      raise Errors.Fatal_error
    end
  | Ast.Offset (i,v,w,e_cont) ->
    begin match eval_value env v with
    | RECORD (l, j) ->
      eval_expr (bind env w (RECORD (l, i+j))) e_cont
    | _ ->
      Errors.error ~tag: e.Ast.e_tag
        "Offset argument is not a record";
      raise Errors.Fatal_error
    end
  | Ast.App (f, vl) ->
    begin match eval_value env f with
    | FUNC g ->
      g (List.map (eval_value env) vl)
    | _ ->
      Errors.error ~tag: e.Ast.e_tag
        "Applied value is not a function.";
      raise Errors.Fatal_error
    end
  | Ast.Record (vl, w, e_cont) ->
    let val_list =
      List.map (fun (x,p) -> eval_field (eval_value env x) p) vl
    in
    let record = RECORD (val_list, 0) in
    eval_expr (bind env w record) e_cont
  | Ast.Switch (v, el) ->
    begin match eval_value env v with
    | INT i ->
      let i = Int64.to_int i in
      if i < 0 || i >= List.length el then begin
        Errors.error ~tag: e.Ast.e_tag
          "Switch argument is out of range (%d)." i;
        raise Errors.Fatal_error
      end;
      eval_expr env (List.nth el i)
    | _ ->
      Errors.error ~tag: e.Ast.e_tag
        "Switch argument is not an integer.";
      raise Errors.Fatal_error
    end
  | Ast.Primop (p, vl, wl, el) ->
    eval_prim e.Ast.e_tag
      p
      (List.map (eval_value env) vl)
      (List.map (fun e -> fun al -> eval_expr (bindn env wl al) e) el)
  | Ast.FFI(name, vs, x, e) ->
    let foreign = Common.FFI.by_name name in
    let result =
      foreign.Common.FFI.call ffi_view
        (List.map (eval_value env) vs) in
    eval_expr (bind env x result) e
  | Ast.Fix (fl, e_cont) ->
    let rec h r1 (f, vl, b) =
      FUNC (fun al -> eval_expr (bindn (g r1) vl al) b)
    and g r = bindn r (List.map (fun (x, _, _) -> x) fl) (List.map (h r) fl) in
    eval_expr (g env) e_cont

let print_value v =
  match v with
  | RECORD _    -> print_endline "RECORD"
  | INT n       -> print_endline (Common.SmlConst.sml_string_of_int64 n)
  | REAL r      -> print_endline (Common.Real.to_sml_string r)
  | FUNC _      -> print_endline "FUNC"
  | STRING s    -> print_endline (Common.SmlConst.sml_string_of_string s)
  | BYTEARRAY _ -> print_endline "BYTEARRAY"
  | ARRAY _     -> print_endline "ARRAY"
  | UARRAY _    -> print_endline "UARRAY"

let env0 = 
  let env0 x = failwith "env: no such variable" in
  bind env0 Ast.top_cont (FUNC (fun args store ->
    List.iter print_value args
  ))
let store0 =
  upd
    (1, (fun _ -> failwith "store: not found"),
        (fun _ -> failwith "ustore: not found"))
    0
    (FUNC (fun _ _ -> failwith "Uncaught exception"))

let eval_program expr = eval_expr env0 expr store0

