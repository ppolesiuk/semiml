type var  = Common.Var.t

type accesspath =
| Offp      of int
| Selp      of int * accesspath

type value =
| Var       of var
| Label     of var
| Int       of int64
| Real      of Common.Real.t
| String    of string

type expr =
  { e_tag  : Common.Tag.t
  ; e_kind : expr_kind
  }
and expr_kind =
| Record    of (value * accesspath) list * var * expr
| Select    of int * value * var * expr
| Offset    of int * value * var * expr
| App       of value * value list
| Fix       of (var * var list * expr) list * expr
| Switch    of value * expr list
| FFI       of string * value list * var * expr
| Primop    of Primop.t * value list * var list * expr list

val top_cont : Common.Var.t

val get_continuations : expr -> expr list

val get_args : expr -> value list

val get_results : expr -> var list
