
type ident =
  { id_tag  : Common.Tag.t
  ; id_name : string
  }

type typedecl =
  { tdecl_tag  : Common.Tag.t
  ; tdecl_args : ident list
  ; tdecl_name : ident
  }

type typ =
  { t_tag  : Common.Tag.t
  ; t_kind : typ_kind
  }
and typ_kind =
| T_Var       of string
| T_Con       of typ list * ident
| T_Arrow     of typ * typ
| T_Record    of typ list
| T_TypeDef   of typedef list * typ
| T_Datatype  of datatype_def list * typ
| T_ForallVar of ident list * typ
| T_Forall    of typedecl list * typ
| T_Exists    of typedecl list * typ

and typedef =
  { tdef_tag  : Common.Tag.t
  ; tdef_args : ident list
  ; tdef_name : ident
  ; tdef_body : typ
  }

and datatype_def =
  { dt_args  : ident list
  ; dt_name  : ident
  ; dt_ctors : (ident * typ option) list
  }

type arg =
  { arg_tag  : Common.Tag.t
  ; arg_kind : arg_kind
  }
and arg_kind =
| Arg_Annot   of ident list * typ
| Arg_Var     of ident
| Arg_TypeVar of ident list
| Arg_Type    of typedecl list

type expr =
  { e_tag  : Common.Tag.t
  ; e_kind : expr_kind
  }
and expr_kind =
| Var        of string
| Fn         of arg list * expr
| Let        of ident * expr * expr
| Fix        of fix_def list * expr
| App        of expr * expr
| TypeVarApp of expr * typ
| TypeApp    of expr * typedef list
| Int        of int64
| Real       of Common.Real.t
| String     of string
| Case       of expr * ctor_case list * default_case option
| CaseInt    of expr * int_case list * default_case
| CaseExn    of expr * exn_case list * default_case
| Exn        of expr
| ExnCtor    of expr * typ
| ExnName    of expr
| FFI        of string * int
| Record     of expr list
| Select     of int * expr
| Raise      of expr
| Handle     of expr * expr
| TypeDef    of typedef list * expr
| Datatype   of datatype_def list * expr
| Pack       of typedef list * expr * typ option
| Unpack     of typedecl list * ident * expr * expr
| Annot      of expr * typ

and fix_def =
  { fxd_tag  : Common.Tag.t
  ; fxd_name : ident
  ; fxd_args : arg list
  ; fxd_type : typ
  ; fxd_body : expr
  }

and ctor_case =
  { cc_ctor : ident
  ; cc_arg  : ident option
  ; cc_body : expr
  }

and int_case = int64 * expr

and exn_case =
  { ec_exn  : expr
  ; ec_arg  : ident option
  ; ec_body : expr
  }

and default_case = ident * expr
