
type var = Common.Var.t

type con =
| C_Tagged of int
| C_Transparent
| C_TransU
| C_TransB

type expr =
  { e_tag  : Common.Tag.t
  ; e_kind : expr_kind
  }
and expr_kind =
| Var       of var
| Fn        of var * expr
| Fix       of (var * expr) list * expr
| App       of expr * expr
| Int       of int64
| Real      of Common.Real.t
| String    of string
| Switch    of switch_expr
| Con       of con * expr
| Decon     of con * expr
| SwitchExn of expr * (expr * expr) list * expr
| ConExn    of expr * expr
| ExnValue  of expr
| ExnName   of expr
| FFI       of string * int (* Function name * arity *)
| Record    of expr list
| Select    of int * expr
| Raise     of expr
| Handle    of expr * expr
| Prim      of Primop.t

and switch_expr =
  { sw_expr           : expr
  ; sw_possible_cons  : con list
  ; sw_con_cases      : (con * expr) list
  ; sw_constant_range : int64 option
  ; sw_constant_cases : (int64 * expr) list
  ; sw_default_case   : expr option
  }
