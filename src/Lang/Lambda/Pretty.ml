open Common.SmlConst
open Printing

module Env = Printing.Scope.Make(Common.Var)

let prec_stmt   = 0
let prec_raise  = 1
let prec_handle = 2
let prec_app    = 3
let prec_atom   = 4

module PrimopPrinter(Op : Common.Primop.Group) : sig
  val pretty : Op.t -> Box.t
end = struct
  let pretty op =
    if Op.is_operator op then
      Box.paren (Box.oper (Op.name op))
    else
      Box.text ~attributes:[Box.Constant] (Op.name op)
end

module PrimArith   = PrimopPrinter(Common.Primop.Arith)
module PrimFArith  = PrimopPrinter(Common.Primop.FArith)
module PrimMem     = PrimopPrinter(Common.Primop.Mem)
module PrimRepr    = PrimopPrinter(Common.Primop.Repr)
module PrimControl = PrimopPrinter(Common.Primop.Control)

let pretty_prim prim =
  match prim with
  | Primop.Arith   op -> PrimArith.pretty op
  | Primop.FArith  op -> PrimFArith.pretty op
  | Primop.Mem     op -> PrimMem.pretty op
  | Primop.Repr    op -> PrimRepr.pretty op
  | Primop.Control op -> PrimControl.pretty op

let pretty_con con =
  match con with
  | Ast.C_Tagged n    -> 
    Box.text ~attributes:[Box.Constructor] ("." ^ string_of_int n)
  | Ast.C_Transparent -> Box.text ~attributes:[Box.Constructor] ".T"
  | Ast.C_TransU      -> Box.text ~attributes:[Box.Constructor] ".U"
  | Ast.C_TransB      -> Box.text ~attributes:[Box.Constructor] ".B"

let rec pretty_expr env prec expr =
  match expr.Ast.e_kind with
  | Ast.Var x -> Env.pretty env x
  | Ast.Fn _ -> 
    let (boxes, body) = pretty_fn "=>" env expr in
    Box.prec_paren prec_stmt prec (Box.box
      [ Box.box (Box.keyword "fn" :: boxes)
      ; body
      ])
  | Ast.Fix([], e) -> pretty_expr env prec e
  | Ast.Fix(def :: defs, e) ->
    let env = build_fix_env env (def :: defs) in
    let (def_boxes, def_box) = pretty_fn "=" env (snd def) in
    let def = Box.box
      [ Box.box 
        (  Box.keyword "letrec"
        :: Box.white_sep (Env.pretty env (fst def))
        :: def_boxes)
      ; def_box
      ]
    in
    let defs = def :: (pretty_fix env defs) in
    Box.prec_paren prec_stmt prec (Box.box
      [ Box.box defs
      ; Box.white_sep (pretty_expr env 0 e)
      ])
  | Ast.App({ Ast.e_kind = Ast.Fn(x, e2) }, e1) ->
    let (env', x) = Env.extend env x in
    Box.prec_paren prec_stmt prec (Box.box
    [ Box.box
      [ Box.box
        [ Box.box
          [ Box.keyword "let"
          ; Box.white_sep x
          ; Box.white_sep (Box.oper "=")
          ]
        ; Box.indent 2 (Box.white_sep (pretty_expr env 0 e1))
        ]
      ; Box.white_sep (Box.keyword "in")
      ]
    ; Box.white_sep (pretty_expr env' prec_stmt e2)
    ])
  | Ast.App(e1, e2) ->
    Box.prec_paren prec_app prec (Box.box
      [ pretty_expr env prec_app e1
      ; Box.indent 2 (Box.white_sep (pretty_expr env (prec_app+1) e2))
      ])
  | Ast.Int n -> Box.text ~attributes:[Box.Number] (sml_string_of_int64 n)
  | Ast.Real r -> 
      Box.text ~attributes:[Box.Number] (Common.Real.to_sml_string r)
  | Ast.String s -> 
      Box.text ~attributes:[Box.Literal] (sml_string_of_string s)
  | Ast.Switch sw ->
    Box.box
    ( Box.box 
      [ Box.keyword "switch"
      ; Box.indent 2 (Box.white_sep (pretty_expr env 0 sw.Ast.sw_expr))
      ]
    :: pretty_switch_data_block env sw
    @  pretty_switch_int_block env sw
    @  pretty_switch_default_case env sw
    @  [ Box.white_sep (Box.keyword "end") ])
  | Ast.Con(con, e) ->
    Box.prec_paren prec_app prec (Box.box
      [ pretty_con con
      ; Box.indent 2 (Box.white_sep (pretty_expr env (prec_app+1) e))
      ])
  | Ast.Decon(con, e) ->
    Box.prec_paren prec_app prec (Box.box
      [ Box.box [ Box.keyword "decon"; Box.white_sep (pretty_con con) ]
      ; Box.indent 2 (Box.white_sep (pretty_expr env (prec_app+1) e))
      ])
  | Ast.SwitchExn(e, cases, defc) ->
    Box.box
    ( Box.box
      [ Box.keyword "match"
      ; Box.indent 2 (Box.white_sep (pretty_expr env 0 e))
      ; Box.white_sep (Box.keyword "with")
      ]
    :: List.map (pretty_exn_case env) cases
    @  pretty_default_case env defc
    @  [ Box.white_sep (Box.keyword "end") ])
  | Ast.ConExn(e1, e2) ->
    Box.box
    [ Box.box
      [ Box.box
        [ Box.keyword "exn"
        ; Box.text ~attributes: [Box.Paren] "("
        ]
      ; Box.indent 2 (Box.suffix (pretty_expr env 0 e1) (Box.oper ","))
      ]
    ; Box.indent 2 (Box.suffix (pretty_expr env 0 e2)
        (Box.text ~attributes: [Box.Paren] ")"))
    ]
  | Ast.ExnValue e ->
    Box.prec_paren prec_app prec (Box.box
      [ Box.keyword "exnvalue"
      ; Box.indent 2 (Box.white_sep (pretty_expr env (prec_app+1) e))
      ])
  | Ast.ExnName e ->
    Box.prec_paren prec_app prec (Box.box
      [ Box.keyword "exnname"
      ; Box.indent 2 (Box.white_sep (pretty_expr env (prec_app+1) e))
      ])
  | Ast.FFI (name, arity) ->
    Box.box
      [ Box.keyword "FFI"
      ; Box.oper "#"
      ; Box.text name
      ; Box.oper ":"
      ; Box.text (string_of_int arity)
      ]
  | Ast.Record [] -> Box.text ~attributes:[Box.Paren] "{}"
  | Ast.Record(e :: es) ->
    Box.box
    (  Box.prefix (Box.text ~attributes: [Box.Paren] "{")
        (pretty_expr env prec_stmt e)
    :: pretty_record_aux env es)
  | Ast.Select(n, e) ->
    Box.prec_paren prec_app prec (Box.box
      [ Box.oper ("#" ^ string_of_int n)
      ; Box.indent 2 (Box.white_sep (pretty_expr env (prec_app+1) e))
      ])
  | Ast.Raise e ->
    Box.prec_paren prec_raise prec (Box.box
      [ Box.keyword "raise"
      ; Box.indent 2 (Box.white_sep (pretty_expr env (prec_raise+1) e))
      ])
  | Ast.Handle(e1, e2) ->
    Box.prec_paren prec_handle prec (Box.box
      [ pretty_expr env prec_handle e1
      ; Box.white_sep (Box.keyword "handle")
      ; Box.indent 2 (Box.white_sep (pretty_expr env (prec_handle+1) e2))
      ])
  | Ast.Prim prim -> pretty_prim prim

and build_fix_env env defs =
  List.fold_left (fun env (x, _) ->
    fst (Env.extend env x)
  ) env defs

and pretty_fix env defs =
  match defs with
  | [] -> [ Box.white_sep (Box.keyword "in") ]
  | def :: defs ->
    let (def_boxes, def_box) = pretty_fn "=" env (snd def) in
    let def = Box.box
      [ Box.box 
        (  Box.white_sep (Box.keyword "and")
        :: Box.white_sep (Env.pretty env (fst def))
        :: def_boxes)
      ; def_box
      ]
    in
    def :: (pretty_fix env defs)

and pretty_fn op env expr =
  match expr.Ast.e_kind with
  | Ast.Fn(x, body) ->
    let (env, x) = Env.extend env x in
    let (boxes, body) = pretty_fn op env body in
    (Box.indent 2 (Box.white_sep x) :: boxes, body)
  | _ ->
    ( [Box.white_sep (Box.oper op)]
    , Box.indent 2 (Box.white_sep (pretty_expr env prec_stmt expr)))

and pretty_switch_con_case env (con, body) =
  Box.box
  [ Box.box
    [ Box.white_sep (Box.oper "|")
    ; Box.white_sep (pretty_con con)
    ; Box.white_sep (Box.oper "=>")
    ]
  ; Box.indent 2 (Box.white_sep (pretty_expr env 0 body))
  ]

and pretty_switch_data_block env sw =
  match sw.Ast.sw_possible_cons with
  | [] ->
    if sw.Ast.sw_con_cases <> [] then
      Box.box 
      [ Box.white_sep (Box.text ~attributes:[Box.Error] "data")
      ; Box.indent 2 (Box.text ~attributes:[Box.Error] "[]")
      ] :: List.map (pretty_switch_con_case env) sw.Ast.sw_con_cases
    else []
  | con :: cs ->
    Box.box
    [ Box.white_sep (Box.keyword "data")
    ; Box.indent 2 (Box.box (
      [ Box.text ~attributes:[Box.Paren] "["
      ; pretty_con con
      ] 
      @ List.map pretty_con cs
      @ [Box.text ~attributes:[Box.Paren] "]"]))
    ] :: List.map (pretty_switch_con_case env) sw.Ast.sw_con_cases

and pretty_switch_constant_case env (n, body) =
  Box.box
  [ Box.box
    [ Box.white_sep (Box.oper "|")
    ; Box.white_sep (Box.text ~attributes:[Box.Number] (sml_string_of_int64 n))
    ; Box.white_sep (Box.oper "=>")
    ]
  ; Box.indent 2 (Box.white_sep (pretty_expr env 0 body))
  ]

and pretty_switch_int_block env sw =
  match sw.Ast.sw_constant_range with
  | None ->
    (Box.white_sep (Box.keyword "int"))
    :: List.map (pretty_switch_constant_case env) sw.Ast.sw_constant_cases
  | Some 0L when sw.Ast.sw_constant_cases <> [] ->
    Box.box
    [ Box.white_sep (Box.text ~attributes:[Box.Error] "int")
    ; Box.white_sep (Box.text ~attributes:[Box.Error] "0")
    ] :: List.map (pretty_switch_constant_case env) sw.Ast.sw_constant_cases
  | Some n ->
    Box.box
    [ Box.white_sep (Box.keyword "int")
    ; Box.indent 2 (Box.paren ~opn:"[" ~cls:"]"
        (Box.text ~attributes:[Box.Number] (sml_string_of_int64 n)))
    ] :: List.map (pretty_switch_constant_case env) sw.Ast.sw_constant_cases

and pretty_switch_default_case env sw =
  match sw.Ast.sw_default_case with
  | None -> []
  | Some e -> pretty_default_case env e

and pretty_default_case env e =
  [ Box.box
    [ Box.white_sep (Box.keyword "default")
    ; Box.white_sep (Box.oper "=>")
    ]
  ; Box.indent 2 (Box.white_sep (pretty_expr env 0 e))
  ]

and pretty_exn_case env (e1, e2) =
  Box.white_sep (Box.prefix (Box.oper "| ") (Box.box
    [ Box.box
      [ pretty_expr env 0 e1
      ; Box.white_sep (Box.oper "=>")
      ]
    ; Box.indent 2 (Box.white_sep (pretty_expr env 0 e2))
    ]))

and pretty_record_aux env es =
  match es with
  | [] -> [ Box.text ~attributes: [Box.Paren] "}" ]
  | e :: es ->
    Box.prefix (Box.oper ",") (pretty_expr env prec_stmt e)
    :: pretty_record_aux env es

let pretty_program expr =
  pretty_expr (Env.create Common.Var.name) 0 expr
