
type var = Common.Var.t
type tag = Common.Tag.t

type expr =
  { e_tag  : tag
  ; e_kind : expr_kind
  }
and expr_kind =
| Succ
| Var  of var
| Num  of int
| Abs  of var * expr
| App  of expr * expr
| Case of expr * case_fun * case_zero * case_succ

and case_fun  = var * expr
and case_zero = expr
and case_succ = var * expr
