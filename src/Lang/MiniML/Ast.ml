
type var  = Common.Var.t
type tvar = Type.tvar

type expr =
  { e_tag  : Common.Tag.t
  ; e_kind : expr_kind
  }
and expr_kind =
| Var        of var
| Fn         of var * Type.t * expr
| Fix        of fix_def list * expr
| App        of expr * expr
| Int        of int64
| Real       of Common.Real.t
| String     of string
| Case       of case_expr
| Con        of Type.cname * Type.t list * expr option
| CaseInt    of expr * (int64 * expr) list * default_case
| CaseExn    of expr * (expr * var option * expr) list * default_case
| Exn        of expr
| ExnCtor    of expr * Type.t
| ConExn     of expr * expr
| ExnName    of expr
| FFI        of string * int
| Record     of expr list
| Select     of int * expr
| Raise      of expr * Type.t
| Handle     of expr * expr
| Prim       of Primop.t
| TypeDef    of tvar list * Type.tcon * Type.t * expr
| DataType   of Type.datatype_def list * expr
| TypeVarFn  of tvar * expr
| TypeVarApp of expr * Type.t
| TypeFn     of int * Type.tcon * expr
| TypeApp    of expr * tvar list * Type.t
| Pack       of tvar list * Type.tcon * Type.t * expr * Type.t
| Unpack     of int * Type.tcon * var * expr * expr

and fix_def =
  { fxd_var  : var
  ; fxd_type : Type.t
  ; fxd_body : expr
  }

and case_expr =
  { ce_expr          : expr
  ; ce_cases         : (Type.cname * var option * expr) list
  ; ce_default_case  : default_case option
  }

and default_case = var * expr

module Expr = struct
  type t = expr

  let rec is_function e =
    match e.e_kind with
    | Fn _ -> true
    | Var _ | Fix _ | App _ | Int _ | Real _ | String _ | Case _ | Con _ 
    | CaseInt _ | CaseExn _ | Exn _ | ExnCtor _ | ConExn _ | ExnName _ 
    | Record _ | Select _ | Raise _ | FFI _ | Handle _ | Prim _ | Unpack _ -> false
    | TypeDef(_, _, _, e) | DataType(_, e)
    | TypeVarFn(_, e) | TypeVarApp(e, _) 
    | TypeFn(_, _, e) | TypeApp(e, _, _) | Pack(_, _, _, e, _) -> is_function e

  let rec is_value e =
    match e.e_kind with
    | Var _ | Fn _ | Int _ | Real _ | String _ | Con(_, _, None) 
    | Prim _ | FFI _ -> true
    | Fix _ | App _ | Case _ | CaseInt _ | CaseExn _ | Exn _ | ExnCtor _
    | ConExn _ | ExnName _ | Select _ | Raise _ | Handle _ | Unpack _ -> false
    | Record es -> List.for_all is_value es
    | Con(_, _, Some e)
    | TypeDef(_, _, _, e) | DataType(_, e)
    | TypeVarFn(_, e) | TypeVarApp(e, _) 
    | TypeFn(_, _, e) | TypeApp(e, _, _) | Pack(_, _, _, e, _) -> is_value e
end
