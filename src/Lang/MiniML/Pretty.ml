module Box = Printing.Box

let rec pretty_type scope prec tp =
  match tp with
  | Type.Var x ->
    let (tvar_scope, _) = scope in
    Type.TVarScope.pretty tvar_scope x
  | Type.TyCon([], x) ->
    let (_, tcon_scope) = scope in
    Type.TConScope.pretty tcon_scope x
  | Type.TyCon([tp], x) ->
    let (_, tcon_scope) = scope in
    pretty_type_tcon scope tp
      [Box.indent 2 (Box.white_sep (Type.TConScope.pretty tcon_scope x))]
  | Type.TyCon(tp :: tps, x) ->
    let (_, tcon_scope) = scope in
    Box.box
    [ pretty_type_list scope tp tps
    ; Box.indent 2 (Box.white_sep (Type.TConScope.pretty tcon_scope x))
    ]
  | Type.Arrow(tp1, tp2) ->
    Box.prec_paren 0 prec (Box.box
      [ Box.suffix (pretty_type scope 1 tp1)
          (Box.white_sep (Box.oper "->"))
      ; Box.white_sep (pretty_type scope 0 tp2)
      ])
  | Type.Record [] ->
    Box.text ~attributes:[Box.Paren] "{}"
  | Type.Record(tp :: tps) -> pretty_type_record scope tp tps
  | Type.TypeDef(args, x, body, tp) ->
    let (scope, def) = pretty_typedef (Box.keyword "type") scope args x body in
    let (defs, tp) = pretty_type_typedef scope tp in
    Box.prec_paren 0 prec (Box.box
      [ Box.box (def :: defs)
      ; Box.white_sep tp
      ])
  | Type.DataType(defs, tp) ->
    let scope = build_datatype_scope scope defs in
    let kw = Box.keyword "datatype" in
    let (defs, tp) = pretty_datatypes kw scope defs tp in
    Box.prec_paren 0 prec (Box.box
      [ Box.box defs
      ; Box.white_sep tp
      ])
  | Type.ForallVar(x, tp) ->
    let (tvar_scope, _) = scope in
    let (tvar_scope, x) = Type.TVarScope.extend tvar_scope x in
    let scope = (tvar_scope, snd scope) in
    let (vars, body) = pretty_type_forall_var scope x tp in
    Box.prec_paren 0 prec (Box.box
      [ Box.keyword "forall"
      ; Box.indent 2 (Box.box vars)
      ; Box.indent 2 (Box.white_sep body)
      ])
  | Type.Forall(n, x, tp) ->
    let kw = 
      Box.box [ Box.keyword "forall"; Box.white_sep (Box.keyword "type") ] in
    let (scope, decl) = pretty_typedecl kw scope n x in
    let (decls, tp) = pretty_type_forall scope tp in
    Box.prec_paren 0 prec (Box.box
      [ Box.box (decl :: decls)
      ; Box.white_sep tp
      ])
  | Type.Exists(n, x, tp) ->
    let kw = 
      Box.box [ Box.keyword "exists"; Box.white_sep (Box.keyword "type") ] in
    let (scope, decl) = pretty_typedecl kw scope n x in
    let (decls, tp) = pretty_type_exists scope tp in
    Box.prec_paren 0 prec (Box.box
      [ Box.box (decl :: decls)
      ; Box.white_sep tp
      ])

and pretty_type_tcon scope tp acc =
  match tp with
  | Type.TyCon([tp], x) ->
    let (_, tcon_scope) = scope in
    pretty_type_tcon scope tp
      (Box.indent 2 (Box.white_sep 
        (Type.TConScope.pretty tcon_scope x)) :: acc)
  | _ ->
    Box.box
    [ pretty_type scope 2 tp
    ; Box.box acc
    ]

and pretty_type_record_aux scope tps =
  match tps with
  | [] -> [ Box.text ~attributes: [Box.Paren] "}" ]
  | tp :: tps ->
    Box.prefix (Box.oper ",") (pretty_type scope 0 tp)
    :: pretty_type_record_aux scope tps

and pretty_type_record scope tp tps =
  Box.box
    (  Box.prefix (Box.text ~attributes: [Box.Paren] "{")
        (pretty_type scope 0 tp)
    :: pretty_type_record_aux scope tps)

and pretty_type_typedef scope tp =
  match tp with
  | Type.TypeDef(args, x, body, tp) ->
    let (scope, def) = 
      pretty_typedef (Box.white_sep (Box.keyword "and")) scope args x body in
    let (defs, tp) = pretty_type_typedef scope tp in
    (def :: defs, tp)
  | _ ->
    ( [Box.white_sep (Box.keyword "in")]
    , pretty_type scope 0 tp
    )

and pretty_type_list_aux scope tps =
  match tps with
  | [] -> [ Box.text ~attributes: [Box.Paren] ")" ]
  | [tp] -> [ Box.prefix (Box.oper ",")
      (Box.suffix (pretty_type scope 0 tp)
        (Box.text ~attributes: [Box.Paren] ")")) ]
  | tp :: tps ->
    Box.prefix (Box.oper ",") (pretty_type scope 0 tp)
    :: pretty_type_list_aux scope tps

and pretty_type_list scope tp tps =
  Box.box
    (  Box.prefix (Box.text ~attributes: [Box.Paren] "(")
        (pretty_type scope 0 tp)
    :: pretty_type_list_aux scope tps)

and pretty_type_forall_var scope x tp =
  let (tvar_scope, _) = scope in
  match tp with
  | Type.ForallVar(y, body) ->
    let (tvar_scope, y) = Type.TVarScope.extend tvar_scope y in
    let scope = (tvar_scope, snd scope) in
    let (vars, body) = pretty_type_forall_var scope y body in
    (Box.white_sep x :: vars, body)
  | _ ->
    ( Box.white_sep (Box.suffix x (Box.oper ",")) :: []
    , pretty_type scope 0 tp)

and pretty_type_forall scope tp =
  match tp with
  | Type.Forall(n, x, tp) ->
    let (scope, decl) = 
      pretty_typedecl (Box.white_sep (Box.keyword "and")) scope n x in
    let (decls, tp) = pretty_type_forall scope tp in
    (decl :: decls, tp)
  | _ ->
    ( [Box.oper ","]
    , pretty_type scope 0 tp
    )

and pretty_type_exists scope tp =
  match tp with
  | Type.Exists(n, x, tp) ->
    let (scope, decl) = 
      pretty_typedecl (Box.white_sep (Box.keyword "and")) scope n x in
    let (decls, tp) = pretty_type_exists scope tp in
    (decl :: decls, tp)
  | _ ->
    ( [Box.oper ","]
    , pretty_type scope 0 tp
    )

and pretty_type_formal_args_aux scope args =
  match args with
  | [] -> (scope, [ Box.text ~attributes: [Box.Paren] ")" ])
  | [arg] ->
    let (tvar_scope, _) = scope in
    let (tvar_scope, x) = Type.TVarScope.extend tvar_scope arg in
    let scope = (tvar_scope, snd scope) in
    ( scope
    , [ Box.box [ Box.oper ","; x; Box.text ~attributes: [Box.Paren] ")" ] ])
  | arg :: args ->
    let (tvar_scope, _) = scope in
    let (tvar_scope, x) = Type.TVarScope.extend tvar_scope arg in
    let scope = (tvar_scope, snd scope) in
    let (scope, xs) = pretty_type_formal_args_aux scope args in
    (scope, Box.box [ Box.oper ","; x ] :: xs)

and pretty_type_formal_args scope arg args =
  let (tvar_scope, _) = scope in
  let (tvar_scope, x) = Type.TVarScope.extend tvar_scope arg in
  let scope = (tvar_scope, snd scope) in
  match args with
  | [] -> (scope, x)
  | _ ->
    let (scope, xs) = pretty_type_formal_args_aux scope args in
    ( scope
    , Box.box (Box.box [ Box.text ~attributes: [Box.Paren] "("; x ] :: xs))

and pretty_typedef kw scope args x body =
  let (_, tcon_scope) = scope in
  let (r_tcon_scope, x) = Type.TConScope.extend tcon_scope x in
  let rscope = (fst scope, r_tcon_scope) in
  let result =
    match args with
    | [] ->
      Box.box
      [ Box.box
        [ kw
        ; Box.white_sep x
        ; Box.white_sep (Box.oper "=")
        ]
      ; Box.indent 2 (Box.white_sep (pretty_type scope 0 body))
      ]
    | arg :: args ->
      let (body_scope, args) = pretty_type_formal_args scope arg args in
      Box.box
      [ Box.box
        [ kw
        ; Box.white_sep args
        ; Box.white_sep x
        ; Box.white_sep (Box.oper "=")
        ]
      ; Box.indent 2 (Box.white_sep (pretty_type body_scope 0 body))
      ]
  in (rscope, result)

and build_datatype_scope scope defs =
  match defs with
  | [] -> scope
  | def :: defs ->
    let (_, tcon_scope) = scope in
    let (tcon_scope, _) = 
      Type.TConScope.extend tcon_scope def.Type.dt_name in
    build_datatype_scope (fst scope, tcon_scope) defs

and pretty_datatypes kw scope defs tp =
  match defs with
  | [] -> ([Box.white_sep (Box.keyword "in")], pretty_type scope 0 tp)
  | def :: defs ->
    let def = pretty_datatype kw scope def in
    let kw = Box.white_sep (Box.keyword "and") in
    let (defs, tp) = pretty_datatypes kw scope defs tp in
    (def :: defs, tp)

and pretty_datatype kw scope def =
  let x = Type.TConScope.pretty (snd scope) def.Type.dt_name in
  match def.Type.dt_args with
  | [] ->
    Box.box
    ( Box.box
      [ kw
      ; Box.white_sep x
      ; Box.white_sep (Box.oper "=")
      ]
    :: List.map (pretty_datatype_ctor scope) def.Type.dt_ctors)
  | arg :: args ->
    let (scope, args) = pretty_type_formal_args scope arg args in
    Box.box
    ( Box.box
      [ kw
      ; Box.white_sep args
      ; Box.white_sep x
      ; Box.white_sep (Box.oper "=")
      ]
    :: List.map (pretty_datatype_ctor scope) def.Type.dt_ctors)

and pretty_datatype_ctor scope (Type.CName x, arg) =
  let x = Box.text (Common.Var.name x) in
  match arg with
  | None ->
    Box.box
    [ Box.white_sep (Box.oper "|")
    ; Box.white_sep x
    ]
  | Some tp ->
    Box.box
    [ Box.box
      [ Box.white_sep (Box.oper "|")
      ; Box.white_sep x
      ; Box.white_sep (Box.keyword "of")
      ]
    ; Box.indent 4 (Box.white_sep (pretty_type scope 0 tp))
    ]

and pretty_typedecl_args_aux i n =
  let name =
    Box.text ~attributes:[Box.Variable]
      (if i < 25 then Printf.sprintf "'%c" (Char.chr (Char.code 'a' + i))
       else Printf.sprintf "'a%d" (i-25))
  in
  if i >= n - 1 then
    [ Box.box
      [ Box.oper ","; name; Box.text ~attributes:[Box.Paren] ")" ]
    ]
  else
    (Box.box [ Box.oper ","; name ] :: pretty_typedecl_args_aux (i+1) n)

and pretty_typedecl_args n =
  Box.box
  (  Box.box 
     [ Box.text ~attributes:[Box.Paren] "("
     ; Box.text ~attributes:[Box.Variable] "'a"
     ]
  :: pretty_typedecl_args_aux 1 n)

and pretty_typedecl kw scope n x =
  let (_, tcon_scope) = scope in
  let (tcon_scope, x) = Type.TConScope.extend tcon_scope x in
  let scope = (fst scope, tcon_scope) in
  let result =
    match n with
    | 0 -> Box.box [ kw; Box.white_sep x ]
    | 1 -> Box.box 
      [ kw
      ; Box.white_sep (Box.text ~attributes:[Box.Variable] "'a")
      ; Box.white_sep x 
      ]
    | _ -> Box.box
      [ kw
      ; Box.white_sep (pretty_typedecl_args n)
      ; Box.white_sep x
      ]
  in (scope, result)
