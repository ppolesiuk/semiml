open Language

let well_typed = Contract.create
  ~description: "The program is well-typed"
  ~languages: [MiniML]
  "contract:well_typed"

let unique_vars  = Common.Contracts.unique_vars
let unique_tags  = Common.Contracts.unique_tags
