
type t =
| Arith   of Common.Primop.Arith.t
| FArith  of Common.Primop.FArith.t
| Mem     of Common.Primop.Mem.t
| Repr    of Common.Primop.Repr.t
| Control of Common.Primop.Control.t

type primop = t
module type Group = sig
  include Common.Primop.Group
  val constr : t -> primop
end

let all_groups : (module Group) list =
  [ (module struct
      include Common.Primop.Arith
      let constr op = Arith op
    end)
  ; (module struct
      include Common.Primop.FArith
      let constr op = FArith op
    end)
  ; (module struct
      include Common.Primop.Mem
      let constr op = Mem op
    end)
  ; (module struct
      include Common.Primop.Repr
      let constr op = Repr op
    end)
  ; (module struct
      include Common.Primop.Control
      let constr op = Control op
    end)
  ]

let name op =
  match op with
  | Arith   op -> Common.Primop.Arith.name   op
  | FArith  op -> Common.Primop.FArith.name  op
  | Mem     op -> Common.Primop.Mem.name     op
  | Repr    op -> Common.Primop.Repr.name    op
  | Control op -> Common.Primop.Control.name op

let rec tr_type args tp =
  match tp with
  | Common.Primop.TUnit     -> Type.TyCon([], Type.t_unit)
  | Common.Primop.TBool     -> Type.TyCon([], Type.t_bool)
  | Common.Primop.TInt      -> Type.TyCon([], Type.t_int)
  | Common.Primop.TReal     -> Type.TyCon([], Type.t_real)
  | Common.Primop.TString   -> Type.TyCon([], Type.t_string)
  | Common.Primop.TExn      -> Type.TyCon([], Type.t_exn)
  | Common.Primop.TVar    i -> Type.Var args.(i)
  | Common.Primop.TRef   tp -> Type.TyCon([tr_type args tp], Type.t_ref)
  | Common.Primop.TCont  tp -> Type.TyCon([tr_type args tp], Type.t_cont)
  | Common.Primop.TArray tp -> Type.TyCon([tr_type args tp], Type.t_array)
  | Common.Primop.TArrow(tp1, tp2) ->
    Type.Arrow(tr_type args tp1, tr_type args tp2)

let tr_scheme (n, tps, tp) =
  let args = Array.init n (fun _ -> Type.TVar.create ()) in
  let base_type =
    match tps with
    | []    -> tr_type args tp
    | [atp] -> Type.Arrow(tr_type args atp, tr_type args tp)
    | tps   ->
      Type.Arrow(Type.Record(List.map (tr_type args) tps), tr_type args tp)
  in
  List.fold_right (fun x tp ->
    Type.ForallVar(x, tp)
  ) (Array.to_list args) base_type

let types =
  List.fold_right (fun (op_module : (module Group)) types ->
    let module Op = (val op_module) in
    List.map (fun op ->
      (Op.constr op, tr_scheme (Op.type_of op))
    ) Op.all_values @ types
  ) all_groups []
