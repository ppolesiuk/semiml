type var  = Common.Var.t

type accesspath =
| Offp      of int
| Selp      of int * accesspath

type reg

type regf

type value =
| Reg       of reg
| Label     of var
| Immediate of int64

type valuef =
| Regf      of regf

type iprimop =
| Ashl | Ashr | Orb | Andb | Xorb
| Add  | Addt | Sub | Subt
| Divt | Modt | Mult

type iprimopu =
| Neg | Notb

type fprimop =
| Mulf | Divf | Addf | Subf

type icond =
| Eq | Neq | Lt | Le | Gt | Ge | Ltu | Leu | Gtu | Geu

type fcond =
| FEq | FNeq | FLt | FLe | FGt | FGe

type instr =
  { i_tag  : Common.Tag.t
  ; i_kind : instr_kind
  }
and instr_kind =
| Align
| Mark
| Halt
| Unreachable
| Mov          of value * reg
| EmitLong     of int64
| DefineLabel  of var
| EmitLabel    of var * int
| EmitString   of string
| RealConstant of Common.Real.t
| CheckLimit   of int
| BeginStdFn
| Jmp          of value
| Record       of (value * accesspath) list * reg
| Select       of int * value * reg
| Offset       of int * value * reg
| FetchIndexB  of value * reg * value
| StoreIndexB  of value * reg * value
| FetchIndexL  of value * reg * value
| StoreIndexL  of value * reg * value
| BBS          of value * value
| IPrimop      of iprimop * value * value * reg
| IPrimopU     of iprimopu * value * reg
| IBranch      of icond * value * value * value
| FPrimop      of fprimop * valuef * valuef * regf
| FBranch      of fcond * valuef * valuef * value
| LoadFloat    of reg * regf
| StoreFloat   of reg * regf

type prog = instr list

val reg_by_id : int -> reg

val regf_by_id : int -> regf

val string_of_reg : reg -> string

val string_of_regf : regf -> string

val string_of_iprimop : iprimop -> string

val string_of_iprimopu : iprimopu -> string

val string_of_icond : icond -> string

val string_of_fprimop : fprimop -> string

val string_of_fcond : fcond -> string
