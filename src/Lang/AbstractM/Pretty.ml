
open Common.SmlConst
open Printing

module Env = Printing.Scope.Make(Common.Var)

let sep box = Box.indent 2 (Box.white_sep box)
let par box = Box.paren box

let rec commasplice boxes =
  match boxes with
  | [] -> []
  | [box] -> [box]
  | (box :: boxes') -> box :: Box.oper ", " :: commasplice boxes'

let pretty_reg r =
  Box.text ~attributes: [Box.Constant] (Ast.string_of_reg r)

let pretty_regf r =
  Box.text ~attributes: [Box.Constant] (Ast.string_of_regf r)

let pretty_value env v =
  match v with
  | Ast.Reg r       -> pretty_reg r
  | Ast.Label v     -> Env.pretty env v
  | Ast.Immediate n -> Box.int64 n

let pretty_valuef env v =
  match v with
  | Ast.Regf r      -> pretty_regf r

let pretty_iprimop op = Box.oper (Ast.string_of_iprimop op)

let pretty_iprimopu op = Box.oper (Ast.string_of_iprimopu op)

let pretty_icond cond = Box.oper (Ast.string_of_icond cond)

let pretty_fprimop op = Box.oper (Ast.string_of_fprimop op)

let pretty_fcond cond = Box.oper (Ast.string_of_fcond cond)

let rec pretty_accesspath ap =
  match ap with
  | Ast.Offp n ->
    Box.prefix (Box.oper "->") (Box.int n)
  | Ast.Selp (n, ap') ->
    Box.box
    [ Box.oper "->"
    ; Box.int n
    ; pretty_accesspath ap'
    ]

let rec pretty_instr env instr =
  begin match instr.Ast.i_kind with
  | Ast.Align       -> Box.keyword "Align"
  | Ast.Mark        -> Box.keyword "Mark"
  | Ast.Halt        -> Box.keyword "Halt"
  | Ast.Unreachable -> Box.keyword "Unreachable"
  | Ast.Mov (v, r) ->
    Box.box
      [ pretty_reg r
      ; sep (Box.oper "<-")
      ; sep (pretty_value env v)
      ]
  | Ast.EmitLong n ->
    Box.box
      [ Box.keyword "EmitLong"
      ; sep (Box.int64 n)
      ]
  | Ast.DefineLabel v ->
    Box.box
      [ Box.keyword "DefineLabel"
      ; sep (Env.pretty env v)
      ]
  | Ast.EmitLabel (v, n) ->
    Box.box
      [ Box.keyword "EmitLabel"
      ; sep (Env.pretty env v)
      ; sep (Box.int n)
      ]
  | Ast.EmitString s ->
    Box.box
      [ Box.keyword "EmitString"
      ; sep (Box.text (sml_string_of_string s))
      ]
  | Ast.RealConstant x ->
    Box.box
      [ Box.keyword "RealConstant"
      (* it would be nice to have this available in module Box *)
      ; sep (Box.text ~attributes: [Box.Number]
        (Common.Real.to_sml_string x))
      ]
  | Ast.CheckLimit n ->
    Box.box
      [ Box.keyword "CheckLimit"
      ; sep (Box.int n)
      ]
  | Ast.BeginStdFn -> Box.keyword "BeginStdFn"
  | Ast.Jmp v ->
    Box.box
      [ Box.keyword "Jmp"
      ; sep (pretty_value env v)
      ]
  | Ast.Record (aplist, r) ->
    Box.box
      [ Box.keyword "Record"
      ; List.map (fun (v,ap) ->
          Box.prefix (pretty_value env v) (pretty_accesspath ap)
        ) aplist |> commasplice |> Box.box |> par |> sep
      ; sep (pretty_reg r)
      ]
  | Ast.Select (n, v, r) ->
    Box.box
      [ Box.keyword "Select"
      ; sep (Box.prefix (Box.oper "#") (Box.int n))
      ; sep (pretty_value env v)
      ; sep (pretty_reg r)
      ]
  | Ast.Offset (n, v, r) ->
    Box.box
      [ Box.keyword "Offset"
      ; sep (Box.prefix (Box.oper "#") (Box.int n))
      ; sep (pretty_value env v)
      ; sep (pretty_reg r)
      ]
  | Ast.FetchIndexB (vb, r, voff) ->
    Box.box
      [ Box.keyword "FetchIndexB"
      ; sep (pretty_value env vb)
      ; sep (pretty_reg r)
      ; sep (pretty_value env voff)
      ]
  | Ast.StoreIndexB (vb, r, voff) ->
    Box.box
      [ Box.keyword "StoreIndexB"
      ; sep (pretty_value env vb)
      ; sep (pretty_reg r)
      ; sep (pretty_value env voff)
      ]
  | Ast.FetchIndexL (vb, r, voff) ->
    Box.box
      [ Box.keyword "FetchIndexL"
      ; sep (pretty_value env vb)
      ; sep (pretty_reg r)
      ; sep (pretty_value env voff)
      ]
  | Ast.StoreIndexL (vb, r, voff) ->
    Box.box
      [ Box.keyword "StoreIndexL"
      ; sep (pretty_value env vb)
      ; sep (pretty_reg r)
      ; sep (pretty_value env voff)
      ]
  | Ast.BBS (v, vdest) ->
    Box.box
      [ Box.oper "BBS"
      ; sep (pretty_value env v)
      ; sep (pretty_value env vdest)
      ]
  | Ast.IPrimop (op, v1, v2, r) ->
    Box.box
      [ pretty_iprimop op
      ; sep (pretty_value env v1)
      ; sep (pretty_value env v2)
      ; sep (pretty_reg r)
      ]
  | Ast.IPrimopU (op, v, r) ->
    Box.box
      [ pretty_iprimopu op
      ; sep (pretty_value env v)
      ; sep (pretty_reg r)
      ]
  | Ast.IBranch (cond, v1, v2, vdest) ->
    Box.box
      [ pretty_icond cond
      ; sep (pretty_value env v1)
      ; sep (pretty_value env v2)
      ; sep (pretty_value env vdest)
      ]
  | Ast.FPrimop (op, v1, v2, r) ->
    Box.box
      [ pretty_fprimop op
      ; sep (pretty_valuef env v1)
      ; sep (pretty_valuef env v2)
      ; sep (pretty_regf r)
      ]
  | Ast.FBranch (cond, v1, v2, vdest) ->
    Box.box
      [ pretty_fcond cond
      ; sep (pretty_valuef env v1)
      ; sep (pretty_valuef env v2)
      ; sep (pretty_value env vdest)
      ]
  | Ast.LoadFloat (rmem, rval) ->
    Box.box
      [ Box.keyword "LoadFloat"
      ; sep (pretty_reg rmem)
      ; sep (pretty_regf rval)
      ]
  | Ast.StoreFloat (rmem, rval) ->
    Box.box
      [ Box.keyword "StoreFloat"
      ; sep (pretty_reg rmem)
      ; sep (pretty_regf rval)
      ]
  end |> Box.br

let pretty_program prog =
  Box.box (List.map (pretty_instr (Env.create Common.Var.name)) prog)
