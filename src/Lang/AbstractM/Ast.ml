type var  = Common.Var.t

type accesspath =
| Offp      of int
| Selp      of int * accesspath

type reg = int

type regf = int

type value =
| Reg       of reg
| Label     of var
| Immediate of int64

type valuef =
| Regf      of regf

type iprimop =
| Ashl | Ashr | Orb | Andb | Xorb
| Add  | Addt | Sub | Subt
| Divt | Modt | Mult

type iprimopu =
| Neg | Notb

type fprimop =
| Mulf | Divf | Addf | Subf

type icond =
| Eq | Neq | Lt | Le | Gt | Ge | Ltu | Leu | Gtu | Geu

type fcond =
| FEq | FNeq | FLt | FLe | FGt | FGe

type instr =
  { i_tag  : Common.Tag.t
  ; i_kind : instr_kind
  }
and instr_kind =
| Align
| Mark
| Halt
| Unreachable
| Mov          of value * reg
| EmitLong     of int64
| DefineLabel  of var
| EmitLabel    of var * int
| EmitString   of string
| RealConstant of Common.Real.t
| CheckLimit   of int
| BeginStdFn
| Jmp          of value
| Record       of (value * accesspath) list * reg
| Select       of int * value * reg
| Offset       of int * value * reg
| FetchIndexB  of value * reg * value
| StoreIndexB  of value * reg * value
| FetchIndexL  of value * reg * value
| StoreIndexL  of value * reg * value
| BBS          of value * value
| IPrimop      of iprimop * value * value * reg
| IPrimopU     of iprimopu * value * reg
| IBranch      of icond * value * value * value
| FPrimop      of fprimop * valuef * valuef * regf
| FBranch      of fcond * valuef * valuef * value
| LoadFloat    of reg * regf
| StoreFloat   of reg * regf

type prog = instr list

let reg_by_id n = n

let regf_by_id n = n

let string_of_reg r =
  "r" ^ string_of_int r

let string_of_regf r =
  "f" ^ string_of_int r

let string_of_iprimop op =
  match op with
  | Ashl -> "Ashl"
  | Ashr -> "Ashr"
  | Orb  -> "Orb"
  | Andb -> "Andb"
  | Xorb -> "Xorb"
  | Add  -> "Add"
  | Addt -> "Addt"
  | Sub  -> "Sub"
  | Subt -> "Subt"
  | Divt -> "Divt"
  | Modt -> "Modt"
  | Mult -> "Mult"

let string_of_iprimopu op =
  match op with
  | Neg  -> "Neg"
  | Notb -> "Notb"

let string_of_icond cond =
  match cond with
  | Eq  -> "Eq"
  | Neq -> "Neq"
  | Lt  -> "Lt"
  | Le  -> "Le"
  | Gt  -> "Gt"
  | Ge  -> "Ge"
  | Ltu -> "Ltu"
  | Leu -> "Leu"
  | Gtu -> "Gtu"
  | Geu -> "Geu"

let string_of_fprimop op =
  match op with
  | Mulf -> "Mulf"
  | Divf -> "Divf"
  | Addf -> "Addf"
  | Subf -> "Subf"

let string_of_fcond cond =
  match cond with
  | FEq  -> "FEq"
  | FNeq -> "FNeq"
  | FLt  -> "FLt"
  | FLe  -> "FLe"
  | FGt  -> "FGt"
  | FGe  -> "FGe"
