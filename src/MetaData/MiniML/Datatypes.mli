
type info =
  { env      : Lang.MiniML.TypeEnv.t
  ; args     : Lang.MiniML.Type.t list
  ; datatype : Lang.MiniML.Type.datatype_def
  }

val get     : Common.Tag.t -> info
val try_get : Common.Tag.t -> info option

val set : Common.Tag.t -> info -> unit
