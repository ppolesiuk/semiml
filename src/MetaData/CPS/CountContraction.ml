let count = ref 0
let inc () =
  let r = !count in
  count := r + 1;()

let get () = !count

let reset () = 
    count := 0;()