let table = Hashtbl.create 1024

let try_get vr =
  try Some(Hashtbl.find table vr) with
  | Not_found -> None

let known name = 
  try not !(Hashtbl.find table name) with
  | Not_found -> false

let escaping name =
  try !(Hashtbl.find table name) with
  | Not_found -> false

let set name info =
  Hashtbl.replace table name info

let reset () =
	Hashtbl.reset table
