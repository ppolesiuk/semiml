let table = Hashtbl.create 32

let get tag = 
  Hashtbl.find table tag

let set tag cost =
  Hashtbl.replace table tag cost
