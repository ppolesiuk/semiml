val get : Common.Var.t -> int
val add : Common.Var.t -> unit
val inc : Common.Var.t -> unit
val dec : Common.Var.t -> unit
val print: unit ->unit
val reset   : unit -> unit
