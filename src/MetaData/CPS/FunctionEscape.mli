open Lang.CPS.Ast

val try_get : var -> bool ref option
val known : var -> bool 
val escaping : var -> bool
val set     : var -> bool ref -> unit
val reset   : unit -> unit
