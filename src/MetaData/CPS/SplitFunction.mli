open Lang.CPS.Ast

val try_get : var -> bool

val set     : var -> unit
val reset   : unit -> unit