open Lang.CPS.Ast

type info = int * (value * accesspath) list

val get     : Common.Var.t -> info
val try_get : Common.Var.t -> info option

val set     : Common.Var.t -> info -> unit
val reset : unit -> unit
