val try_get : Common.Var.t -> int option
val inc : Common.Var.t -> unit
val dec : Common.Var.t -> unit
val reset : unit -> unit
val print: unit ->unit