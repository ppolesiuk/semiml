open Lang.CPS.Ast

type arity = BOT
			| UNK
		  | TOP
			| COUNT of int *bool


type fninfo =  {arity : arity list ref;
			  	alias : var option ref;
			  	escape : bool ref}

type info = FNinfo of fninfo
			| ARGinfo of int ref
			| RECinfo of int
			


let raise_not_found name =
  let err = Printf.sprintf "MetaData::CPS::FlatInfo: variable %s not found" (Common.Var.string_of_var name)
  in begin
    CompilerLog.error "%s" err;
    failwith err;
  end

let table = Hashtbl.create 1024

let get vr = 
  try Hashtbl.find table vr with
  | Not_found -> raise_not_found vr

let try_get vr =
  try Some(Hashtbl.find table vr) with
  | Not_found -> None

let set name info =
  Hashtbl.replace table name info

let reset () =
  Hashtbl.reset table  