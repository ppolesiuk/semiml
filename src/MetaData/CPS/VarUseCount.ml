let table = Hashtbl.create 1024

let try_get name = 
  try Some(Hashtbl.find table name) with
  | Not_found -> None

let get_with_default name =
  try (Hashtbl.find table name) with
  | Not_found -> 0

let inc name =
  let previousCount = get_with_default name
  in Hashtbl.replace table name (previousCount + 1)

let dec name =
    let previousCount = get_with_default name
    in Hashtbl.replace table name (previousCount - 1)

let reset () =
	Hashtbl.reset table
  
let print () = 
  Hashtbl.iter (fun x -> fun y -> print_string (Common.Var.string_of_var x) ; print_int y) table