type t =
| AbstractM
| CPS
| Lambda
| MiniML
| NuL
| RawCPS
| RawMiniML
| RawNuL

let name lang =
  match lang with
  | AbstractM -> "AbstractM"
  | CPS       -> "CPS"
  | Lambda    -> "Lambda"
  | MiniML    -> "MiniML"
  | NuL       -> "NuL"
  | RawCPS    -> "RawCPS"
  | RawMiniML -> "RawMiniML"
  | RawNuL    -> "RawNuL"
