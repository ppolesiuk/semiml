type t =
| AbstractM
| CPS
| Lambda
| MiniML
| NuL
| RawCPS
| RawMiniML
| RawNuL

val name : t -> string
