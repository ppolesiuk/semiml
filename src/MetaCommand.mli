type t =
| MetaCmd_Eval
| MetaCmd_Pretty
| MetaCmd_Require  of string list
| MetaCmd_RequireC of Contract.t list
| MetaCmd_Echo     of string
| MetaCmd_Seq      of t list
| MetaCmd_Code     of (Compiler.state -> unit)
| MetaCmd_If       of (Compiler.state -> bool) * t * t
| MetaCmd_While    of (Compiler.state -> bool) * t
| MetaCmd_DoWhile  of t * (Compiler.state -> bool)
| MetaCmd_Dynamic  of (Compiler.state -> t)

val run : Compiler.state -> t -> Compiler.state
