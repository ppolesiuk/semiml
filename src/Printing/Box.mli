
type color =
| Black
| Red
| Green
| Yellow
| Blue
| Magenta
| Cyan
| White

type base_attribute =
| BA_Bold
| BA_Underline
| BA_Blink
| BA_Negative
| BA_CrossedOut
| BA_FgColor of color
| BA_BgColor of color

type attribute =
| Error
| Keyword
| Number
| Literal
| Constant
| Constructor
| Operator
| Paren
| Variable
| Base of base_attribute

type t

val text  : ?attributes: attribute list -> string -> t

val error   : string -> t
val keyword : string -> t
val oper    : string -> t

val prefix : t -> t -> t
val suffix : t -> t -> t

val indent    : int -> t -> t
val white_sep : t -> t
val br        : t -> t

val box : t list -> t

val int : int -> t

val int64 : int64 -> t

val paren : 
  ?opn: string ->
  ?cls: string ->
    t -> t

val prec_paren : 
  ?opn: string -> 
  ?cls: string ->
  int -> int -> t -> t

val print_stdout : t -> unit
val print_stderr : t -> unit
