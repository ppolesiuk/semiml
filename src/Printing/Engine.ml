open DataTypes

exception Too_wide

let is_whitespace ch =
  match ch with
  | '\n' | '\r' | '\t' | ' ' -> true
  | _ -> false

class printer ?(max_width=80) ?(max_indent=60) (chan : out_channel) =
  let use_colors = ColorScheme.use_colors chan in
object(self)
  val mutable new_line   = true
  val mutable last_ws    = true
  val mutable pos        = 0
  val mutable break_line = false
  val mutable sl_new_line   = true
  val mutable sl_last_ws    = true
  val mutable sl_pos        = 0
  val mutable sl_break_line = false

  val sl_queue = Queue.create ()

  method private new_line =
    output_char chan '\n';
    new_line   <- true;
    last_ws    <- true;
    pos        <- 0;
    break_line <- false

  method private check_width =
    if sl_pos > max_width then raise Too_wide

  method private print_sl_box box =
    match box with
    | B_Text(attrs, str) ->
      let len = String.length str in
      if len > 0 then begin
        sl_pos <- sl_pos + len;
        self#check_width;
        sl_last_ws <- is_whitespace str.[len-1];
        sl_new_line <- false;
        Queue.add (attrs, str) sl_queue
      end
    | B_Prefix(box1, box2) | B_Suffix(box1, box2) -> 
      self#print_sl_box (B_Box [ box1; box2 ])
    | B_Indent(n, box) ->
      if sl_new_line then begin
        let n = min n (max_indent - sl_pos) in
        if n > 0 then begin
          sl_pos <- sl_pos + n;
          self#check_width;
          Queue.add ([], String.make n ' ') sl_queue
        end
      end;
      self#print_sl_box box
    | B_WhiteSep box ->
      if sl_last_ws then self#print_sl_box box
      else begin
        sl_pos <- sl_pos + 1;
        self#check_width;
        Queue.add ([], " ") sl_queue;
        sl_last_ws <- true;
        self#print_sl_box box
      end
    | B_BreakLine box ->
      self#print_sl_box box;
      sl_break_line <- true
    | B_Box boxes ->
      List.iter (fun box ->
        if sl_break_line then raise Too_wide;
        self#print_sl_box box
      ) boxes

  method private print_singleline indent box =
    sl_new_line   <- new_line;
    sl_last_ws    <- last_ws;
    sl_pos        <- (if new_line then indent else pos);
    sl_break_line <- false;
    Queue.clear sl_queue;
    self#print_sl_box box;
    last_ws    <- sl_last_ws;
    pos        <- sl_pos;
    break_line <- sl_break_line;
    if new_line && indent > 0 then
      output_string chan (String.make indent ' ');
    Queue.iter (fun (attrs, str) ->
      new_line <- false;
      if attrs <> [] && use_colors then ColorScheme.use_attributes chan attrs;
      output_string chan str;
      if attrs <> [] && use_colors then ColorScheme.default_attributes chan
    ) sl_queue

  method private print_box indent box suffixes =
    match box with
    | B_Text(attrs, str) ->
      let len = String.length str in
      if pos + len > max_width && not new_line then
        self#new_line;
      if len > 0 then begin
        if new_line && indent > 0 then begin
          output_string chan (String.make indent ' ');
          pos <- indent
        end;
        pos <- pos + len;
        last_ws <- is_whitespace str.[len-1];
        new_line <- false;
        if attrs <> [] && use_colors then
          ColorScheme.use_attributes chan attrs;
        output_string chan str;
        if attrs <> [] && use_colors then
          ColorScheme.default_attributes chan
      end;
      begin match suffixes with
      | [] -> ()
      | (indent, box) :: suffixes ->
        self#print_box indent box suffixes
      end
    | B_Prefix(box1, box2) ->
      self#print_box indent box1 [];
      let indent = min pos max_indent in
      if break_line then self#new_line;
      self#print_box indent box2 suffixes
    | B_Suffix(box1, box2) ->
      self#print_box indent box1 ((indent, box2) :: suffixes)
    | B_Indent(n, box) ->
      self#print_box (min (indent + n) max_indent) box suffixes
    | B_WhiteSep box ->
      if last_ws then self#print_box indent box suffixes
      else if pos >= max_width then begin
        self#new_line;
        self#print_box indent box suffixes
      end else begin
        pos <- pos + 1;
        output_char chan ' ';
        last_ws <- true;
        self#print_box indent box suffixes
      end
    | B_BreakLine box ->
      self#print_box indent box [];
      begin match suffixes with
      | [] -> break_line <- true
      | (indent, box) :: boxes ->
        self#new_line;
        self#print_box indent box suffixes
      end
    | B_Box [] -> ()
    | B_Box(box0 :: boxes) ->
      begin try 
        self#print_singleline indent (B_Box(box :: List.map snd suffixes))
      with
      | Too_wide ->
        self#print_multiline_box indent box0 boxes suffixes
      end

  method private print_multiline_box indent box boxes suffixes =
    match boxes with
    | [] -> self#print_box indent box suffixes
    | box1 :: boxes ->
      self#print_box indent box [];
      self#new_line;
      self#print_multiline_box indent box1 boxes suffixes

  method print box =
    self#print_box 0 box [];
    self#new_line
end

let print_stdout box =
  (new printer stdout)#print box

let print_stderr box =
  (new printer stderr)#print box
