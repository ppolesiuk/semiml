
include CompilerDB.GraphTypes

let node_of_state (Compiler.State(lang, _, contracts)) =
  Node(lang, contracts)

let find_path source dest =
  CompilerDB.PathFinder.find_path 
    source
    (fun node -> 
      if CompilerDB.GraphTypes.Node.instance node dest then Some ()
      else None)
    (fun () -> [])

let find_path_to_eval source =
  CompilerDB.PathFinder.find_path
    source
    (fun (Node(lang, contracts)) ->
      match CompilerDB.EvaluatorDB.possible_evaluators lang contracts with
      | []        -> None
      | eval :: _ -> Some eval)
    (fun eval -> [ CompilerDB.GraphTypes.Cmd_Eval eval ])

let find_path_to_pretty source =
  CompilerDB.PathFinder.find_path
    source
    (fun (Node(lang, contracts)) ->
      match CompilerDB.PrettyDB.possible_printers lang contracts with
      | []         -> None
      | print :: _ -> Some print)
    (fun print -> [ CompilerDB.GraphTypes.Cmd_Pretty print ])

let find_path_to_contracts source contracts =
  CompilerDB.PathFinder.find_path
    source
    (fun (Node(lang, st_contracts)) ->
      if
        List.for_all (fun c -> Contract.Set.mem c st_contracts) contracts
      then Some ()
      else None)
    (fun () -> [])

let find_path_to_contract_names source contract_names =
  CompilerDB.PathFinder.find_path
    source
    (fun (Node(lang, contracts)) ->
      if 
        List.for_all (fun name ->
            Contract.Set.exists (fun c -> Contract.name c = name) contracts
          ) contract_names
      then Some ()
      else None)
    (fun () -> [])

let run_analyse_cmd
    (CompilerDB.Analysis.Analysis(lang, adata))
    (CompilerDB.Types.State(st_lang, program, contracts)) =
  CompilerLog.info "running analysis: %s" 
    adata.CompilerDB.Analysis.name;
  match CompilerDB.Types.Language.get_equality lang st_lang with
  | Equality.Eq ->
    assert (List.for_all
      (fun c -> Contract.Set.mem c contracts)
      (adata.CompilerDB.Analysis.require));
    adata.CompilerDB.Analysis.analyse program;
    let new_contracts =
      Contract.apply_rules
        adata.CompilerDB.Analysis.contract_rules
        contracts in
    CompilerDB.Types.State(lang, program, new_contracts)
  | Equality.Neq -> assert false

let run_eval_cmd
    (CompilerDB.Evaluator.Evaluator(eval_lang, eval_data))
    (CompilerDB.Types.State(st_lang, program, contracts)) =
  CompilerLog.info "running evaluator: %s" 
    eval_data.CompilerDB.Evaluator.name;
  match CompilerDB.Types.Language.get_equality eval_lang st_lang with
  | Equality.Eq ->
    assert (List.for_all
      (fun c -> Contract.Set.mem c contracts)
      (eval_data.CompilerDB.Evaluator.require));
    eval_data.CompilerDB.Evaluator.eval program
  | Equality.Neq -> assert false

let run_pretty_cmd
    (CompilerDB.Pretty.Pretty(pretty_lang, pretty_data))
    (CompilerDB.Types.State(st_lang, program, contracts)) =
  CompilerLog.info "running pretty-printer: %s"
    pretty_data.CompilerDB.Pretty.name;
  match CompilerDB.Types.Language.get_equality pretty_lang st_lang with
  | Equality.Eq ->
    assert (List.for_all 
      (fun c -> Contract.Set.mem c contracts)
      (pretty_data.CompilerDB.Pretty.require));
    Printing.Box.print_stdout 
      (pretty_data.CompilerDB.Pretty.pretty program)
  | Equality.Neq -> assert false

let run_transform_cmd
    (CompilerDB.Transform.Transform(src_lang, tgt_lang, trans_data))
    (CompilerDB.Types.State(st_lang, program, contracts)) =
  CompilerLog.info "running transformation: %s"
    trans_data.CompilerDB.Transform.name;
  match CompilerDB.Types.Language.get_equality src_lang st_lang with
  | Equality.Eq ->
    assert (List.for_all
      (fun c -> Contract.Set.mem c contracts)
      (trans_data.CompilerDB.Transform.require));
    let new_program   = trans_data.CompilerDB.Transform.transform program in
    let new_contracts =
      Contract.apply_rules
        trans_data.CompilerDB.Transform.contract_rules
        contracts in
    CompilerDB.Types.State(tgt_lang, new_program, new_contracts)
  | Equality.Neq -> assert false

let check_contracts (CompilerDB.Types.State(lang, program, contracts)) =
  Contract.Set.iter (fun contract ->
    match CompilerDB.ContractCheckerDB.possible_checkers lang contract with
    | [] ->
      CompilerLog.info "there is no checker to check contract %s"
        (Contract.name contract)
    | (CompilerDB.ContractChecker.ContractChecker(ch_lang, data)) :: _ ->
      CompilerLog.info "checking contract %s using checker: %s"
        (Contract.name contract)
        data.CompilerDB.ContractChecker.name;
      begin match CompilerDB.Types.Language.get_equality lang ch_lang with
      | Equality.Eq ->
        if not (data.CompilerDB.ContractChecker.checker program) then
          CompilerLog.error "Contract %s failed." (Contract.name contract)
      | Equality.Neq -> assert false
      end
  ) contracts

let rec run_path path state =
  match path with
  | [] -> state
  | Cmd_Analyse analyse :: path ->
    let state = run_analyse_cmd analyse state in
    if Settings.get_check_contracts () then
      check_contracts state;
    run_path path state
  | Cmd_Eval eval :: path ->
    run_eval_cmd eval state;
    run_path path state
  | Cmd_Pretty pretty :: path ->
    run_pretty_cmd pretty state;
    run_path path state
  | Cmd_Transform trans :: path ->
    let state = run_transform_cmd trans state in
    if Settings.get_check_contracts () then
      check_contracts state;
    run_path path state

let parse_file fname =
  let state =
    match CompilerDB.ParserDB.find_parser_by_filename fname with
    | CompilerDB.Parser.Parser(lang, parser_data) ->
      CompilerLog.info "parsing file `%s' using parser: %s"
        fname
        parser_data.CompilerDB.Parser.name;
      begin match open_in fname with
      | chan ->
        let lexbuf = Lexing.from_channel chan in
        lexbuf.Lexing.lex_curr_p <-
          { lexbuf.Lexing.lex_curr_p with
            Lexing.pos_fname = fname
          };
        let result = parser_data.CompilerDB.Parser.parser_func lexbuf in
        Compiler.State(lang, result, parser_data.CompilerDB.Parser.contracts)
      | exception Sys_error msg ->
        Errors.error "Cannot open the file `%s' (%s)." fname msg;
        raise Errors.Fatal_error
      end
    | exception Not_found ->
      Errors.error "Cannot find a parser to parse the file `%s'." fname;
      raise Errors.Fatal_error
  in
  if Settings.get_check_contracts ()
    then check_contracts state;
  state
