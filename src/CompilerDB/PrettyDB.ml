
open Pretty

let pretty_list = ref []

let register_pretty_printer pretty_obj =
  pretty_list := pretty_obj :: !pretty_list

let possible_printers lang contracts =
  List.filter (fun pretty_obj ->
    match pretty_obj with
    | Pretty(lang2, data) ->
      Types.Language.equal lang lang2 &&
      List.for_all (fun c -> Contract.Set.mem c contracts) data.require
  ) !pretty_list
