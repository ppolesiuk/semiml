
let parser_list = ref []

let register_parser parser_obj =
  parser_list := parser_obj :: !parser_list

let find_parser_by_filename fname =
  List.find (fun (Parser.Parser(_, data)) ->
    match data.Parser.extension with
    | None -> false
    | Some ext -> Filename.check_suffix fname ("." ^ ext)
  ) !parser_list
