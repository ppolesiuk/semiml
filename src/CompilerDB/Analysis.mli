
type 'lang analysis_data =
  { analyse        : 'lang Types.analysis
  ; name           : string
  ; weight         : float
  ; require        : Contract.t list
  ; contract_rules : Contract.rule list
  }

type t =
| Analysis : 'lang Types.language * 'lang analysis_data -> t
