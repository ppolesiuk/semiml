
type 'lang parser_data =
  { parser_func : 'lang Types.parser_func
  ; name        : string
  ; extension   : string option
  ; contracts   : Contract.Set.t
  }

type t =
| Parser : 'lang Types.language * 'lang parser_data -> t
