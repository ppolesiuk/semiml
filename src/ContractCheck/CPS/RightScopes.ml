open Lang.CPS.Ast

module Env = Common.Var.Set

let check_value env value =
  match value with
  | Var x | Label x -> Env.mem x env
  | Int _ | Real _ | String _ -> true

let build_fix_env env defs =
  List.fold_left (fun env (x, _, _) -> Env.add x env) env defs

let add_vars env xs =
  List.fold_left (fun env x -> Env.add x env) env xs

let rec check_expr env expr =
  match expr.e_kind with
  | Record(vs, x, e) ->
    List.for_all (fun (v, _) -> check_value env v) vs
    && check_expr (Env.add x env) e
  | Select(_, v, x, e) | Offset(_, v, x, e) ->
    check_value env v && check_expr (Env.add x env) e
  | App(v, vs) ->
    check_value env v && List.for_all (check_value env) vs
  | Fix(defs, e) ->
    let env = build_fix_env env defs in
    List.for_all (fun (_, xs, body) ->
      check_expr (add_vars env xs) body
    ) defs
    && check_expr env e
  | Switch(v, es) ->
    check_value env v && List.for_all (check_expr env) es
  | FFI(_, _, ret, cont) ->
    check_expr (Env.add ret env) cont
  | Primop(_, vs, xs, es) ->
    List.for_all (check_value env) vs &&
    List.for_all (check_expr (add_vars env xs)) es

let check_program expr =
  check_expr (Env.singleton top_cont) expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_CPS
    ~name: "CPS:right_scopes"
    ~contract: Lang.CPS.Contracts.right_scopes
    check_program
