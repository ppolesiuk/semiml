open Lang.CPS.Ast

(* TODO: this is the number of machine registers.
 * How to get it from the user? *)
let registers_num = 10

module VarSet = Common.Var.Set

let fv e = MetaData.CPS.FreeVars.get e.e_tag

let rec check expr =
  if VarSet.cardinal (fv expr) > registers_num then
    false
  else
    List.for_all check (get_continuations expr)

let check_program expr =
  (* TODO: this should be a requirement, but contract checkers cannot be
   * registered with requirements right now. *)
  let _ = Analysis.CPS.FreeVars.analyse expr in
    check expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_CPS
    ~name: "CPS:registers_spilled"
    ~contract: Lang.CPS.Contracts.registers_spilled
    check_program
