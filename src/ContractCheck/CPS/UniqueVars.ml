open Lang.CPS.Ast

let used_vars = Hashtbl.create 32

let check_var x =
  if Hashtbl.mem used_vars x then false
  else begin
    Hashtbl.add used_vars x ();
    true
  end

let rec check_expr expr =
  match expr.e_kind with
  | Record(_, x, e) | Select(_, _, x, e) | Offset(_, _, x, e) ->
    check_var x && check_expr e
  | App _ -> true
  | Fix(defs, e) ->
    List.for_all (fun (x, args, body) ->
      check_var x &&
      List.for_all check_var args &&
      check_expr body
    ) defs
    && check_expr e
  | Switch(_, es) -> List.for_all check_expr es
  | FFI(_, _, ret, cont) ->
    check_var ret && check_expr cont
  | Primop(_, _, xs, es) ->
    List.for_all check_var xs && List.for_all check_expr es

let check_program expr =
  Hashtbl.reset used_vars;
  check_expr expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_CPS
    ~name: "CPS:unique_vars"
    ~contract: Lang.CPS.Contracts.unique_vars
    check_program
