open Lang.Lambda.Ast

module Env = Common.Var.Set


let build_fix_env env defs =
  List.fold_left (fun env (x, _) -> Env.add x env) env defs

let rec check_expr env expr =
  match expr.e_kind with
    | Var x  -> Env.mem x env
    | Int _ | Real _ | String _ | FFI _ | Prim _ -> true
    | Fn(x, e) -> check_expr (Env.add x env) e
    | Con(_, e) | Decon(_, e) | ExnValue(e) | ExnName(e) 
    | Select(_,e) | Raise (e) ->
      check_expr env e
    | App(e1, e2) | ConExn(e1, e2) | Handle(e1, e2)
      -> check_expr env e1 && check_expr env e2
    | Fix(defs, e) ->
      let env = build_fix_env env defs in
        List.for_all (fun (_, def) ->
          check_expr env def
        ) defs
       && check_expr env e
    | Record(es) ->
      List.for_all (check_expr env) es
    | SwitchExn(e1, l, e2) -> 
      List.for_all (fun (x1, x2) -> check_expr env x1 && check_expr env x2) l 
      && check_expr env e1 && check_expr env e2
    | Switch se -> check_expr env se.sw_expr 
      && List.for_all (fun (_, e) -> check_expr env e) se.sw_con_cases
      && List.for_all (fun (_, e) -> check_expr env e) se.sw_constant_cases 
      && match se.sw_default_case with 
         |None -> true
         |Some e -> check_expr env e
         

let check_program expr =
  check_expr (Env.empty) expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_Lambda
    ~name: "Lambda:right_scopes"
    ~contract: Lang.Lambda.Contracts.right_scopes
    check_program



