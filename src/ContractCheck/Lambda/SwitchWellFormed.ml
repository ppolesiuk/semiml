open Lang.Lambda.Ast

let rec contains l c =
  match l with
  | [] -> false
  | (x,_) :: ll -> if x=c then true else contains ll c

let rec range (m:int64) (n:int64) : int64 list = 
  if (Int64.compare m n) >= 0 then [] else m :: range (Int64.succ m) n

let exhaustive se =
  match se.sw_default_case with 
  | Some _ -> true
  | None -> List.for_all (contains se.sw_con_cases) se.sw_possible_cons &&
    match se.sw_constant_range with
    | None -> false
    | Some n -> List.for_all (contains se.sw_constant_cases) (range 0L n)

let in_range se = 
  List.for_all (fun (c, _) -> List.mem c se.sw_possible_cons) 
    se.sw_con_cases &&
  match se.sw_constant_range with
  | None ->  true 
  | Some n -> 
    List.for_all 
      (fun (i, _) -> Int64.compare i 0L >= 0 && Int64.compare i n < 0)  
      se.sw_constant_cases

let well_boxed se = 
   if List.mem C_Transparent se.sw_possible_cons then 
     List.length se.sw_con_cases = 1 
     && se.sw_constant_range = Some 0L 
   else if List.mem C_TransU se.sw_possible_cons then 
     se.sw_constant_range = Some 0L
   else if List.mem C_TransB se.sw_possible_cons then
     List.length se.sw_con_cases = 1 
   else true

let check_switch se  =
  List.length se.sw_possible_cons = List.length se.sw_con_cases && 
  exhaustive se &&
  in_range se &&
  well_boxed se

let rec check_expr expr =
  match expr.e_kind with
    | Var _  | Int _ | Real _ | String _ | FFI _ | Prim _ -> true
    | Fn(_, e) -> check_expr e
    | Con(_, e) | Decon(_, e) | ExnValue(e) | ExnName(e) 
    | Select(_,e) | Raise (e) ->
      check_expr  e
    | App(e1, e2) | ConExn(e1, e2) | Handle(e1, e2)
      -> check_expr e1 && check_expr e2
    | Fix(defs, e) ->
        List.for_all (fun (_, def) -> check_expr def) defs
       && check_expr e
    | Record(es) ->
      List.for_all check_expr es
    | SwitchExn(e1, l, e2) -> 
      List.for_all (fun (x1, x2) -> check_expr x1 && check_expr x2) l 
      && check_expr e1 && check_expr e2
    | Switch se -> check_switch se && check_expr se.sw_expr 
      && List.for_all (fun (_, e) -> check_expr e) se.sw_con_cases
      && List.for_all (fun (_, e) -> check_expr e) se.sw_constant_cases 
      && match se.sw_default_case with 
         |None -> true
         |Some e -> check_expr e

let check_program expr =
  check_expr expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_Lambda
    ~name: "Lambda:switch_well_formed"
    ~contract: Lang.Lambda.Contracts.switch_well_formed
    check_program
