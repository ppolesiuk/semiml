
let table = Hashtbl.create 32

let get tag = 
  Hashtbl.find table tag

let try_get tag =
  try Some(get tag) with
  | Not_found -> None

let set tag pos =
  Hashtbl.replace table tag pos

let copy tag1 tag2 =
  try let pos = get tag1 in set tag2 pos with
  | Not_found -> ()
