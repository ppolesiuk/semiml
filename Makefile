BINDIR = bin
TARGET = semiML
SOURCES = $(shell find src -regex ".*\\.ml\\([ily]\\|pack\\)?")
.PHONY: all clean $(BINDIR)/$(TARGET)

all: $(BINDIR) $(BINDIR)/$(TARGET) $(SOURCES)

$(BINDIR):
	mkdir $(BINDIR)

$(BINDIR)/$(TARGET): $(SOURCES)
	ocamlbuild -cflags -bin-annot -libs str,unix -Is src $(TARGET).native
	cp -L $(TARGET).native $@
	rm $(TARGET).native

$(BINDIR)/tests: $(SOURCES) test/tests.ml
	ocamlbuild -cflags -bin-annot -libs str -pkgs oUnit -Is src,test tests.native
	cp -L tests.native $@
	rm tests.native

clean:
	ocamlbuild -clean
	rm -f $(BINDIR)/$(TARGET)
	rm -f $(BINDIR)/tests
